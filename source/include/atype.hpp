#ifndef ATYPE_HPP
#define ATYPE_HPP

class BaseExpressionAST;
class MainContext;

#include <iostream>
#include <string>
#include <cassert>
#include <llvm/IR/Module.h>

//-----------------------------------------------------------------------------
// AType
//-----------------------------------------------------------------------------
class AType
{
public:
    enum TypeTag {T_VOID, T_BOOL, T_BYTE, T_WORD, T_DWORD, T_REAL, T_POINTER, T_ARRAY, T_UNKNOWN};
    enum TypeClass {C_NUMBER, C_POINTER, C_BOOL, C_VOID, C_ARRAY};

    virtual ~AType();

    virtual AType *clone() const = 0;

    static bool areCompatible(AType const *left, AType const *right, bool isPointerType = false);

    virtual llvm::Type *getRawType(MainContext &mainContext) const = 0;
    virtual TypeTag getTypeTag() const = 0;
    virtual TypeClass getTypeClass() const = 0;
    virtual std::string const printType() const = 0;

    friend bool operator>(AType const &left, AType const &right);
    friend bool operator==(AType const &left, AType const &right);
    friend bool operator<(AType const &left, AType const &right);
};

std::ostream &operator<<(std::ostream &out, AType const &type);

//-----------------------------------------------------------------------------
// APointer
//-----------------------------------------------------------------------------
class APointer : public AType
{
public:
    APointer(AType const *pointerToType);
    APointer(APointer const &type);
    APointer &operator=(APointer const &type) = delete;
    ~APointer() override;

    virtual AType *clone() const override;

    virtual llvm::Type *getRawType(MainContext &mainContext) const override;
    virtual TypeTag getTypeTag() const override;
    virtual TypeClass getTypeClass() const override;
    virtual std::string const printType() const override;

    llvm::Type *calculatePointerType(AType const *pointerType, MainContext &mainContext) const;
    AType const *getPointee() const;

private:
    AType const *_pointee;
};

//-----------------------------------------------------------------------------
// AArray
//-----------------------------------------------------------------------------
class AArray : public AType
{
public:
    AArray(AType const *pointerToType, unsigned long numberOfElements);
    AArray(AArray const &type);
    AArray &operator=(AArray const &type) = delete;
    ~AArray() override;

    virtual AType *clone() const override;

    virtual llvm::Type *getRawType(MainContext &mainContext) const override;
    virtual TypeTag getTypeTag() const override;
    virtual TypeClass getTypeClass() const override;
    virtual std::string const printType() const override;

    unsigned long getArraySize() const;
    AType const *getPointee() const;

private:
    AType const *_pointee;
    unsigned long const _numberOfElements;
};

//-----------------------------------------------------------------------------
// ABool
//-----------------------------------------------------------------------------
class ABool : public AType
{
public:    
    virtual AType *clone() const override;

    virtual llvm::Type *getRawType(MainContext &mainContext) const override;
    virtual TypeTag getTypeTag() const override;
    virtual TypeClass getTypeClass() const override;
    virtual std::string const printType() const override;
};

//-----------------------------------------------------------------------------
// AByte
//-----------------------------------------------------------------------------
class AByte : public AType
{
public:
    virtual AType *clone() const override;

    virtual llvm::Type *getRawType(MainContext &mainContext) const override;
    virtual TypeTag getTypeTag() const override;
    virtual TypeClass getTypeClass() const override;
    virtual std::string const printType() const override;
};

//-----------------------------------------------------------------------------
// AWord
//-----------------------------------------------------------------------------
class AWord : public AType
{
public:
    virtual AType *clone() const override;

    virtual llvm::Type *getRawType(MainContext &mainContext) const override;
    virtual TypeTag getTypeTag() const override;
    virtual TypeClass getTypeClass() const override;
    virtual std::string const printType() const override;
};

//-----------------------------------------------------------------------------
// ADword
//-----------------------------------------------------------------------------
class ADword : public AType
{
public:
    virtual AType *clone() const override;

    virtual llvm::Type *getRawType(MainContext &mainContext) const override;
    virtual TypeTag getTypeTag() const override;
    virtual TypeClass getTypeClass() const override;
    virtual std::string const printType() const override;
};

//-----------------------------------------------------------------------------
// AReal
//-----------------------------------------------------------------------------
class AReal : public AType
{
public:
    virtual AType *clone() const override;

    virtual llvm::Type *getRawType(MainContext &mainContext) const override;
    virtual TypeTag getTypeTag() const override;
    virtual TypeClass getTypeClass() const override;
    virtual std::string const printType() const override;
};

//-----------------------------------------------------------------------------
// AVoid
//-----------------------------------------------------------------------------
class AVoid : public AType
{
public:
    virtual AType *clone() const override;

    virtual llvm::Type *getRawType(MainContext &mainContext) const override;
    virtual TypeTag getTypeTag() const override;
    virtual TypeClass getTypeClass() const override;
    virtual std::string const printType() const override;
};

//-----------------------------------------------------------------------------
// AUnknown
//-----------------------------------------------------------------------------
class AUnknown : public AType
{
public:
    virtual AType *clone() const override;

    virtual llvm::Type *getRawType(MainContext &mainContext) const override;
    virtual TypeTag getTypeTag() const override;
    virtual TypeClass getTypeClass() const override;
    virtual std::string const printType() const override;
};

#endif // ATYPE_HPP
