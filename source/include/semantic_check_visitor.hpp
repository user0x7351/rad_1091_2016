#ifndef SEMANTIC_CHECK_VISITOR_HPP
#define SEMANTIC_CHECK_VISITOR_HPP

class MainContext;

#include "visitor.hpp"
#include "node_ast.hpp"

//-----------------------------------------------------------------------------
// SemanticCheckVisitor
//-----------------------------------------------------------------------------
class SemanticCheckVisitor : public Visitor
{
public:
	SemanticCheckVisitor();
	virtual ~SemanticCheckVisitor() override;

	bool getSemanticState(BaseNodeAST const *node);
	bool getCurrentSemanticState();
	void storeSemanticState(bool state);

	virtual void visit(BoolAST const *node) override;
	virtual void visit(WordAST const *node) override;
	virtual void visit(DWordAST const *node) override;
	virtual void visit(ByteAST const *node) override;
	virtual void visit(RealAST const *node) override;
	virtual void visit(BinaryOperatorAST const *node) override;
	virtual void visit(ArithmeticBinaryOperatorAST const *node) override;
	virtual void visit(ArithmeticUnaryOperatorAST const *node) override;
	virtual void visit(RelationBinaryOperatorAST const *node) override;
	virtual void visit(LogicBinaryOperatorAST const *node) override;
	virtual void visit(UnaryOperatorAST const *node) override;
	virtual void visit(LogicUnaryOperatorAST const *node) override;
	virtual void visit(AdditionAST const *node) override;
	virtual void visit(SubtractionAST const *node) override;
	virtual void visit(MultiplicationAST const *node) override;
	virtual void visit(DivisionAST const *node) override;
	virtual void visit(ModAST const *node) override;
	virtual void visit(LessThanAST const *node) override;
	virtual void visit(GreaterThanAST const *node) override;
	virtual void visit(EqualToAST const *node) override;
	virtual void visit(NotEqualToAST const *node) override;
	virtual void visit(AndAST const *node) override;
	virtual void visit(OrAST const *node) override;
	virtual void visit(NotAST const *node) override;
	virtual void visit(NegAST const *node) override;
	virtual void visit(EquivalentToAST const *node) override;
	virtual void visit(FunctionCallExpressionAST const *node) override;
	virtual void visit(LocalVariableAccessAST const *node) override;
	virtual void visit(LocalPointerAccessAST const *node) override;
	virtual void visit(LocalVariableAddressAST const *node) override;
	virtual void visit(ExpressionCommandAST const *node) override;

	virtual void visit(BlockAST const *node) override;
	virtual void visit(PrototypeAST const *node) override;
	virtual void visit(FunctionDefinitionAST const *node) override;
	virtual void visit(ReturnStatementAST const *node) override;
	virtual void visit(WhileAST const *node) override;
	virtual void visit(ForAST const *node) override;
	virtual void visit(IfThenElseAST const *node) override;
	virtual void visit(IOCommandAST const *node) override;
	virtual void visit(FunctionCallStatementAST const *node) override;
	virtual void visit(LocalVariableDeclarationAST const *node) override;
	virtual void visit(AssignmentAST const *node) override;
	virtual void visit(PointerAssignmentAST const *node) override;

private:
	bool _state;
	MainContext &_mainContext;
};

#endif // SEMANTIC_CHECK_VISITOR_HPP
