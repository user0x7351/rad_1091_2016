#ifndef SCOPE_HPP
#define SCOPE_HPP

class AType;
class PrototypeAST;
class LocalVariableDeclarationAST;

#include <vector>

//-----------------------------------------------------------------------------
// Scope
//-----------------------------------------------------------------------------
class Scope
{
public:
    Scope(PrototypeAST const *prototype);

    AType const *getReturnType() const;
    PrototypeAST const *getPrototype() const;

    std::vector<LocalVariableDeclarationAST const *> const &getSymbolTable() const;
    std::vector<LocalVariableDeclarationAST const *> &updateSymbolTable();

private:
    std::vector<LocalVariableDeclarationAST const *> _variables;
    PrototypeAST const *_prototype;
};

#endif // SCOPE_HPP
