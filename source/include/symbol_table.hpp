#ifndef SYMBOL_TABLE_HPP
#define SYMBOL_TABLE_HPP

class PrototypeAST;
class AType;
class Scope;
class LocalVariableDeclarationAST;

#include <string>
#include <vector>

//-----------------------------------------------------------------------------
// SymbolTable
//-----------------------------------------------------------------------------
class SymbolTable
{
public:
    void registerNewScope(PrototypeAST const *prototype = nullptr);
    void destroyCurrentScope();
    AType const *getCurrentScopeReturnType() const;

    bool registerPrototype(PrototypeAST const *prototype);
    PrototypeAST const *getPrototypeFromSymbolTable(std::string const &functionName) const;

    bool registerVariable(LocalVariableDeclarationAST const *localVariableDeclaration);
    LocalVariableDeclarationAST const *getLocalVariableFromSymbolTable(std::string const &variableName) const;

private:
    std::vector<Scope *> _scopes;
    std::vector<PrototypeAST const *> _prototypes;
};

#endif // SYMBOL_TABLE_HPP
