#ifndef POINTER_CONTENT_HPP
#define POINTER_CONTENT_HPP

class BaseExpressionAST;

#include <iostream>
#include <string>

//-----------------------------------------------------------------------------
// PointerContent
//-----------------------------------------------------------------------------
class PointerContent
{
public:
    PointerContent(std::string const *identifier, BaseExpressionAST const *index);
    PointerContent(PointerContent const &pointerContent);
    PointerContent &operator=(PointerContent const &pointerContent) = delete;
    ~PointerContent();

    PointerContent *clone() const;

    unsigned short getReferenceNumber() const;
    std::string const &getIdentifier() const;
    BaseExpressionAST const *getIndex() const;

    void incrementReferenceNumber();

private:
    std::string const _identifier;
    unsigned short _referenceNumber;
    BaseExpressionAST const *_index;
};

#endif // POINTER_CONTENT_HPP
