#ifndef VISITOR_HPP
#define VISITOR_HPP

class BoolAST;
class WordAST;
class DWordAST;
class ByteAST;
class RealAST;
class BinaryOperatorAST;
class ArithmeticBinaryOperatorAST;
class ArithmeticUnaryOperatorAST;
class RelationBinaryOperatorAST;
class LogicBinaryOperatorAST;
class UnaryOperatorAST;
class LogicUnaryOperatorAST;
class AdditionAST;
class SubtractionAST;
class MultiplicationAST;
class DivisionAST;
class ModAST;
class LessThanAST;
class GreaterThanAST;
class EqualToAST;
class NotEqualToAST;
class AndAST;
class OrAST;
class NotAST;
class NegAST;
class EquivalentToAST;
class FunctionCallExpressionAST;
class LocalVariableAccessAST;
class LocalPointerAccessAST;
class LocalVariableAddressAST;
class ExpressionCommandAST;
class BlockAST;
class PrototypeAST;
class FunctionDefinitionAST;
class ReturnStatementAST;
class WhileAST;
class ForAST;
class IfThenElseAST;
class IOCommandAST;
class FunctionCallStatementAST;
class LocalVariableDeclarationAST;
class AssignmentAST;
class PointerAssignmentAST;
class BaseStatementAST;
class BaseExpressionAST;

//-----------------------------------------------------------------------------
// Visitor
//-----------------------------------------------------------------------------
class Visitor
{
public:
	virtual ~Visitor();

	virtual void visit(BoolAST const *node) = 0;
	virtual void visit(WordAST const *node) = 0;
	virtual void visit(DWordAST const *node) = 0;
	virtual void visit(ByteAST const *node) = 0;
	virtual void visit(RealAST const *node) = 0;
	virtual void visit(BinaryOperatorAST const *node) = 0;
	virtual void visit(ArithmeticBinaryOperatorAST const *node) = 0;
	virtual void visit(ArithmeticUnaryOperatorAST const *node) = 0;
	virtual void visit(RelationBinaryOperatorAST const *node) = 0;
	virtual void visit(LogicBinaryOperatorAST const *node) = 0;
	virtual void visit(UnaryOperatorAST const *node) = 0;
	virtual void visit(LogicUnaryOperatorAST const *node) = 0;
	virtual void visit(AdditionAST const *node) = 0;
	virtual void visit(SubtractionAST const *node) = 0;
	virtual void visit(MultiplicationAST const *node) = 0;
	virtual void visit(DivisionAST const *node) = 0;
	virtual void visit(ModAST const *node) = 0;
	virtual void visit(LessThanAST const *node) = 0;
	virtual void visit(GreaterThanAST const *node) = 0;
	virtual void visit(EqualToAST const *node) = 0;
	virtual void visit(NotEqualToAST const *node) = 0;
	virtual void visit(AndAST const *node) = 0;
	virtual void visit(OrAST const *node) = 0;
	virtual void visit(NotAST const *node) = 0;
	virtual void visit(NegAST const *node) = 0;
	virtual void visit(EquivalentToAST const *node) = 0;
	virtual void visit(FunctionCallExpressionAST const *node) = 0;
	virtual void visit(LocalVariableAccessAST const *node) = 0;
	virtual void visit(LocalPointerAccessAST const *node) = 0;
	virtual void visit(LocalVariableAddressAST const *node) = 0;
	virtual void visit(ExpressionCommandAST const *node) = 0;

	virtual void visit(BlockAST const *node) = 0;
	virtual void visit(PrototypeAST const *node) = 0;
	virtual void visit(FunctionDefinitionAST const *node) = 0;
	virtual void visit(ReturnStatementAST const *node) = 0;
	virtual void visit(WhileAST const *node) = 0;
	virtual void visit(ForAST const *node) = 0;
	virtual void visit(IfThenElseAST const *node) = 0;
	virtual void visit(IOCommandAST const *node) = 0;
	virtual void visit(FunctionCallStatementAST const *node) = 0;
	virtual void visit(LocalVariableDeclarationAST const *node) = 0;
	virtual void visit(AssignmentAST const *node) = 0;
	virtual void visit(PointerAssignmentAST const *node) = 0;
};

#endif // VISITOR_HPP
