#ifndef MAIN_CONTEXT_HPP
#define MAIN_CONTEXT_HPP

#include <string>
#include <llvm/IR/Value.h>
#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/Support/raw_ostream.h>
#include "symbol_table.hpp"

//-----------------------------------------------------------------------------
// MainContext
//-----------------------------------------------------------------------------
class MainContext
{
public:
    MainContext &operator=(MainContext const &mainContext) = delete;
    MainContext(MainContext const &mainContext) = delete;
    static MainContext &GetInstance();

    llvm::Module *getLLVMModule();
    llvm::IRBuilder<> &getIRBuilder();
    llvm::LLVMContext &getLLVMContext();

    SymbolTable &updateSymbolTable();
    SymbolTable const &getSymbolTable() const;

    void printToFile() const;

    void setFileName(const std::string &fileName);
    std::string const &getFileName() const;

    llvm::Value *doPromotion(llvm::Value *value, AType const *fromType, AType const *toType);
    llvm::Value *doDemotion(llvm::Value *value, AType const *fromType, AType const *toType);

    llvm::AllocaInst *createEntryBlockAlloca(llvm::Function *function, LocalVariableDeclarationAST const *variable, MainContext &mainContext) const;

private:
    MainContext();

    std::string _filename;
    SymbolTable _symbolTable;
    llvm::LLVMContext _llvmContext;
    llvm::Module _llvmModule;
    llvm::IRBuilder<> _irBuilder;
};

#endif // MAIN_CONTEXT_HPP
