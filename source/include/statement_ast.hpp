#ifndef STATEMENT_AST_HPP
#define STATEMENT_AST_HPP

class MainContext;
class AType;
class Visitor;
class BaseExpressionAST;
class LocalVariableDeclarationAST;
class PointerContent;

#include <iostream>
#include <vector>
#include <llvm/IR/IRBuilder.h>
#include "node_ast.hpp"

//-----------------------------------------------------------------------------
// BaseStatementAST
//-----------------------------------------------------------------------------
class BaseStatementAST : public BaseNodeAST
{
public:
	virtual ~BaseStatementAST();
};

//-----------------------------------------------------------------------------
// BlockAST
//-----------------------------------------------------------------------------
class BlockAST : public BaseStatementAST
{
public:
	BlockAST();
	BlockAST(BlockAST const &block) = delete;
	BlockAST &operator=(BlockAST const &block) = delete;
	~BlockAST() override;
	void addStatement(BaseStatementAST const *statement);
	const std::vector<BaseStatementAST const *> &getStatements() const;
	virtual void accept(Visitor &visitor) const override;

private:
	std::vector<BaseStatementAST const *> _block;
};

//-----------------------------------------------------------------------------
// PrototypeAST
//-----------------------------------------------------------------------------
class PrototypeAST : public BaseStatementAST
{
public:
	PrototypeAST(std::string const &functionName, std::vector<LocalVariableDeclarationAST const *> const &parameters, AType const *returnType, std::string const &prototypeType = "prototype");
	PrototypeAST(PrototypeAST const &function) = delete;
	PrototypeAST &operator=(PrototypeAST const &function) = delete;
	~PrototypeAST() override;
	std::string const &getFunctionName() const;
	AType const *getReturnType() const;
	std::vector<LocalVariableDeclarationAST const *> const &getParameters() const;
	std::string const &getPrototypeType() const;
	void setLineNumber(unsigned long lineNumber);
	unsigned long getLineNumber() const;
	virtual void accept(Visitor &visitor) const override;

private:
	std::string const _functionName;
	std::vector<const LocalVariableDeclarationAST *> const _parameters;
	AType const *_returnType;
	std::string const _prototypeType;
	unsigned long _lineNumber;
};

//-----------------------------------------------------------------------------
// FunctionDefinitionAST
//-----------------------------------------------------------------------------
class FunctionDefinitionAST : public BaseStatementAST
{
public:
	FunctionDefinitionAST(PrototypeAST const *prototype, BlockAST const *body);
	FunctionDefinitionAST(FunctionDefinitionAST const &function) = delete;
	FunctionDefinitionAST &operator=(FunctionDefinitionAST const &function) = delete;
	~FunctionDefinitionAST() override;
	std::string const &getFunctionName() const;
	AType const *getReturnType() const;
	std::vector<LocalVariableDeclarationAST const *> const &getParameters() const;
	PrototypeAST const *getPrototype() const;
	BlockAST const *getBody() const;
	void setLineNumber(unsigned long lineNumber);
	unsigned long getLineNumber() const;
	virtual void accept(Visitor &visitor) const override;

private:
	PrototypeAST const *_prototype;
	BlockAST const *_body;
	unsigned long _lineNumber;
};

//-----------------------------------------------------------------------------
// ReturnStatementAST
//-----------------------------------------------------------------------------
class ReturnStatementAST : public BaseStatementAST
{
public:
	ReturnStatementAST(BaseExpressionAST const *returnExpression);
	ReturnStatementAST(ReturnStatementAST const &returnStatement) = delete;
	ReturnStatementAST &operator=(ReturnStatementAST const &returnStatement) = delete;
	~ReturnStatementAST() override;
	void setLineNumber(unsigned long lineNumber);
	unsigned long getLineNumber() const;
	AType const *getScopeReturnType() const;
	void replaceType(AType const *type) const;
	BaseExpressionAST const *getReturnExpression() const;
	virtual void accept(Visitor &visitor) const override;

private:
	BaseExpressionAST const *_returnExpression;
	mutable AType const *_scopeReturnType;
	unsigned long _lineNumber;
};

//-----------------------------------------------------------------------------
// WhileAST
//-----------------------------------------------------------------------------
class WhileAST : public BaseStatementAST
{
public:
	WhileAST(BaseExpressionAST const *condition, BaseStatementAST const *block);
	WhileAST(WhileAST const &whileStatement) = delete;
	WhileAST &operator=(WhileAST const &whileStatement) = delete;
	~WhileAST() override;
	BaseExpressionAST const *getCondition() const;
	BaseStatementAST const *getBlock() const;
	void setLineNumber(unsigned long lineNumber);
	unsigned long getLineNumber() const;
	virtual void accept(Visitor &visitor) const override;

private:
	BaseExpressionAST const *_condition;
	BaseStatementAST const *_block;
	unsigned long _lineNumber;
};

//-----------------------------------------------------------------------------
// ForAST
//-----------------------------------------------------------------------------
class ForAST : public BaseStatementAST
{
public:
	ForAST(LocalVariableDeclarationAST const *counter, BaseExpressionAST const *startValue, BaseExpressionAST const *endValue, BaseStatementAST const *block);
	ForAST(ForAST const &forStatement) = delete;
	ForAST &operator=(ForAST const &forStatement) = delete;
	~ForAST() override;
	void setLineNumber(unsigned long lineNumber);
	unsigned long getLineNumber() const;
	BaseExpressionAST const *getStartValue() const;
	BaseExpressionAST const *getStopValue() const;
	BaseStatementAST const *getBlock() const;
	LocalVariableDeclarationAST const *getCounter() const;
	void replaceCounterType(AType const *counterType) const;
	virtual void accept(Visitor &visitor) const override;
	AType const *getCounterType() const;

private:
	mutable LocalVariableDeclarationAST const *_counter;
	mutable AType const *_counterType;
	BaseExpressionAST const *_startValue;
	BaseExpressionAST const *_stopValue;
	BaseStatementAST const *_block;
	unsigned long _lineNumber;
};

//-----------------------------------------------------------------------------
// IfThenElseAST
//-----------------------------------------------------------------------------
class IfThenElseAST : public BaseStatementAST
{
public:
	IfThenElseAST(BaseExpressionAST const *condition, BaseStatementAST const *thenBlock, BaseStatementAST const *elseBlock = nullptr);
	IfThenElseAST(IfThenElseAST const &ifThenElseStatement) = delete;
	IfThenElseAST &operator=(IfThenElseAST const &ifThenElseStatement) = delete;
	~IfThenElseAST() override;
	void setLineNumber(unsigned long lineNumber);
	unsigned long getLineNumber() const;
	BaseExpressionAST const *getCondition() const;
	BaseStatementAST const *getThenBlock() const;
	BaseStatementAST const *getElseBlock() const;
	virtual void accept(Visitor &visitor) const override;

private:
	BaseExpressionAST const *_condition;
	BaseStatementAST const *_thenBlock;
	BaseStatementAST const *_elseBlock;
	unsigned long _lineNumber;
};

//-----------------------------------------------------------------------------
// IOCommandAST
//-----------------------------------------------------------------------------
class IOCommandAST : public BaseStatementAST
{
public:
	IOCommandAST(std::string const &commandName, std::vector<BaseExpressionAST const *> const &pinList);
	IOCommandAST(std::string const &commandName, BaseExpressionAST const *pinExpression);
	IOCommandAST(std::string const &commandName);
	IOCommandAST(IOCommandAST const &command) = delete;
	IOCommandAST &operator=(IOCommandAST const &command) = delete;
	~IOCommandAST() override;
	void setLineNumber(unsigned long lineNumber);
	unsigned long getLineNumber() const;
	std::string const &getCommandName() const;
	std::vector<BaseExpressionAST const *> const &getPinList() const;
	BaseExpressionAST const *getPinExpression() const;
	virtual void accept(Visitor &visitor) const override;

private:
	std::string const _commandName;
	std::vector<BaseExpressionAST const *> const _pinList;
	BaseExpressionAST const *_pinExpression;
	unsigned long _lineNumber;
};

//-----------------------------------------------------------------------------
// FunctionCallStatementAST
//-----------------------------------------------------------------------------
class FunctionCallStatementAST : public BaseStatementAST
{
public:
	FunctionCallStatementAST(std::string const &functionName, std::vector<BaseExpressionAST const *> const arguments);
	FunctionCallStatementAST(FunctionCallStatementAST const &functionCall) = delete;
	FunctionCallStatementAST &operator=(FunctionCallStatementAST const &functionCall) = delete;
	~FunctionCallStatementAST() override;
	void setLineNumber(unsigned long lineNumber);
	unsigned long getLineNumber() const;
	std::string const &getFunctionName() const;
	std::vector<BaseExpressionAST const *> const &getArguments() const;
	void setPrototype(PrototypeAST const *prototype) const;
	PrototypeAST const *getPrototype() const;
	virtual void accept(Visitor &visitor) const override;

private:
	std::string const _functionName;
	std::vector<BaseExpressionAST const *> const _arguments;
	mutable PrototypeAST const *_prototype;
	unsigned long _lineNumber;
};

//-----------------------------------------------------------------------------
// LocalVariableDeclarationAST
//-----------------------------------------------------------------------------
class LocalVariableDeclarationAST : public BaseStatementAST
{
public:
	LocalVariableDeclarationAST(std::string const &name, AType const *type, BaseExpressionAST const *expression = nullptr);
	LocalVariableDeclarationAST(LocalVariableDeclarationAST const &variable) = delete;
	LocalVariableDeclarationAST &operator=(LocalVariableDeclarationAST const &variable) = delete;
	~LocalVariableDeclarationAST() override;
	llvm::AllocaInst *getAlloca() const;
	void setAlloca(llvm::AllocaInst *allocator) const;
	void replaceType(AType const *type) const;
	void setLineNumber(unsigned long lineNumber);
	unsigned long getLineNumber() const;
	std::string const &getName() const;
	AType const *getType() const;
	BaseExpressionAST const *getExpression() const;
	virtual void accept(Visitor &visitor) const override;

private:
	std::string const _name;
	mutable AType const *_type;
	BaseExpressionAST const *_expression;
	mutable llvm::AllocaInst *_localVariableAlloca;
	unsigned long _lineNumber;
};

//-----------------------------------------------------------------------------
// AssignmentAST
//-----------------------------------------------------------------------------
class AssignmentAST : public BaseStatementAST
{
public:
	AssignmentAST(std::string const &variableName, BaseExpressionAST const *expression);
	AssignmentAST(AssignmentAST const &assignment) = delete;
	AssignmentAST &operator=(AssignmentAST const &assignment) = delete;
	~AssignmentAST() override;
	void setLineNumber(unsigned long lineNumber);
	unsigned long getLineNumber() const;
	std::string const &getVaribleName() const;
	BaseExpressionAST const *getExpression() const;
	LocalVariableDeclarationAST const *getLocalVariableDeclaration() const;
	void setLocalVariableDeclaration(LocalVariableDeclarationAST const *localVariableDeclaration) const;
	virtual void accept(Visitor &visitor) const override;

private:
	std::string const _variableName;
	BaseExpressionAST const *_expression;
	mutable LocalVariableDeclarationAST const *_localVariableDeclaration;
	unsigned long _lineNumber;
};

//-----------------------------------------------------------------------------
// PointerAssignmentAST
//-----------------------------------------------------------------------------
class PointerAssignmentAST : public BaseStatementAST
{
public:
	PointerAssignmentAST(PointerContent const *pointerContent, BaseExpressionAST const *expression);
	PointerAssignmentAST(PointerAssignmentAST const &assignment) = delete;
	PointerAssignmentAST &operator=(PointerAssignmentAST const &assignment) = delete;
	~PointerAssignmentAST() override;
	void setLineNumber(unsigned long lineNumber);
	unsigned long getLineNumber() const;
	PointerContent const *getPointerContent() const;
	BaseExpressionAST const *getExpression() const;
	LocalVariableDeclarationAST const *getLocalVariableDeclaration() const;
	void setLocalVariableDeclaration(LocalVariableDeclarationAST const *localVariableDeclaration) const;
	virtual void accept(Visitor &visitor) const override;

private:
	PointerContent const *_pointerContent;
	BaseExpressionAST const *_expression;
	mutable LocalVariableDeclarationAST const *_localVariableDeclaration;
	unsigned long _lineNumber;
};

#endif // STATEMENT_AST_HPP
