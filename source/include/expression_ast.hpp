#ifndef EXPRESSION_AST_HPP
#define EXPRESSION_AST_HPP

class LocalVariableDeclarationAST;
class PrototypeAST;
class MainContext;
class AType;
class Visitor;
class LocalVariableDeclarationAST;
class PrototypeAST;
class PointerContent;

#include <iostream>
#include <vector>
#include <string>
#include <llvm/IR/Value.h>
#include "node_ast.hpp"

//-----------------------------------------------------------------------------
// BaseExpressionAST
//-----------------------------------------------------------------------------
class BaseExpressionAST : public BaseNodeAST
{
public:
	virtual ~BaseExpressionAST();
	virtual BaseExpressionAST *clone() const = 0;
	virtual AType const *getExpressionType() const = 0;
};

//-----------------------------------------------------------------------------
// BoolAST
//-----------------------------------------------------------------------------
class BoolAST : public BaseExpressionAST
{
public:
	BoolAST(bool const value);
	BoolAST(BoolAST const &boolAST);
	BoolAST &operator=(BoolAST const &boolAST) = delete;
	~BoolAST() override;
	virtual BaseExpressionAST *clone() const override;
	virtual AType const *getExpressionType() const override;
	virtual void accept(Visitor &visitor) const override;
	bool getValue() const;

private:
	bool const _value;
	AType const *_type;
};

//-----------------------------------------------------------------------------
// ByteAST
//-----------------------------------------------------------------------------
class ByteAST : public BaseExpressionAST
{
public:
	ByteAST(int8_t const value);
	ByteAST(ByteAST const &byteAST);
	ByteAST &operator=(ByteAST const &byteAST) = delete;
	~ByteAST() override;
	virtual BaseExpressionAST *clone() const override;
	virtual AType const *getExpressionType() const override;
	virtual void accept(Visitor &visitor) const override;
	int8_t getValue() const;

private:
	int8_t const _value;
	AType const *_type;
};

//-----------------------------------------------------------------------------
// WordAST
//-----------------------------------------------------------------------------
class WordAST : public BaseExpressionAST
{
public:
	WordAST(int16_t const value);
	WordAST(WordAST const &wordAST);
	WordAST &operator=(WordAST const &wordAST) = delete;
	~WordAST() override;
	virtual BaseExpressionAST *clone() const override;
	virtual AType const *getExpressionType() const override;
	virtual void accept(Visitor &visitor) const override;
	int16_t getValue() const;

private:
	int16_t const _value;
	AType const *_type;
};

//-----------------------------------------------------------------------------
// DWordAST
//-----------------------------------------------------------------------------
class DWordAST : public BaseExpressionAST
{
public:
	DWordAST(int32_t const value);
	DWordAST(DWordAST const &dwordAST);
	DWordAST &operator=(DWordAST const &dwordAST) = delete;
	~DWordAST() override;
	virtual BaseExpressionAST *clone() const override;
	virtual AType const *getExpressionType() const override;
	virtual void accept(Visitor &visitor) const override;
	int32_t getValue() const;

private:
	int32_t const _value;
	AType const *_type;
};

//-----------------------------------------------------------------------------
// RealAST
//-----------------------------------------------------------------------------
class RealAST : public BaseExpressionAST
{
public:
	RealAST(float const value);
	RealAST(RealAST const &realAST);
	RealAST &operator=(RealAST const &realAST) = delete;
	~RealAST() override;
	virtual BaseExpressionAST *clone() const override;
	virtual AType const *getExpressionType() const override;
	virtual void accept(Visitor &visitor) const override;
	float getValue() const;

private:
	float const _value;
	AType const *_type;
};

//-----------------------------------------------------------------------------
// BinaryOperatorAST
//-----------------------------------------------------------------------------
class BinaryOperatorAST : public BaseExpressionAST
{
public:
	BinaryOperatorAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode);
	BinaryOperatorAST(BinaryOperatorAST const &binaryOperator);
	BinaryOperatorAST &operator=(BinaryOperatorAST const &binaryOperator) = delete;
	~BinaryOperatorAST() override;
	BaseExpressionAST const *getLeftOperand() const;
	BaseExpressionAST const *getRightOperand() const;
	void replaceType(AType const *type) const;
	std::string const &getOperatorCode() const;
	virtual AType const *getExpressionType() const override;
	virtual void accept(Visitor &visitor) const override;
	void setLineNumber(unsigned long lineNumber);
	unsigned long getLineNumber() const;

protected:
	std::string const _operatorCode;
	BaseExpressionAST const *_left;
	BaseExpressionAST const *_right;
	mutable AType const *_type;
	unsigned long _lineNumber;
};

//-----------------------------------------------------------------------------
// ArithmeticBinaryOperatorAST
//-----------------------------------------------------------------------------
class ArithmeticBinaryOperatorAST : public BinaryOperatorAST
{
public:
	ArithmeticBinaryOperatorAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode);
	virtual void accept(Visitor &visitor) const override;
};

//-----------------------------------------------------------------------------
// AdditionAST
//-----------------------------------------------------------------------------
class AdditionAST : public ArithmeticBinaryOperatorAST
{
public:
	AdditionAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode);
	virtual BaseExpressionAST *clone() const override;
	virtual void accept(Visitor &visitor) const override;
};

//-----------------------------------------------------------------------------
// SubtractionAST
//-----------------------------------------------------------------------------
class SubtractionAST : public ArithmeticBinaryOperatorAST
{
public:
	SubtractionAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode);
	virtual void accept(Visitor &visitor) const override;
	virtual BaseExpressionAST *clone() const override;
};

//-----------------------------------------------------------------------------
// MultiplicationAST
//-----------------------------------------------------------------------------
class MultiplicationAST : public ArithmeticBinaryOperatorAST
{
public:
	MultiplicationAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode);
	virtual void accept(Visitor &visitor) const override;
	virtual BaseExpressionAST *clone() const override;
};

//-----------------------------------------------------------------------------
// DivisionAST
//-----------------------------------------------------------------------------
class DivisionAST : public ArithmeticBinaryOperatorAST
{
public:
	DivisionAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode);
	virtual void accept(Visitor &visitor) const override;
	virtual BaseExpressionAST *clone() const override;
};

//-----------------------------------------------------------------------------
// ModAST
//-----------------------------------------------------------------------------
class ModAST : public ArithmeticBinaryOperatorAST
{
public:
	ModAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode);
	virtual void accept(Visitor &visitor) const override;
	virtual BaseExpressionAST *clone() const override;
};

//-----------------------------------------------------------------------------
// RelationBinaryOperatorAST
//-----------------------------------------------------------------------------
class RelationBinaryOperatorAST : public BinaryOperatorAST
{
public:
	RelationBinaryOperatorAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode);
	virtual void accept(Visitor &visitor) const override;
};

//-----------------------------------------------------------------------------
// LessThanAST
//-----------------------------------------------------------------------------
class LessThanAST : public RelationBinaryOperatorAST
{
public:
	LessThanAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode);
	virtual void accept(Visitor &visitor) const override;
	virtual BaseExpressionAST *clone() const override;
};

//-----------------------------------------------------------------------------
// GreaterThanAST
//-----------------------------------------------------------------------------
class GreaterThanAST : public RelationBinaryOperatorAST
{
public:
	GreaterThanAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode);
	virtual void accept(Visitor &visitor) const override;
	virtual BaseExpressionAST *clone() const override;
};

//-----------------------------------------------------------------------------
// EqualToAST
//-----------------------------------------------------------------------------
class EqualToAST : public RelationBinaryOperatorAST
{
public:
	EqualToAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode);
	virtual void accept(Visitor &visitor) const override;
	virtual BaseExpressionAST *clone() const override;
};

//-----------------------------------------------------------------------------
// NotEqualToAST
//-----------------------------------------------------------------------------
class NotEqualToAST : public RelationBinaryOperatorAST
{
public:
	NotEqualToAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode);
	virtual void accept(Visitor &visitor) const override;
	virtual BaseExpressionAST *clone() const override;
};

//-----------------------------------------------------------------------------
// LogicBinaryOperatorAST
//-----------------------------------------------------------------------------
class LogicBinaryOperatorAST : public BinaryOperatorAST
{
public:
	LogicBinaryOperatorAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode);
	virtual void accept(Visitor &visitor) const override;
};

//-----------------------------------------------------------------------------
// AndAST
//-----------------------------------------------------------------------------
class AndAST : public LogicBinaryOperatorAST
{
public:
	AndAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode);
	virtual void accept(Visitor &visitor) const override;
	virtual BaseExpressionAST *clone() const override;
};

//-----------------------------------------------------------------------------
// OrAST
//-----------------------------------------------------------------------------
class OrAST : public LogicBinaryOperatorAST
{
public:
	OrAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode);
	virtual void accept(Visitor &visitor) const override;
	virtual BaseExpressionAST *clone() const override;
};

//-----------------------------------------------------------------------------
// EquivalentToAST
//-----------------------------------------------------------------------------
class EquivalentToAST : public LogicBinaryOperatorAST
{
public:
	EquivalentToAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode);
	virtual void accept(Visitor &visitor) const override;
	virtual BaseExpressionAST *clone() const override;
};

//-----------------------------------------------------------------------------
// UnaryOperatorAST
//-----------------------------------------------------------------------------
class UnaryOperatorAST : public BaseExpressionAST
{
public:
	UnaryOperatorAST(BaseExpressionAST const *operand, std::string const &operatorCode);
	UnaryOperatorAST(UnaryOperatorAST const &unaryOperator);
	UnaryOperatorAST &operator=(UnaryOperatorAST const &unaryOperator) = delete;
	~UnaryOperatorAST() override;
	virtual AType const *getExpressionType() const override;
	virtual void accept(Visitor &visitor) const override;
	BaseExpressionAST const *getOperand() const;
	std::string const &getOperatorCode() const;
	void replaceType(AType const *type) const;
	void setLineNumber(unsigned long lineNumber);
	unsigned long getLineNumber() const;

protected:
	std::string const _operatorCode;
	BaseExpressionAST const *_operand;
	mutable AType const *_type;
	unsigned long _lineNumber;
};

//-----------------------------------------------------------------------------
// LogicUnaryOperatorAST
//-----------------------------------------------------------------------------
class LogicUnaryOperatorAST : public UnaryOperatorAST
{
public:
	LogicUnaryOperatorAST(BaseExpressionAST const *operand, std::string const &operatorCode);
	virtual void accept(Visitor &visitor) const override;
};

//-----------------------------------------------------------------------------
// NotAST
//-----------------------------------------------------------------------------
class NotAST : public LogicUnaryOperatorAST
{
public:
	NotAST(BaseExpressionAST const *operand, std::string const &operatorCode);
	virtual void accept(Visitor &visitor) const override;
	virtual BaseExpressionAST *clone() const override;
};

//-----------------------------------------------------------------------------
// ArithmeticUnaryOperatorAST
//-----------------------------------------------------------------------------
class ArithmeticUnaryOperatorAST : public UnaryOperatorAST
{
public:
	ArithmeticUnaryOperatorAST(BaseExpressionAST const *operand, std::string const &operatorCode);
	virtual void accept(Visitor &visitor) const override;
};

//-----------------------------------------------------------------------------
// NegAST
//-----------------------------------------------------------------------------
class NegAST : public ArithmeticUnaryOperatorAST
{
public:
	NegAST(BaseExpressionAST const *operand, std::string const &operatorCode);
	virtual void accept(Visitor &visitor) const override;
	virtual BaseExpressionAST *clone() const override;
};

//-----------------------------------------------------------------------------
// FunctionCallExpressionAST
//-----------------------------------------------------------------------------
class FunctionCallExpressionAST : public BaseExpressionAST
{
public:
	FunctionCallExpressionAST(std::string const &functionName, std::vector<BaseExpressionAST const *> const &arguments);
	FunctionCallExpressionAST(FunctionCallExpressionAST const &functionCall);
	FunctionCallExpressionAST &operator=(FunctionCallExpressionAST const &functionCall) = delete;
	~FunctionCallExpressionAST() override;
	BaseExpressionAST *clone() const override;
	virtual AType const *getExpressionType() const override;
	std::string const &getFunctionName() const;
	std::vector<BaseExpressionAST const *> const &getArguments() const;
	const PrototypeAST *getPrototype() const;
	void setPrototype(PrototypeAST const *prototype) const;
	void setLineNumber(unsigned long lineNumber);
	unsigned long getLineNumber() const;
	virtual void accept(Visitor &visitor) const override;

private:
	std::string const _functionName;
	std::vector<BaseExpressionAST const *> _arguments;
	mutable PrototypeAST const *_prototype;
	unsigned long _lineNumber;
};

//-----------------------------------------------------------------------------
// LocalVariableAccessAST
//-----------------------------------------------------------------------------
class LocalVariableAccessAST : public BaseExpressionAST
{
public:
	LocalVariableAccessAST(std::string const &name);
	LocalVariableAccessAST(LocalVariableAccessAST const &variableAccess);
	LocalVariableAccessAST &operator=(LocalVariableAccessAST const &variableAcess) = delete;
	virtual BaseExpressionAST *clone() const override;
	virtual AType const *getExpressionType() const override;
	LocalVariableDeclarationAST const *getLocalVariableDeclaration() const;
	void setLineNumber(unsigned long lineNumber);
	unsigned long getLineNumber() const;
	std::string const &getName() const;
	void setLocalVariableDeclaration(LocalVariableDeclarationAST const *localVariableDeclaration) const;
	virtual void accept(Visitor &visitor) const override;

private:
	std::string const _name;
	mutable LocalVariableDeclarationAST const *_localVariableDeclaration;
	unsigned long _lineNumber;
};

//-----------------------------------------------------------------------------
// LocalPointerAccessAST
//-----------------------------------------------------------------------------
class LocalPointerAccessAST : public BaseExpressionAST
{
public:
	LocalPointerAccessAST(PointerContent const *pointerContent);
	LocalPointerAccessAST(LocalPointerAccessAST const &localPointerAccess);
	LocalPointerAccessAST &operator=(LocalPointerAccessAST const &localPointerAccess) = delete;
	~LocalPointerAccessAST() override;
	virtual BaseExpressionAST *clone() const override;
	virtual AType const *getExpressionType() const override;
	PointerContent const *getPointerContent() const;
	LocalVariableDeclarationAST const *getLocalVariableDeclaration() const;
	void setLocalVariableDeclaration(LocalVariableDeclarationAST const *localVariableDeclaration) const;
	void setLineNumber(unsigned long lineNumber);
	unsigned long getLineNumber() const;
	void replaceType(AType const *type) const;
	virtual void accept(Visitor &visitor) const override;

private:
	PointerContent const *_pointerContent;
	mutable AType const *_type;
	mutable LocalVariableDeclarationAST const *_localVariableDeclaration;
	unsigned long _lineNumber;
};

//-----------------------------------------------------------------------------
// LocalVariableAddressAST
//-----------------------------------------------------------------------------
class LocalVariableAddressAST : public BaseExpressionAST
{
public:
	LocalVariableAddressAST(std::string const &variableName, BaseExpressionAST const *index = nullptr);
	LocalVariableAddressAST(LocalVariableAddressAST const &localVariableAddress);
	LocalVariableAddressAST &operator=(LocalVariableAddressAST const &localVariableAddress) = delete;
	~LocalVariableAddressAST() override;
	virtual AType const *getExpressionType() const override;
	virtual BaseExpressionAST *clone() const override;
	LocalVariableDeclarationAST const *getLocalVariableDeclaration() const;
	void setLocalVariableDeclaration(LocalVariableDeclarationAST const *localVariableDeclaration) const;
	std::string const &getName() const;
	void setLineNumber(unsigned long lineNumber);
	unsigned long getLineNumber() const;
	void replaceType(AType const *type) const;
	virtual void accept(Visitor &visitor) const override;
	BaseExpressionAST const *getIndex() const;

private:
	std::string const _name;
	mutable AType const *_type;
	BaseExpressionAST const *_index;
	mutable LocalVariableDeclarationAST const *_localVariableDeclaration;
	unsigned long _lineNumber;
};

//-----------------------------------------------------------------------------
// ExpressionCommandAST
//-----------------------------------------------------------------------------
class ExpressionCommandAST : public BaseExpressionAST
{
public:
	ExpressionCommandAST(std::string const &commandName, BaseExpressionAST const *pinNumber = nullptr);
	ExpressionCommandAST(ExpressionCommandAST const &command);
	ExpressionCommandAST &operator=(ExpressionCommandAST const &command) = delete;
	~ExpressionCommandAST() override;
	virtual AType const *getExpressionType() const override;
	virtual BaseExpressionAST *clone() const override;
	void setLineNumber(unsigned long lineNumber);
	unsigned long getLineNumber() const;
	BaseExpressionAST const *getPinNumber() const;
	std::string const &getCommandName() const;
	void replaceType(AType const *type) const;
	virtual void accept(Visitor &visitor) const override;

private:
	std::string const _commandName;
	BaseExpressionAST const *_pinNumber;
	mutable AType const *_type;
	unsigned long _lineNumber;
};

#endif // EXPRESSION_AST_HPP
