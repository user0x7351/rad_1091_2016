#ifndef DEBUG_HPP
#define DEBUG_HPP

//-----------------------------------------------------------------------------

#define RST     "\x1B[0m"
#define KRED    "\x1B[31m"
#define KGRN    "\x1B[32m"
#define KYEL    "\x1B[33m"

#define FRED(x) KRED x RST
#define FGRN(x) KGRN x RST
#define FYEL(x) KYEL x RST

//-----------------------------------------------------------------------------

// uncomment to see debug messages
//#define AKCENT_DEBUG

//-----------------------------------------------------------------------------

#define YYDEBUG 1

//-----------------------------------------------------------------------------

#endif // DEBUG_HPP
