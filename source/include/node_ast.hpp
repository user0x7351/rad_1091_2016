#ifndef NODE_AST_HPP
#define NODE_AST_HPP

#include "../include/visitor.hpp"

//-----------------------------------------------------------------------------
// BaseNodeAST
//-----------------------------------------------------------------------------
class BaseNodeAST
{
public:
	virtual ~BaseNodeAST();
	virtual void accept(Visitor &visitor) const = 0;
};

#endif // NODE_AST_HPP
