#include <string>
#include "../include/pointer_content.hpp"
#include "../include/expression_ast.hpp"

PointerContent::PointerContent(std::string const *identifier, BaseExpressionAST const *index)
    : _identifier {*identifier},
      _referenceNumber {0},
      _index {index->clone()}
{}

PointerContent::PointerContent(const PointerContent &pointerContent)
    : _identifier {pointerContent._identifier},
      _referenceNumber {pointerContent._referenceNumber},
      _index {(pointerContent._index)->clone()}
{}

PointerContent::~PointerContent()
{
    delete _index;
}

PointerContent *PointerContent::clone() const
{
    return new PointerContent(*this);
}

void PointerContent::incrementReferenceNumber()
{
    ++_referenceNumber;
}

unsigned short PointerContent::getReferenceNumber() const
{
    return _referenceNumber;
}

std::string const &PointerContent::getIdentifier() const
{
    return _identifier;
}

BaseExpressionAST const *PointerContent::getIndex() const
{
    return _index;
}
