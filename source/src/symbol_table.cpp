#include "../include/symbol_table.hpp"
#include "../include/scope.hpp"
#include "../include/statement_ast.hpp"

void SymbolTable::registerNewScope(PrototypeAST const *prototype)
{
    if (prototype != nullptr)
    {
        _scopes.push_back(new Scope(prototype));
    }
    else
    {
        _scopes.push_back(new Scope((_scopes.back())->getPrototype()));
    }
}

void SymbolTable::destroyCurrentScope()
{
    delete _scopes.back();
    _scopes.pop_back();
}

AType const *SymbolTable::getCurrentScopeReturnType() const
{
    return (_scopes.back())->getReturnType();
}

bool SymbolTable::registerPrototype(PrototypeAST const *prototype)
{
    for (PrototypeAST const *proto : _prototypes)
    {
        if (proto->getFunctionName() == prototype->getFunctionName())
        {
            return false;
        }
    }

    _prototypes.push_back(prototype);

    return true;
}

PrototypeAST const *SymbolTable::getPrototypeFromSymbolTable(std::string const &functionName) const
{
    for (PrototypeAST const *prototype : _prototypes)
    {
        if (prototype->getFunctionName() == functionName)
        {
            return prototype;
        }
    }

    return nullptr;
}

bool SymbolTable::registerVariable(LocalVariableDeclarationAST const *localVariableDeclaration)
{
    for (LocalVariableDeclarationAST const *variable : (_scopes.back())->getSymbolTable())
    {
        if (localVariableDeclaration->getName() == variable->getName())
        {
            return false;
        }
    }

    (_scopes.back())->updateSymbolTable().push_back(localVariableDeclaration);

    return true;
}

LocalVariableDeclarationAST const *SymbolTable::getLocalVariableFromSymbolTable(std::string const &variableName) const
{
    for (std::vector<Scope *>::const_reverse_iterator crit = _scopes.rbegin(); crit != _scopes.rend(); ++crit)
    {
        for (LocalVariableDeclarationAST const *variable : (*crit)->getSymbolTable())
        {
            if (variableName == variable->getName())
            {
                return variable;
            }
        }
    }

    return nullptr;
}
