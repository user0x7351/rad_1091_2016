#include "../include/prototype_register_visitor.hpp"
#include "../include/statement_ast.hpp"
#include "../include/expression_ast.hpp"
#include "../include/main_context.hpp"
#include "../include/symbol_table.hpp"
#include "../include/debug.hpp"
#include "../include/atype.hpp"

#define SEMANTIC_ERROR this->storeSemanticState(false); return;
#define SEMANTIC_CORRECT this->storeSemanticState(true);

PrototypeRegisterVisitor::PrototypeRegisterVisitor()
	: _mainContext {MainContext::GetInstance()}
{}

PrototypeRegisterVisitor::~PrototypeRegisterVisitor()
{}

void PrototypeRegisterVisitor::visit(BlockAST const *node)
{
	bool status = true;

	for (BaseStatementAST const *statementPtr : node->getStatements())
		status = status && this->getSemanticState(statementPtr);

	this->storeSemanticState(status);
}

void PrototypeRegisterVisitor::visit(PrototypeAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Registering PrototypeAST" << std::endl;
#endif

	if (_mainContext.updateSymbolTable().registerPrototype(node) == false)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("function declaration error: ")
				  << "duplicate function declaration: " <<  node->getFunctionName() << "(...)"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	_mainContext.updateSymbolTable().registerNewScope(node);
	for (LocalVariableDeclarationAST const *parameter : node->getParameters())
	{
		if (_mainContext.updateSymbolTable().registerVariable(parameter) == false)
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FRED("prototype parameter error: ")
					  << "duplicate parameter declaration: " << parameter->getName()
					  << " in the " << node->getFunctionName() << "(...)"
					  << std::endl;

			SEMANTIC_ERROR;
		}
	}
	_mainContext.updateSymbolTable().destroyCurrentScope();

	SEMANTIC_CORRECT;

	std::vector<llvm::Type *> parameters;
	for (auto it = node->getParameters().begin(); it != node->getParameters().end(); ++it)
	{
		parameters.push_back(((*it)->getType())->getRawType(_mainContext));
	}

	llvm::FunctionType *returnType = llvm::FunctionType::get(node->getReturnType()->getRawType(_mainContext), parameters, false);
	llvm::Function::Create(returnType, llvm::Function::ExternalLinkage, node->getFunctionName(), _mainContext.getLLVMModule());
}

void PrototypeRegisterVisitor::visit(FunctionDefinitionAST const *node)
{
	this->storeSemanticState(this->getSemanticState(node->getPrototype()));
}

void PrototypeRegisterVisitor::visit(BoolAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(WordAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(DWordAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(ByteAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(RealAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(BinaryOperatorAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(ArithmeticBinaryOperatorAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(ArithmeticUnaryOperatorAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(RelationBinaryOperatorAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(LogicBinaryOperatorAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(UnaryOperatorAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(LogicUnaryOperatorAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(AdditionAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(SubtractionAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(MultiplicationAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(DivisionAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(ModAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(LessThanAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(GreaterThanAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(EqualToAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(NotEqualToAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(AndAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(OrAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(NotAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(NegAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(EquivalentToAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(FunctionCallExpressionAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(LocalVariableAccessAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(LocalPointerAccessAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(LocalVariableAddressAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(ExpressionCommandAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(ReturnStatementAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(WhileAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(ForAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(IfThenElseAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(IOCommandAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(FunctionCallStatementAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(LocalVariableDeclarationAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(AssignmentAST const *node __attribute__((unused)))
{}

void PrototypeRegisterVisitor::visit(PointerAssignmentAST const *node __attribute__((unused)))
{}

bool PrototypeRegisterVisitor::getSemanticState(BaseNodeAST const *node)
{
	node->accept(*this);
	return _state;
}

bool PrototypeRegisterVisitor::getCurrentSemanticState()
{
	return _state;
}

void PrototypeRegisterVisitor::storeSemanticState(bool state)
{
	_state = state;
}
