#include "../include/expression_ast.hpp"
#include "../include/main_context.hpp"
#include "../include/atype.hpp"
#include "../include/visitor.hpp"
#include "../include/debug.hpp"
#include "../include/statement_ast.hpp"
#include "../include/pointer_content.hpp"

//-----------------------------------------------------------------------------
// BaseExpressionAST
//-----------------------------------------------------------------------------
BaseExpressionAST::~BaseExpressionAST()
{}

//-----------------------------------------------------------------------------
// BoolAST
//-----------------------------------------------------------------------------
BoolAST::BoolAST(bool const value)
	: _value {value},
	  _type {new ABool()}
{}

BoolAST::~BoolAST()
{
	delete _type;
}

BoolAST::BoolAST(BoolAST const &boolAST)
	: _value {boolAST._value},
	  _type {(boolAST._type)->clone()}
{}

BaseExpressionAST *BoolAST::clone() const
{
	return new BoolAST(*this);
}

AType const *BoolAST::getExpressionType() const
{
	return _type;
}

void BoolAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

bool BoolAST::getValue() const
{
	return _value;
}

//-----------------------------------------------------------------------------
// ByteAST
//-----------------------------------------------------------------------------
ByteAST::ByteAST(int8_t const value)
	: _value {value},
	  _type {new AByte()}
{}

ByteAST::~ByteAST()
{
	delete _type;
}

ByteAST::ByteAST(ByteAST const &byteAST)
	: _value {byteAST._value},
	  _type {(byteAST._type)->clone()}
{}

BaseExpressionAST *ByteAST::clone() const
{
	return new ByteAST(*this);
}

AType const *ByteAST::getExpressionType() const
{
	return _type;
}

void ByteAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

int8_t ByteAST::getValue() const
{
	return _value;
}

//-----------------------------------------------------------------------------
// WordAST
//-----------------------------------------------------------------------------
WordAST::WordAST(int16_t const value)
	: _value {value},
	  _type {new AWord()}
{}

WordAST::~WordAST()
{
	delete _type;
}

WordAST::WordAST(WordAST const &wordAST)
	: _value {wordAST._value},
	  _type {(wordAST._type)->clone()}
{}

BaseExpressionAST *WordAST::clone() const
{
	return new WordAST(*this);
}

AType const *WordAST::getExpressionType() const
{
	return _type;
}

void WordAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

int16_t WordAST::getValue() const
{
	return _value;
}

//-----------------------------------------------------------------------------
// DWordAST
//-----------------------------------------------------------------------------
DWordAST::DWordAST(int32_t const value)
	: _value {value},
	  _type {new ADword()}
{}

DWordAST::~DWordAST()
{
	delete _type;
}

DWordAST::DWordAST(DWordAST const &dwordAST)
	: _value {dwordAST._value},
	  _type {(dwordAST._type)->clone()}
{}

BaseExpressionAST *DWordAST::clone() const
{
	return new DWordAST(*this);
}

AType const *DWordAST::getExpressionType() const
{
	return _type;
}

void DWordAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

int32_t DWordAST::getValue() const
{
	return _value;
}

//-----------------------------------------------------------------------------
// RealAST
//-----------------------------------------------------------------------------
RealAST::RealAST(float const value)
	: _value {value},
	  _type {new AReal()}
{}

RealAST::~RealAST()
{
	delete _type;
}

RealAST::RealAST(RealAST const &realAST)
	: _value {realAST._value},
	  _type {(realAST._type)->clone()}
{}

BaseExpressionAST *RealAST::clone() const
{
	return new RealAST(*this);
}

AType const *RealAST::getExpressionType() const
{
	return _type;
}

void RealAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

float RealAST::getValue() const
{
	return _value;
}

//-----------------------------------------------------------------------------
// BinaryOperatorAST
//-----------------------------------------------------------------------------
BinaryOperatorAST::BinaryOperatorAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode)
	: _operatorCode {operatorCode},
	  _left {left->clone()},
	  _right {right->clone()},
	  _type {new AUnknown()}
{}

BinaryOperatorAST::~BinaryOperatorAST()
{
	delete _left;
	delete _right;
	delete _type;
}

BinaryOperatorAST::BinaryOperatorAST(BinaryOperatorAST const &binaryOperator)
	: _operatorCode {binaryOperator._operatorCode},
	  _left {(binaryOperator._left)->clone()},
	  _right {(binaryOperator._right)->clone()},
	  _type {(binaryOperator._type)->clone()},
	  _lineNumber {binaryOperator._lineNumber}
{}

AType const *BinaryOperatorAST::getExpressionType() const
{
	return _type;
}

void BinaryOperatorAST::setLineNumber(unsigned long lineNumber)
{
	_lineNumber = lineNumber;
}

void BinaryOperatorAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

BaseExpressionAST const *BinaryOperatorAST::getLeftOperand() const
{
	return _left;
}

BaseExpressionAST const *BinaryOperatorAST::getRightOperand() const
{
	return _right;
}

void BinaryOperatorAST::replaceType(AType const *type) const
{
	delete _type;
	_type = type;
}

unsigned long BinaryOperatorAST::getLineNumber() const
{
	return _lineNumber;
}

std::string const &BinaryOperatorAST::getOperatorCode() const
{
	return _operatorCode;
}

//-----------------------------------------------------------------------------
// UnaryOperatorAST
//-----------------------------------------------------------------------------
UnaryOperatorAST::UnaryOperatorAST(BaseExpressionAST const *operand, std::string const &operatorCode)
	: _operatorCode {operatorCode},
	  _operand {operand->clone()},
	  _type {new AUnknown()}
{}

UnaryOperatorAST::~UnaryOperatorAST()
{
	delete _operand;
	delete _type;
}

UnaryOperatorAST::UnaryOperatorAST(UnaryOperatorAST const &unaryOperator)
	: _operatorCode {unaryOperator._operatorCode},
	  _operand {(unaryOperator._operand)->clone()},
	  _type {(unaryOperator._type)->clone()},
	  _lineNumber {unaryOperator._lineNumber}
{}

AType const *UnaryOperatorAST::getExpressionType() const
{
	return _type;
}

void UnaryOperatorAST::setLineNumber(unsigned long lineNumber)
{
	_lineNumber = lineNumber;
}

unsigned long UnaryOperatorAST::getLineNumber() const
{
	return _lineNumber;
}

BaseExpressionAST const *UnaryOperatorAST::getOperand() const
{
	return _operand;
}

void UnaryOperatorAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

std::string const &UnaryOperatorAST::getOperatorCode() const
{
	return _operatorCode;
}

void UnaryOperatorAST::replaceType(AType const *type) const
{
	delete _type;
	_type = type;
}

//-----------------------------------------------------------------------------
// ArithmeticBinaryOperatorAST
//-----------------------------------------------------------------------------
ArithmeticBinaryOperatorAST::ArithmeticBinaryOperatorAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode)
	: BinaryOperatorAST {left, right, operatorCode}
{}

void ArithmeticBinaryOperatorAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// RelationBinaryOperatorAST
//-----------------------------------------------------------------------------
RelationBinaryOperatorAST::RelationBinaryOperatorAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode)
	: BinaryOperatorAST {left, right, operatorCode}
{}

void RelationBinaryOperatorAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// LogicBinaryOperatorAST
//-----------------------------------------------------------------------------
LogicBinaryOperatorAST::LogicBinaryOperatorAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode)
	: BinaryOperatorAST {left, right, operatorCode}
{}

void LogicBinaryOperatorAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// LogicUnaryOperatorAST
//-----------------------------------------------------------------------------
LogicUnaryOperatorAST::LogicUnaryOperatorAST(BaseExpressionAST const *operand, std::string const &operatorCode)
	: UnaryOperatorAST {operand, operatorCode}
{}

void LogicUnaryOperatorAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// LogicUnaryOperatorAST
//-----------------------------------------------------------------------------
ArithmeticUnaryOperatorAST::ArithmeticUnaryOperatorAST(BaseExpressionAST const *operand, std::string const &operatorCode)
	: UnaryOperatorAST {operand, operatorCode}
{}

void ArithmeticUnaryOperatorAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// AdditionAST
//-----------------------------------------------------------------------------
AdditionAST::AdditionAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode)
	: ArithmeticBinaryOperatorAST {left, right, operatorCode}
{}

BaseExpressionAST *AdditionAST::clone() const
{
	return new AdditionAST(*this);
}

void AdditionAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// SubtractionAST
//-----------------------------------------------------------------------------
SubtractionAST::SubtractionAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode)
	: ArithmeticBinaryOperatorAST {left, right, operatorCode}
{}

BaseExpressionAST *SubtractionAST::clone() const
{
	return new SubtractionAST(*this);
}

void SubtractionAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// MultiplicationAST
//-----------------------------------------------------------------------------
MultiplicationAST::MultiplicationAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode)
	: ArithmeticBinaryOperatorAST {left, right, operatorCode}
{}

BaseExpressionAST *MultiplicationAST::clone() const
{
	return new MultiplicationAST(*this);
}

void MultiplicationAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// DivisionAST
//-----------------------------------------------------------------------------
DivisionAST::DivisionAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode)
	: ArithmeticBinaryOperatorAST {left, right, operatorCode}
{}

BaseExpressionAST *DivisionAST::clone() const
{
	return new DivisionAST(*this);
}

void DivisionAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// ModAST
//-----------------------------------------------------------------------------
ModAST::ModAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode)
	: ArithmeticBinaryOperatorAST {left, right, operatorCode}
{}

BaseExpressionAST *ModAST::clone() const
{
	return new ModAST(*this);
}

void ModAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// LessThanAST
//-----------------------------------------------------------------------------
LessThanAST::LessThanAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode)
	: RelationBinaryOperatorAST {left, right, operatorCode}
{}

BaseExpressionAST *LessThanAST::clone() const
{
	return new LessThanAST(*this);
}

void LessThanAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// GreaterThanAST
//-----------------------------------------------------------------------------
GreaterThanAST::GreaterThanAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode)
	: RelationBinaryOperatorAST {left, right, operatorCode}
{}

BaseExpressionAST *GreaterThanAST::clone() const
{
	return new GreaterThanAST(*this);
}

void GreaterThanAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// EqualToAST
//-----------------------------------------------------------------------------
EqualToAST::EqualToAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode)
	: RelationBinaryOperatorAST {left, right, operatorCode}
{}

BaseExpressionAST *EqualToAST::clone() const
{
	return new EqualToAST(*this);
}

void EqualToAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// NotEqualToAST
//-----------------------------------------------------------------------------
NotEqualToAST::NotEqualToAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode)
	: RelationBinaryOperatorAST {left, right, operatorCode}
{}

BaseExpressionAST *NotEqualToAST::clone() const
{
	return new NotEqualToAST(*this);
}

void NotEqualToAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// EquivalentToAST
//-----------------------------------------------------------------------------
EquivalentToAST::EquivalentToAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode)
	: LogicBinaryOperatorAST {left, right, operatorCode}
{}

BaseExpressionAST *EquivalentToAST::clone() const
{
	return new EquivalentToAST(*this);
}

void EquivalentToAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// AndAST
//-----------------------------------------------------------------------------
AndAST::AndAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode)
	: LogicBinaryOperatorAST {left, right, operatorCode}
{}

BaseExpressionAST *AndAST::clone() const
{
	return new AndAST(*this);
}

void AndAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// OrAST
//-----------------------------------------------------------------------------
OrAST::OrAST(BaseExpressionAST const *left, BaseExpressionAST const *right, std::string const &operatorCode)
	: LogicBinaryOperatorAST {left, right, operatorCode}
{}

BaseExpressionAST *OrAST::clone() const
{
	return new OrAST(*this);
}

void OrAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// NotAST
//-----------------------------------------------------------------------------
NotAST::NotAST(BaseExpressionAST const *operand, std::string const &operatorCode)
	: LogicUnaryOperatorAST {operand, operatorCode}
{}

BaseExpressionAST *NotAST::clone() const
{
	return new NotAST(*this);
}

void NotAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// NegAST
//-----------------------------------------------------------------------------
NegAST::NegAST(BaseExpressionAST const *operand, std::string const &operatorCode)
	: ArithmeticUnaryOperatorAST {operand, operatorCode}
{}

BaseExpressionAST *NegAST::clone() const
{
	return new NegAST(*this);
}

void NegAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// FunctionCallExpressionAST
//-----------------------------------------------------------------------------
FunctionCallExpressionAST::FunctionCallExpressionAST(std::string const &functionName, std::vector<BaseExpressionAST const *> const &arguments)
	: _functionName {functionName},
	  _arguments {arguments}
{}

FunctionCallExpressionAST::~FunctionCallExpressionAST()
{
	for (BaseExpressionAST const *argument : _arguments)
		delete argument;
}

FunctionCallExpressionAST::FunctionCallExpressionAST(FunctionCallExpressionAST const &functionCall)
	: _functionName {functionCall._functionName},
	  _lineNumber {functionCall._lineNumber}
{
	for (BaseExpressionAST const *arg : functionCall._arguments)
		_arguments.push_back(arg->clone());
}

BaseExpressionAST *FunctionCallExpressionAST::clone() const
{
	return new FunctionCallExpressionAST(*this);
}

AType const *FunctionCallExpressionAST::getExpressionType() const
{
	return _prototype->getReturnType();
}

std::string const &FunctionCallExpressionAST::getFunctionName() const
{
	return _functionName;
}

std::vector<BaseExpressionAST const *> const &FunctionCallExpressionAST::getArguments() const
{
	return _arguments;
}

PrototypeAST const *FunctionCallExpressionAST::getPrototype() const
{
	return _prototype;
}

void FunctionCallExpressionAST::setPrototype(PrototypeAST const *prototype) const
{
	_prototype = prototype;
}

void FunctionCallExpressionAST::setLineNumber(unsigned long lineNumber)
{
	_lineNumber = lineNumber;
}

unsigned long FunctionCallExpressionAST::getLineNumber() const
{
	return _lineNumber;
}

void FunctionCallExpressionAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// LocalVariableAccessAST
//-----------------------------------------------------------------------------
LocalVariableAccessAST::LocalVariableAccessAST(std::string const &name)
	: _name {name}
{}

LocalVariableAccessAST::LocalVariableAccessAST(LocalVariableAccessAST const &variableAccess)
	: _name {variableAccess._name},
	  _lineNumber {variableAccess._lineNumber}
{}

BaseExpressionAST *LocalVariableAccessAST::clone() const
{
	return new LocalVariableAccessAST(*this);
}

AType const *LocalVariableAccessAST::getExpressionType() const
{
	return _localVariableDeclaration->getType();
}

LocalVariableDeclarationAST const *LocalVariableAccessAST::getLocalVariableDeclaration() const
{
	return _localVariableDeclaration;
}

void LocalVariableAccessAST::setLineNumber(unsigned long lineNumber)
{
	_lineNumber = lineNumber;
}

unsigned long LocalVariableAccessAST::getLineNumber() const
{
	return _lineNumber;
}

std::string const &LocalVariableAccessAST::getName() const
{
	return _name;
}

void LocalVariableAccessAST::setLocalVariableDeclaration(LocalVariableDeclarationAST const *localVariableDeclaration) const
{
	_localVariableDeclaration = localVariableDeclaration;
}

void LocalVariableAccessAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// LocalPointerAccessAST
//-----------------------------------------------------------------------------
LocalPointerAccessAST::LocalPointerAccessAST(PointerContent const *pointerContent)
	: _pointerContent {pointerContent->clone()},
	  _type {new AUnknown()}
{}

LocalPointerAccessAST::LocalPointerAccessAST(LocalPointerAccessAST const &localPointerAccess)
	: _pointerContent {(localPointerAccess._pointerContent)->clone()},
	  _type {(localPointerAccess._type)->clone()},
	  _lineNumber {localPointerAccess._lineNumber}
{}

LocalPointerAccessAST::~LocalPointerAccessAST()
{
	delete _pointerContent;
	delete _type;
}

BaseExpressionAST *LocalPointerAccessAST::clone() const
{
	return new LocalPointerAccessAST(*this);
}

AType const *LocalPointerAccessAST::getExpressionType() const
{
	return _type;
}

PointerContent const *LocalPointerAccessAST::getPointerContent() const
{
	return _pointerContent;
}

LocalVariableDeclarationAST const *LocalPointerAccessAST::getLocalVariableDeclaration() const
{
	return _localVariableDeclaration;
}

void LocalPointerAccessAST::setLocalVariableDeclaration(LocalVariableDeclarationAST const *localVariableDeclaration) const
{
	_localVariableDeclaration = localVariableDeclaration;
}

void LocalPointerAccessAST::setLineNumber(unsigned long lineNumber)
{
	_lineNumber = lineNumber;
}

unsigned long LocalPointerAccessAST::getLineNumber() const
{
	return _lineNumber;
}

void LocalPointerAccessAST::replaceType(AType const *type) const
{
	delete _type;
	_type = type;
}

void LocalPointerAccessAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// LocalVariableAddressAST
//-----------------------------------------------------------------------------
LocalVariableAddressAST::LocalVariableAddressAST(std::string const &name, BaseExpressionAST const *index)
	: _name {name},
	  _type {new AUnknown()},
	  _index {index == nullptr ? nullptr : index->clone()}
{}

LocalVariableAddressAST::LocalVariableAddressAST(LocalVariableAddressAST const &localVariableAddress)
	: _name {localVariableAddress._name},
	  _type {(localVariableAddress._type)->clone()},
	  _index {localVariableAddress._index == nullptr ? nullptr : (localVariableAddress._index)->clone()},
	  _lineNumber {localVariableAddress._lineNumber}
{}

LocalVariableAddressAST::~LocalVariableAddressAST()
{
	delete _type;

	if (_index != nullptr)
		delete _index;
}

BaseExpressionAST *LocalVariableAddressAST::clone() const
{
	return new LocalVariableAddressAST(*this);
}

AType const *LocalVariableAddressAST::getExpressionType() const
{
	return _type;
}

LocalVariableDeclarationAST const *LocalVariableAddressAST::getLocalVariableDeclaration() const
{
	return _localVariableDeclaration;
}

void LocalVariableAddressAST::setLocalVariableDeclaration(LocalVariableDeclarationAST const *localVariableDeclaration) const
{
	_localVariableDeclaration = localVariableDeclaration;
}

std::string const &LocalVariableAddressAST::getName() const
{
	return _name;
}

void LocalVariableAddressAST::setLineNumber(unsigned long lineNumber)
{
	_lineNumber = lineNumber;
}

unsigned long LocalVariableAddressAST::getLineNumber() const
{
	return _lineNumber;
}

void LocalVariableAddressAST::replaceType(AType const *type) const
{
	delete _type;
	_type = type;
}

void LocalVariableAddressAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

BaseExpressionAST const *LocalVariableAddressAST::getIndex() const
{
	return _index;
}

//-----------------------------------------------------------------------------
// ExpressionCommandAST
//-----------------------------------------------------------------------------
ExpressionCommandAST::ExpressionCommandAST(std::string const &commandName, BaseExpressionAST const *pinNumber)
	: _commandName {commandName},
	  _type {new AUnknown()}
{
	if (_commandName == "read" || _commandName == "adc")
		_pinNumber = pinNumber->clone();
}

ExpressionCommandAST::ExpressionCommandAST(ExpressionCommandAST const &command)
	: _commandName {command._commandName},
	  _type {(command._type)->clone()},
	  _lineNumber {command._lineNumber}
{
	if (_commandName == "read" || _commandName == "adc")
		_pinNumber = (command._pinNumber)->clone();
}

ExpressionCommandAST::~ExpressionCommandAST()
{
	if (_commandName == "read" || _commandName == "adc")
		delete _pinNumber;

	delete _type;
}

BaseExpressionAST *ExpressionCommandAST::clone() const
{
	return new ExpressionCommandAST(*this);
}

AType const *ExpressionCommandAST::getExpressionType() const
{
	return _type;
}

void ExpressionCommandAST::setLineNumber(unsigned long lineNumber)
{
	_lineNumber = lineNumber;
}

unsigned long ExpressionCommandAST::getLineNumber() const
{
	return _lineNumber;
}

BaseExpressionAST const *ExpressionCommandAST::getPinNumber() const
{
	return _pinNumber;
}

std::string const &ExpressionCommandAST::getCommandName() const
{
	return _commandName;
}

void ExpressionCommandAST::replaceType(AType const *type) const
{
	delete _type;
	_type = type;
}

void ExpressionCommandAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}
