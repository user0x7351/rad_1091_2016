#include "../include/atype.hpp"
#include "../include/main_context.hpp"
#include "../include/expression_ast.hpp"

//-----------------------------------------------------------------------------
// AType
//-----------------------------------------------------------------------------
AType::~AType()
{}

bool AType::areCompatible(AType const *left, AType const *right, bool isPointerType)
{
    if ((left->getTypeTag() == AType::T_VOID) || (right->getTypeTag() == AType::T_VOID))
        return false;

    if (left->getTypeClass() != right->getTypeClass())
        return false;

    if (isPointerType == true)
        if (left->getTypeTag() != right->getTypeTag())
            return false;

    if (((left->getTypeTag() == AType::T_POINTER || left->getTypeTag() == AType::T_ARRAY)  && (right->getTypeTag() == AType::T_POINTER || right->getTypeTag() == AType::T_ARRAY)))
        if (areCompatible(static_cast<APointer const *>(left)->getPointee(), static_cast<APointer const *>(right)->getPointee(), true) == false)
            return false;

    return true;
}

bool operator==(AType const &left, AType const &right)
{
    if ((left.getTypeTag() == AType::T_ARRAY && right.getTypeTag() == AType::T_POINTER) || ((left.getTypeTag() == AType::T_POINTER && right.getTypeTag() == AType::T_ARRAY)))
        return true;

    if (left.getTypeTag() != right.getTypeTag())
        return false;

    if (left.getTypeTag() == AType::T_POINTER)
        if ((*(static_cast<APointer const &>(left).getPointee()) == *(static_cast<APointer const &>(right).getPointee())) == false)
            return false;

    return true;
}

bool operator>(AType const &left, AType const &right)
{
    if (left.getTypeTag() == AType::T_REAL && (right.getTypeTag() == AType::T_WORD || right.getTypeTag() == AType::T_BYTE))
        return true;

    if (left.getTypeTag() == AType::T_DWORD && (right.getTypeTag() == AType::T_REAL || right.getTypeTag() == AType::T_WORD || right.getTypeTag() == AType::T_BYTE))
        return true;

    if (left.getTypeTag() == AType::T_WORD && right.getTypeTag() == AType::T_BYTE)
        return true;

    if (left.getTypeTag() == AType::T_POINTER)
        if ((*(static_cast<APointer const &>(left).getPointee()) > *(static_cast<APointer const &>(left).getPointee())) == false)
            return false;

    return false;
}

bool operator<(AType const &left, AType const &right)
{
    return !((left == right) || (left > right));
}

std::ostream &operator<<(std::ostream &out, AType const &type)
{
    return out << type.printType();
}

//-----------------------------------------------------------------------------
// ABool
//-----------------------------------------------------------------------------
AType *ABool::clone() const
{
    return new ABool(*this);
}

AType::TypeTag ABool::getTypeTag() const
{
    return T_BOOL;
}

llvm::Type *ABool::getRawType(MainContext &mainContext) const
{
    return llvm::Type::getInt1Ty(mainContext.getLLVMContext());
}

std::string const ABool::printType() const
{
    return "bool";
}

AType::TypeClass ABool::getTypeClass() const
{
    return C_BOOL;
}

//-----------------------------------------------------------------------------
// AArray
//-----------------------------------------------------------------------------
AArray::AArray(const AType *pointerToType, unsigned long numberOfElements)
    : _pointee {pointerToType->clone()},
      _numberOfElements {numberOfElements}
{}

AArray::~AArray()
{
    delete _pointee;
}

AArray::AArray(AArray const &type)
    : _pointee {(type._pointee)->clone()},
      _numberOfElements {type._numberOfElements}
{}

AType *AArray::clone() const
{
    return new AArray(*this);
}

AType::TypeTag AArray::getTypeTag() const
{
    return T_ARRAY;
}

llvm::Type *AArray::getRawType(MainContext &mainContext) const
{
    return llvm::ArrayType::get(_pointee->getRawType(mainContext), _numberOfElements);
}

AType const *AArray::getPointee() const
{
    return _pointee;
}

std::string const AArray::printType() const
{
    return "*" + _pointee->printType();
}

AType::TypeClass AArray::getTypeClass() const
{
    return C_POINTER;
}

unsigned long AArray::getArraySize() const
{
    return _numberOfElements;
}

//-----------------------------------------------------------------------------
// AByte
//-----------------------------------------------------------------------------
AType *AByte::clone() const
{
    return new AByte(*this);
}

AType::TypeTag AByte::getTypeTag() const
{
    return T_BYTE;
}

llvm::Type *AByte::getRawType(MainContext &mainContext) const
{
    return llvm::Type::getInt8Ty(mainContext.getLLVMContext());
}

std::string const AByte::printType() const
{
    return "i8";
}

AType::TypeClass AByte::getTypeClass() const
{
    return C_NUMBER;
}

//-----------------------------------------------------------------------------
// AWord
//-----------------------------------------------------------------------------
AType *AWord::clone() const
{
    return new AWord(*this);
}

AType::TypeTag AWord::getTypeTag() const
{
    return T_WORD;
}

llvm::Type *AWord::getRawType(MainContext &mainContext) const
{
    return llvm::Type::getInt16Ty(mainContext.getLLVMContext());
}

std::string const AWord::printType() const
{
    return "i16";
}

AType::TypeClass AWord::getTypeClass() const
{
    return C_NUMBER;
}

//-----------------------------------------------------------------------------
// ADword
//-----------------------------------------------------------------------------
AType *ADword::clone() const
{
    return new ADword(*this);
}

AType::TypeTag ADword::getTypeTag() const
{
    return T_DWORD;
}

llvm::Type *ADword::getRawType(MainContext &mainContext) const
{
    return llvm::Type::getInt32Ty(mainContext.getLLVMContext());
}

std::string const ADword::printType() const
{
    return "i32";
}

AType::TypeClass ADword::getTypeClass() const
{
    return C_NUMBER;
}

//-----------------------------------------------------------------------------
// AReal
//-----------------------------------------------------------------------------
AType *AReal::clone() const
{
    return new AReal(*this);
}

AType::TypeTag AReal::getTypeTag() const
{
    return T_REAL;
}

llvm::Type *AReal::getRawType(MainContext &mainContext) const
{
    return llvm::Type::getFloatTy(mainContext.getLLVMContext());
}

std::string const AReal::printType() const
{
    return "real";
}

AType::TypeClass AReal::getTypeClass() const
{
    return C_NUMBER;
}

//-----------------------------------------------------------------------------
// APointer
//-----------------------------------------------------------------------------
APointer::APointer(AType const *pointerToType)
    : _pointee {pointerToType->clone()}
{}

APointer::~APointer()
{
    delete _pointee;
}

APointer::APointer(APointer const &type)
{
    _pointee = (type._pointee)->clone();
}

AType *APointer::clone() const
{
    return new APointer(*this);
}

AType::TypeTag APointer::getTypeTag() const
{
    return T_POINTER;
}

llvm::Type *APointer::getRawType(MainContext &mainContext) const
{
    return calculatePointerType(this, mainContext);
}

llvm::Type *APointer::calculatePointerType(AType const *pointerType, MainContext &mainContext) const
{
    if (pointerType->getTypeTag() != T_POINTER)
        return pointerType->getRawType(mainContext);

    return llvm::PointerType::get(calculatePointerType(static_cast<APointer const *>(pointerType)->getPointee(), mainContext), 0);
}

AType const *APointer::getPointee() const
{
    return _pointee;
}

std::string const APointer::printType() const
{
    std::string typeMark("*");
    AType const *pointee = _pointee;

    while (pointee->getTypeTag() == T_POINTER)
    {
        typeMark += "*";
        pointee = static_cast<APointer const *>(pointee)->getPointee();
    }

    return typeMark + pointee->printType();
}

AType::TypeClass APointer::getTypeClass() const
{
    return C_POINTER;
}

//-----------------------------------------------------------------------------
// AVoid
//-----------------------------------------------------------------------------
AType *AVoid::clone() const
{
    return new AVoid(*this);
}

AType::TypeTag AVoid::getTypeTag() const
{
    return T_VOID;
}

llvm::Type *AVoid::getRawType(MainContext &mainContext) const
{
    return llvm::Type::getVoidTy(mainContext.getLLVMContext());
}

std::string const AVoid::printType() const
{
    return "void";
}

AType::TypeClass AVoid::getTypeClass() const
{
    return C_VOID;
}

//-----------------------------------------------------------------------------
// AUnknown
//-----------------------------------------------------------------------------
AType *AUnknown::clone() const
{
    return new AUnknown(*this);
}

AType::TypeTag AUnknown::getTypeTag() const
{
    assert(false);
}

llvm::Type *AUnknown::getRawType(MainContext &mainContext) const
{
    assert(false);
}

const std::string AUnknown::printType() const
{
    assert(false);
}

AType::TypeClass AUnknown::getTypeClass() const
{
    assert(false);
}
