#include "../include/code_gen_visitor.hpp"
#include "../include/statement_ast.hpp"
#include "../include/expression_ast.hpp"
#include "../include/main_context.hpp"
#include "../include/debug.hpp"
#include "../include/atype.hpp"
#include "../include/pointer_content.hpp"

CodeGenVisitor::CodeGenVisitor()
	: _mainContext {MainContext::GetInstance()}
{}

CodeGenVisitor::~CodeGenVisitor()
{}

void CodeGenVisitor::visit(BoolAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating BoolAST" << std::endl;
#endif

	this->storeLLVMValue(llvm::ConstantInt::get(_mainContext.getLLVMContext(), llvm::APInt(1, node->getValue(), true)));
}

void CodeGenVisitor::visit(ByteAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating ByteAST" << std::endl;
#endif

	this->storeLLVMValue(llvm::ConstantInt::get(_mainContext.getLLVMContext(), llvm::APInt(8, node->getValue(), true)));
}

void CodeGenVisitor::visit(WordAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating WordAST" << std::endl;
#endif

	this->storeLLVMValue(llvm::ConstantInt::get(_mainContext.getLLVMContext(), llvm::APInt(16, node->getValue(), true)));
}

void CodeGenVisitor::visit(DWordAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating DWordAST" << std::endl;
#endif

	this->storeLLVMValue(llvm::ConstantInt::get(_mainContext.getLLVMContext(), llvm::APInt(32, node->getValue(), true)));
}

void CodeGenVisitor::visit(RealAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating RealAST" << std::endl;
#endif

	this->storeLLVMValue(llvm::ConstantFP::get(_mainContext.getLLVMContext(), llvm::APFloat(node->getValue())));
}

void CodeGenVisitor::visit(BinaryOperatorAST const *node __attribute__((unused)))
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating BinaryOperatorAST" << std::endl;
#endif
}

void CodeGenVisitor::visit(ArithmeticBinaryOperatorAST const *node __attribute__((unused)))
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating ArithmeticBinaryOperatorAST" << std::endl;
#endif
}

void CodeGenVisitor::visit( ArithmeticUnaryOperatorAST const *node __attribute__((unused)))
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating ArithmeticUnaryOperatorAST" << std::endl;
#endif
}

void CodeGenVisitor::visit(RelationBinaryOperatorAST const *node __attribute__((unused)))
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating RelationBinaryOperatorAST" << std::endl;
#endif
}

void CodeGenVisitor::visit(LogicBinaryOperatorAST const *node __attribute__((unused)))
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating LogicBinaryOperatorAST" << std::endl;
#endif
}

void CodeGenVisitor::visit(UnaryOperatorAST const *node __attribute__((unused)))
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating UnaryOperatorAST" << std::endl;
#endif
}

void CodeGenVisitor::visit(LogicUnaryOperatorAST const *node __attribute__((unused)))
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating LogicUnaryOperatorAST" << std::endl;
#endif
}

void CodeGenVisitor::visit(AdditionAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating AdditionAST" << std::endl;
#endif

	BaseExpressionAST const *leftOperand = node->getLeftOperand();
	BaseExpressionAST const *rightOperand = node->getRightOperand();

	llvm::Value *leftValue = this->getLLVMValue(leftOperand);
	llvm::Value *rightValue = this->getLLVMValue(rightOperand);

	AType const *leftType = leftOperand->getExpressionType();
	AType const *rightType = rightOperand->getExpressionType();
	AType const *largerType = (*leftType > *rightType) ? leftType : rightType;

	if (largerType->getTypeTag() != AType::T_REAL)
	{
		this->storeLLVMValue(_mainContext.getIRBuilder().CreateAdd(_mainContext.doPromotion(leftValue, leftType, largerType),
						 _mainContext.doPromotion(rightValue, rightType, largerType)));
	}
	else
	{
		this->storeLLVMValue(_mainContext.getIRBuilder().CreateFAdd(_mainContext.doPromotion(leftValue, leftType, largerType),
						 _mainContext.doPromotion(rightValue, rightType, largerType)));
	}
}

void CodeGenVisitor::visit(SubtractionAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating SubtractionAST" << std::endl;
#endif

	BaseExpressionAST const *leftOperand = node->getLeftOperand();
	BaseExpressionAST const *rightOperand = node->getRightOperand();

	llvm::Value *leftValue = this->getLLVMValue(leftOperand);
	llvm::Value *rightValue = this->getLLVMValue(rightOperand);

	AType const *leftType = leftOperand->getExpressionType();
	AType const *rightType = rightOperand->getExpressionType();
	AType const *largerType = (*leftType > *rightType) ? leftType : rightType;

	if (largerType->getTypeTag() != AType::T_REAL)
	{
		this->storeLLVMValue(_mainContext.getIRBuilder().CreateSub(_mainContext.doPromotion(leftValue, leftType, largerType),
						 _mainContext.doPromotion(rightValue, rightType, largerType)));
	}
	else
	{
		this->storeLLVMValue(_mainContext.getIRBuilder().CreateFSub(_mainContext.doPromotion(leftValue, leftType, largerType),
						 _mainContext.doPromotion(rightValue, rightType, largerType)));
	}
}

void CodeGenVisitor::visit(MultiplicationAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating MultiplicationAST" << std::endl;
#endif

	BaseExpressionAST const *leftOperand = node->getLeftOperand();
	BaseExpressionAST const *rightOperand = node->getRightOperand();

	llvm::Value *leftValue = this->getLLVMValue(leftOperand);
	llvm::Value *rightValue = this->getLLVMValue(rightOperand);

	AType const *leftType = leftOperand->getExpressionType();
	AType const *rightType = rightOperand->getExpressionType();
	AType const *largerType = (*leftType > *rightType) ? leftType : rightType;

	if (largerType->getTypeTag() != AType::T_REAL)
	{
		this->storeLLVMValue(_mainContext.getIRBuilder().CreateMul(_mainContext.doPromotion(leftValue, leftType, largerType),
						 _mainContext.doPromotion(rightValue, rightType, largerType)));
	}
	else
	{
		this->storeLLVMValue(_mainContext.getIRBuilder().CreateFMul(_mainContext.doPromotion(leftValue, leftType, largerType),
						 _mainContext.doPromotion(rightValue, rightType, largerType)));
	}
}

void CodeGenVisitor::visit(DivisionAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating DivisionAST" << std::endl;
#endif

	BaseExpressionAST const *leftOperand = node->getLeftOperand();
	BaseExpressionAST const *rightOperand = node->getRightOperand();

	llvm::Value *leftValue = this->getLLVMValue(leftOperand);
	llvm::Value *rightValue = this->getLLVMValue(rightOperand);

	AType const *leftType = leftOperand->getExpressionType();
	AType const *rightType = rightOperand->getExpressionType();
	AType const *largerType = (*leftType > *rightType) ? leftType : rightType;

	if (largerType->getTypeTag() != AType::T_REAL)
	{
		this->storeLLVMValue(_mainContext.getIRBuilder().CreateSDiv(_mainContext.doPromotion(leftValue, leftType, largerType),
						 _mainContext.doPromotion(rightValue, rightType, largerType)));
	}
	else
	{
		this->storeLLVMValue(_mainContext.getIRBuilder().CreateFDiv(_mainContext.doPromotion(leftValue, leftType, largerType),
						 _mainContext.doPromotion(rightValue, rightType, largerType)));
	}
}

void CodeGenVisitor::visit(ModAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating ModAST" << std::endl;
#endif

	BaseExpressionAST const *leftOperand = node->getLeftOperand();
	BaseExpressionAST const *rightOperand = node->getRightOperand();

	llvm::Value *leftValue = this->getLLVMValue(leftOperand);
	llvm::Value *rightValue = this->getLLVMValue(rightOperand);

	AType const *leftType = leftOperand->getExpressionType();
	AType const *rightType = rightOperand->getExpressionType();
	AType const *largerType = (*leftType > *rightType) ? leftType : rightType;

	if (largerType->getTypeTag() != AType::T_REAL)
	{
		this->storeLLVMValue(_mainContext.getIRBuilder().CreateSRem(_mainContext.doPromotion(leftValue, leftType, largerType),
						 _mainContext.doPromotion(rightValue, rightType, largerType)));
	}
	else
	{
		this->storeLLVMValue(_mainContext.getIRBuilder().CreateFRem(_mainContext.doPromotion(leftValue, leftType, largerType),
						 _mainContext.doPromotion(rightValue, rightType, largerType)));
	}
}

void CodeGenVisitor::visit(LessThanAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating LessThanAST" << std::endl;
#endif

	BaseExpressionAST const *leftOperand = node->getLeftOperand();
	BaseExpressionAST const *rightOperand = node->getRightOperand();

	llvm::Value *leftValue = this->getLLVMValue(leftOperand);
	llvm::Value *rightValue = this->getLLVMValue(rightOperand);

	AType const *leftType = leftOperand->getExpressionType();
	AType const *rightType = rightOperand->getExpressionType();
	AType const *largerType = (*leftType > *rightType) ? leftType : rightType;

	if (largerType->getTypeTag() != AType::T_REAL)
	{
		this->storeLLVMValue(_mainContext.getIRBuilder().CreateICmpSLT(_mainContext.doPromotion(leftValue, leftType, largerType),
						 _mainContext.doPromotion(rightValue, rightType, largerType)));
	}
	else
	{
		this->storeLLVMValue(_mainContext.getIRBuilder().CreateFCmpULT(_mainContext.doPromotion(leftValue, leftType, largerType),
						 _mainContext.doPromotion(rightValue, rightType, largerType)));
	}
}

void CodeGenVisitor::visit(GreaterThanAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating GreaterThanAST" << std::endl;
#endif

	BaseExpressionAST const *leftOperand = node->getLeftOperand();
	BaseExpressionAST const *rightOperand = node->getRightOperand();

	llvm::Value *leftValue = this->getLLVMValue(leftOperand);
	llvm::Value *rightValue = this->getLLVMValue(rightOperand);

	AType const *leftType = leftOperand->getExpressionType();
	AType const *rightType = rightOperand->getExpressionType();
	AType const *largerType = (*leftType > *rightType) ? leftType : rightType;

	if (largerType->getTypeTag() != AType::T_REAL)
	{
		this->storeLLVMValue(_mainContext.getIRBuilder().CreateICmpSGT(_mainContext.doPromotion(leftValue, leftType, largerType),
						 _mainContext.doPromotion(rightValue, rightType, largerType)));
	}
	else
	{
		this->storeLLVMValue(_mainContext.getIRBuilder().CreateFCmpUGT(_mainContext.doPromotion(leftValue, leftType, largerType),
						 _mainContext.doPromotion(rightValue, rightType, largerType)));
	}
}

void CodeGenVisitor::visit(EqualToAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating EqualToAST" << std::endl;
#endif

	BaseExpressionAST const *leftOperand = node->getLeftOperand();
	BaseExpressionAST const *rightOperand = node->getRightOperand();

	llvm::Value *leftValue = this->getLLVMValue(leftOperand);
	llvm::Value *rightValue = this->getLLVMValue(rightOperand);

	AType const *leftType = leftOperand->getExpressionType();
	AType const *rightType = rightOperand->getExpressionType();
	AType const *largerType = (*leftType > *rightType) ? leftType : rightType;

	if (largerType->getTypeTag() != AType::T_REAL)
	{
		this->storeLLVMValue(_mainContext.getIRBuilder().CreateICmpEQ(_mainContext.doPromotion(leftValue, leftType, largerType),
						 _mainContext.doPromotion(rightValue, rightType, largerType)));
	}
	else
	{
		this->storeLLVMValue(_mainContext.getIRBuilder().CreateFCmpUEQ(_mainContext.doPromotion(leftValue, leftType, largerType),
						 _mainContext.doPromotion(rightValue, rightType, largerType)));
	}
}

void CodeGenVisitor::visit(NotEqualToAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating NotEqualToAST" << std::endl;
#endif

	BaseExpressionAST const *leftOperand = node->getLeftOperand();
	BaseExpressionAST const *rightOperand = node->getRightOperand();

	llvm::Value *leftValue = this->getLLVMValue(leftOperand);
	llvm::Value *rightValue = this->getLLVMValue(rightOperand);

	AType const *leftType = leftOperand->getExpressionType();
	AType const *rightType = rightOperand->getExpressionType();
	AType const *largerType = (*leftType > *rightType) ? leftType : rightType;

	if (largerType->getTypeTag() != AType::T_REAL)
	{
		this->storeLLVMValue(_mainContext.getIRBuilder().CreateICmpNE(_mainContext.doPromotion(leftValue, leftType, largerType),
						 _mainContext.doPromotion(rightValue, rightType, largerType)));
	}
	else
	{
		this->storeLLVMValue(_mainContext.getIRBuilder().CreateFCmpUNE(_mainContext.doPromotion(leftValue, leftType, largerType),
						 _mainContext.doPromotion(rightValue, rightType, largerType)));
	}
}

void CodeGenVisitor::visit(AndAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating AndAST" << std::endl;
#endif

	llvm::Value *leftValue = this->getLLVMValue(node->getLeftOperand());
	llvm::Value *rightValue = this->getLLVMValue(node->getRightOperand());

	this->storeLLVMValue(_mainContext.getIRBuilder().CreateAnd(leftValue, rightValue));
}

void CodeGenVisitor::visit(OrAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating OrAST" << std::endl;
#endif

	llvm::Value *leftValue = this->getLLVMValue(node->getLeftOperand());
	llvm::Value *rightValue = this->getLLVMValue(node->getRightOperand());

	this->storeLLVMValue(_mainContext.getIRBuilder().CreateOr(leftValue, rightValue));
}

void CodeGenVisitor::visit(NotAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating NotAST" << std::endl;
#endif

	llvm::Value *operand = this->getLLVMValue(node->getOperand());

	this->storeLLVMValue(_mainContext.getIRBuilder().CreateNot(operand));
}

void CodeGenVisitor::visit(NegAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating NegAST" << std::endl;
#endif

	llvm::Value *operand = this->getLLVMValue(node->getOperand());

	this->storeLLVMValue(_mainContext.getIRBuilder().CreateNeg(operand));
}

void CodeGenVisitor::visit(EquivalentToAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating EquivalentToAST" << std::endl;
#endif

	llvm::Value *leftValue = this->getLLVMValue(node->getLeftOperand());
	llvm::Value *rightValue = this->getLLVMValue(node->getRightOperand());

	this->storeLLVMValue(_mainContext.getIRBuilder().CreateICmpEQ(leftValue, rightValue));
}

void CodeGenVisitor::visit(FunctionCallExpressionAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating FunctionCallExpressionAST" << std::endl;
#endif

	llvm::Function *function = _mainContext.getLLVMModule()->getFunction(node->getFunctionName());
	if (function == nullptr)
	{
		assert(false);
	}

	std::vector<llvm::Value *> argumentValues;
	unsigned char argumentIndex = 0;
	for (BaseExpressionAST const *argument : node->getArguments())
	{
		AType const *argumentType = argument->getExpressionType();
		AType const *parameterType = ((node->getPrototype()->getParameters())[argumentIndex])->getType();

		if (*argumentType < *parameterType)
		{
			argumentValues.push_back(_mainContext.doPromotion(this->getLLVMValue(argument), argumentType, parameterType));
		}
		else if (*argumentType > *parameterType)
		{
			argumentValues.push_back(_mainContext.doDemotion(this->getLLVMValue(argument), argumentType, parameterType));
		}
		else
		{
			argumentValues.push_back(this->getLLVMValue(argument));
		}

		++argumentIndex;
	}

	if (node->getPrototype()->getReturnType()->getTypeTag() != AType::T_VOID)
	{
		this->storeLLVMValue(_mainContext.getIRBuilder().CreateCall(function, argumentValues, "function_call_expression"));
	}
	else
	{
		this->storeLLVMValue(_mainContext.getIRBuilder().CreateCall(function, argumentValues));
	}
}

void CodeGenVisitor::visit(LocalVariableAccessAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating LocalVariableAccessAST" << std::endl;
#endif

	if (node->getLocalVariableDeclaration()->getType()->getTypeTag() != AType::T_ARRAY)
	{
		this->storeLLVMValue(_mainContext.getIRBuilder().CreateLoad(node->getLocalVariableDeclaration()->getAlloca()));
	}
	else // functionCall(array);
	{
		std::vector<llvm::Value *> indices {2};
		indices[0] = llvm::ConstantInt::get(_mainContext.getLLVMContext(), llvm::APInt(32, 0, true));
		indices[1] = llvm::ConstantInt::get(_mainContext.getLLVMContext(), llvm::APInt(32, 0, true));

		this->storeLLVMValue(_mainContext.getIRBuilder().CreateGEP(node->getLocalVariableDeclaration()->getType()->getRawType(_mainContext),
						 node->getLocalVariableDeclaration()->getAlloca(),
						 indices));
	}
}

void CodeGenVisitor::visit(LocalPointerAccessAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating LocalPointerAccessAST" << std::endl;
#endif

	if (node->getLocalVariableDeclaration()->getType()->getTypeTag() == AType::T_ARRAY)
	{
		std::vector<llvm::Value *> indices {2};
		indices[0] = llvm::ConstantInt::get(_mainContext.getLLVMContext(), llvm::APInt(32, 0, true));
		indices[1] = this->getLLVMValue(node->getPointerContent()->getIndex());

		llvm::Value *loadedValue = _mainContext.getIRBuilder().CreateGEP(node->getLocalVariableDeclaration()->getType()->getRawType(_mainContext),
								   node->getLocalVariableDeclaration()->getAlloca(),
								   indices);

		this->storeLLVMValue(_mainContext.getIRBuilder().CreateLoad(loadedValue));
	}
	else if (node->getLocalVariableDeclaration()->getType()->getTypeTag() == AType::T_POINTER)
	{
		llvm::Value *loadedValue = node->getLocalVariableDeclaration()->getAlloca();
		for (unsigned int i = 0; i < node->getPointerContent()->getReferenceNumber(); ++i)
			loadedValue = _mainContext.getIRBuilder().CreateLoad(loadedValue);

		loadedValue = _mainContext.getIRBuilder().CreateGEP(loadedValue, this->getLLVMValue(node->getPointerContent()->getIndex()));

		this->storeLLVMValue(_mainContext.getIRBuilder().CreateLoad(loadedValue));
	}
}

void CodeGenVisitor::visit(LocalVariableAddressAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating LocalVariableAddressAST" << std::endl;
#endif

	if (node->getLocalVariableDeclaration()->getType()->getTypeTag() == AType::T_ARRAY)
	{
		std::vector<llvm::Value *> indices {2};
		indices[0] = llvm::ConstantInt::get(_mainContext.getLLVMContext(), llvm::APInt(32, 0, true));
		indices[1] = this->getLLVMValue(node->getIndex());

		this->storeLLVMValue(_mainContext.getIRBuilder().CreateGEP(node->getLocalVariableDeclaration()->getType()->getRawType(_mainContext),
						 node->getLocalVariableDeclaration()->getAlloca(),
						 indices));
	}
	else if (node->getLocalVariableDeclaration()->getType()->getTypeTag() == AType::T_POINTER)
	{
		if (node->getIndex() == nullptr)
		{
			this->storeLLVMValue(node->getLocalVariableDeclaration()->getAlloca());
		}
		else
		{
			std::vector<llvm::Value *> indices {2};
			indices[0] = llvm::ConstantInt::get(_mainContext.getLLVMContext(), llvm::APInt(32, 0, true));
			indices[1] = this->getLLVMValue(node->getIndex());

			this->storeLLVMValue(_mainContext.getIRBuilder().CreateGEP(node->getLocalVariableDeclaration()->getType()->getRawType(_mainContext),
							 node->getLocalVariableDeclaration()->getAlloca(),
							 indices));
		}
	}
	else
	{
		this->storeLLVMValue(node->getLocalVariableDeclaration()->getAlloca());
	}
}

void CodeGenVisitor::visit(ExpressionCommandAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating ExpressionCommandAST " << node->getCommandName() << std::endl;
#endif

	std::string const &commandName = node->getCommandName();
	llvm::Function *function = _mainContext.getLLVMModule()->getFunction(commandName);

	if (commandName == "time" || commandName == "utime")
	{
		llvm::FunctionType *returnType = llvm::FunctionType::get(llvm::Type::getInt32Ty(_mainContext.getLLVMContext()), false);
		if (function == nullptr)
			function = llvm::Function::Create(returnType, llvm::Function::ExternalLinkage, commandName, _mainContext.getLLVMModule());

		this->storeLLVMValue(_mainContext.getIRBuilder().CreateCall(function));
	}
	else if (commandName == "receive")
	{
		llvm::FunctionType *returnType = llvm::FunctionType::get(llvm::Type::getInt8Ty(_mainContext.getLLVMContext()), false);
		if (function == nullptr)
			function = llvm::Function::Create(returnType, llvm::Function::ExternalLinkage, commandName, _mainContext.getLLVMModule());

		this->storeLLVMValue(_mainContext.getIRBuilder().CreateCall(function));
	}
	else if (commandName == "read")
	{
		std::vector<llvm::Type *> parameters { llvm::Type::getInt8Ty(_mainContext.getLLVMContext()) };
		llvm::FunctionType *returnType = llvm::FunctionType::get(llvm::Type::getInt1Ty(_mainContext.getLLVMContext()), parameters, false);
		if (function == nullptr)
			function = llvm::Function::Create(returnType, llvm::Function::ExternalLinkage, commandName, _mainContext.getLLVMModule());

		std::vector<llvm::Value *> argumentValues;
		AType const *argumentType = node->getPinNumber()->getExpressionType();
		AType const *parameterType = new AByte();

		if (*argumentType < *parameterType)
		{
			argumentValues.push_back(_mainContext.doPromotion(this->getLLVMValue(node->getPinNumber()), argumentType, parameterType));
		}
		else if (*argumentType > *parameterType)
		{
			argumentValues.push_back(_mainContext.doDemotion(this->getLLVMValue(node->getPinNumber()), argumentType, parameterType));
		}
		else
		{
			argumentValues.push_back(this->getLLVMValue(node->getPinNumber()));
		}

		delete parameterType;

		this->storeLLVMValue(_mainContext.getIRBuilder().CreateCall(function, argumentValues));
	}
	else if (commandName == "adc")
	{
		std::vector<llvm::Type *> parameters { llvm::Type::getInt8Ty(_mainContext.getLLVMContext()) };
		llvm::FunctionType *returnType = llvm::FunctionType::get(llvm::Type::getInt16Ty(_mainContext.getLLVMContext()), parameters, false);
		if (function == nullptr)
			function = llvm::Function::Create(returnType, llvm::Function::ExternalLinkage, commandName, _mainContext.getLLVMModule());

		std::vector<llvm::Value *> argumentValues;
		AType const *argumentType = node->getPinNumber()->getExpressionType();
		AType const *parameterType = new AByte();

		if (*argumentType < *parameterType)
		{
			argumentValues.push_back(_mainContext.doPromotion(this->getLLVMValue(node->getPinNumber()), argumentType, parameterType));
		}
		else if (*argumentType > *parameterType)
		{
			argumentValues.push_back(_mainContext.doDemotion(this->getLLVMValue(node->getPinNumber()), argumentType, parameterType));
		}
		else
		{
			argumentValues.push_back(this->getLLVMValue(node->getPinNumber()));
		}

		delete parameterType;

		this->storeLLVMValue(_mainContext.getIRBuilder().CreateCall(function, argumentValues));
	}
}

void CodeGenVisitor::visit(BlockAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating BlockAST" << std::endl;
#endif

	for (BaseStatementAST const *statementPtr : node->getStatements())
	{
		this->getLLVMValue(statementPtr);
	}
}

void CodeGenVisitor::visit(PrototypeAST const *node)
{
	this->storeLLVMValue(_mainContext.getLLVMModule()->getFunction(node->getFunctionName()));
}

void CodeGenVisitor::visit(const FunctionDefinitionAST *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating FunctionDefinitionAST" << std::endl;
#endif

	llvm::Function *function = static_cast<llvm::Function *>(this->getLLVMValue(node->getPrototype()));

	unsigned char parameterIndex = 0;
	for (auto &parameter : function->args())
		parameter.setName(node->getPrototype()->getParameters()[parameterIndex++]->getName() + "_PARAM_");

	llvm::BasicBlock *basicBlock = llvm::BasicBlock::Create(_mainContext.getLLVMContext(), "function_block", function);
	_mainContext.getIRBuilder().SetInsertPoint(basicBlock);

	parameterIndex = 0;
	for (auto &argument : function->args())
		_mainContext.getIRBuilder().CreateStore(&argument, this->getLLVMValue(node->getPrototype()->getParameters()[parameterIndex++]));

	// ------------------------ init() - init timers -------------------------
	if (node->getPrototype()->getFunctionName() == "main")
	{
		llvm::FunctionType *returnType = llvm::FunctionType::get(llvm::Type::getVoidTy(_mainContext.getLLVMContext()), false);
		llvm::Function::Create(returnType, llvm::Function::ExternalLinkage, "init", _mainContext.getLLVMModule());
		_mainContext.getIRBuilder().CreateCall(_mainContext.getLLVMModule()->getFunction("init"));
	}
	// ------------------------------------------------------------------------------------

	this->getLLVMValue(node->getBody());

	if (_mainContext.getIRBuilder().GetInsertBlock()->getTerminator() == nullptr)
	{
		AType::TypeTag typeTag = node->getPrototype()->getReturnType()->getTypeTag();

		if (typeTag == AType::T_VOID)
		{
			_mainContext.getIRBuilder().CreateRetVoid();
		}
		else
		{
			std::cerr << FRED("Caution: possibly no 'ret' statement in non-void function: ")
					  << node->getPrototype()->getFunctionName() << "(...)"
					  << std::endl;

			_mainContext.getIRBuilder().CreateRet(llvm::Constant::getNullValue(node->getPrototype()->getReturnType()->getRawType(_mainContext)));
		}
	}
}

void CodeGenVisitor::visit(ReturnStatementAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating ReturnStatementAST" << std::endl;
#endif

	AType const *expressionType = node->getReturnExpression()->getExpressionType();

	if (*(node->getScopeReturnType()) > *expressionType)
	{
		_mainContext.getIRBuilder().CreateRet(_mainContext.doPromotion(this->getLLVMValue(node->getReturnExpression()), expressionType, node->getScopeReturnType()));
	}
	else if (*(node->getScopeReturnType()) < *expressionType)
	{
		_mainContext.getIRBuilder().CreateRet(_mainContext.doDemotion(this->getLLVMValue(node->getReturnExpression()), expressionType, node->getScopeReturnType()));
	}
	else
	{
		_mainContext.getIRBuilder().CreateRet(this->getLLVMValue(node->getReturnExpression()));
	}
}

void CodeGenVisitor::visit(WhileAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating WhileAST" << std::endl;
#endif

	llvm::Function *function = _mainContext.getIRBuilder().GetInsertBlock()->getParent();
	llvm::BasicBlock *whileConditionBB = llvm::BasicBlock::Create(_mainContext.getLLVMContext(), "while_condition");
	llvm::BasicBlock *whileLoopBB = llvm::BasicBlock::Create(_mainContext.getLLVMContext(), "while_loop");
	llvm::BasicBlock *whileContinueBB = llvm::BasicBlock::Create(_mainContext.getLLVMContext(), "while_continue");

	_mainContext.getIRBuilder().CreateBr(whileConditionBB);
	function->getBasicBlockList().push_back(whileConditionBB);
	_mainContext.getIRBuilder().SetInsertPoint(whileConditionBB);

	llvm::Value *conditionValue = this->getLLVMValue(node->getCondition());

	_mainContext.getIRBuilder().CreateCondBr(conditionValue, whileLoopBB, whileContinueBB);

	function->getBasicBlockList().push_back(whileLoopBB);
	_mainContext.getIRBuilder().SetInsertPoint(whileLoopBB);

	this->getLLVMValue(node->getBlock());

	_mainContext.getIRBuilder().CreateBr(whileConditionBB);

	function->getBasicBlockList().push_back(whileContinueBB);
	_mainContext.getIRBuilder().SetInsertPoint(whileContinueBB);
}

void CodeGenVisitor::visit(ForAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating ForAST" << std::endl;
#endif

	llvm::Function *function = _mainContext.getIRBuilder().GetInsertBlock()->getParent();
	llvm::BasicBlock *forConditionBB = llvm::BasicBlock::Create(_mainContext.getLLVMContext(), "for_condition");
	llvm::BasicBlock *forLoopBB = llvm::BasicBlock::Create(_mainContext.getLLVMContext(), "for_loop");
	llvm::BasicBlock *forContinueBB = llvm::BasicBlock::Create(_mainContext.getLLVMContext(), "for_continue");

	node->getCounter()->replaceType(node->getCounterType());
	this->getLLVMValue(node->getCounter());

	_mainContext.getIRBuilder().CreateBr(forConditionBB);
	function->getBasicBlockList().push_back(forConditionBB);
	_mainContext.getIRBuilder().SetInsertPoint(forConditionBB);

	llvm::Value *counterVariableLoad = _mainContext.getIRBuilder().CreateLoad(node->getCounter()->getAlloca());
	llvm::Value *endValue = _mainContext.doPromotion(this->getLLVMValue(node->getStopValue()), node->getStopValue()->getExpressionType(), node->getCounter()->getType());
	llvm::Value *lessThan = _mainContext.getIRBuilder().CreateICmpSLT(counterVariableLoad, endValue);

	_mainContext.getIRBuilder().CreateCondBr(lessThan, forLoopBB, forContinueBB);

	function->getBasicBlockList().push_back(forLoopBB);
	_mainContext.getIRBuilder().SetInsertPoint(forLoopBB);

	this->getLLVMValue(node->getBlock());

	AType *oneValueType = new AByte();
	llvm::Value *oneValue = _mainContext.doPromotion(llvm::ConstantInt::get(_mainContext.getLLVMContext(), llvm::APInt(8, 1, true)), oneValueType, node->getCounterType());
	delete oneValueType;

	llvm::Value *counterVariableLoad2 = _mainContext.getIRBuilder().CreateLoad(node->getCounter()->getAlloca());
	llvm::Value *counterVariableIncrement = _mainContext.getIRBuilder().CreateAdd(counterVariableLoad2, oneValue);
	_mainContext.getIRBuilder().CreateStore(counterVariableIncrement, node->getCounter()->getAlloca());

	_mainContext.getIRBuilder().CreateBr(forConditionBB);

	function->getBasicBlockList().push_back(forContinueBB);
	_mainContext.getIRBuilder().SetInsertPoint(forContinueBB);
}

void CodeGenVisitor::visit(IfThenElseAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating IfThenElseAST" << std::endl;
#endif

	llvm::Function *function = _mainContext.getIRBuilder().GetInsertBlock()->getParent();
	llvm::BasicBlock *ifConditionBB = llvm::BasicBlock::Create(_mainContext.getLLVMContext(), "if_condition");
	llvm::BasicBlock *thenBB = llvm::BasicBlock::Create(_mainContext.getLLVMContext(), "then_block");
	llvm::BasicBlock *elseBB = nullptr;
	if (node->getElseBlock() != nullptr)
		elseBB = llvm::BasicBlock::Create(_mainContext.getLLVMContext(), "else_block");
	llvm::BasicBlock *ifContinueBB = llvm::BasicBlock::Create(_mainContext.getLLVMContext(), "if_continue");

	_mainContext.getIRBuilder().CreateBr(ifConditionBB);
	function->getBasicBlockList().push_back(ifConditionBB);
	_mainContext.getIRBuilder().SetInsertPoint(ifConditionBB);

	llvm::Value *conditionValue = this->getLLVMValue(node->getCondition());
	if (node->getElseBlock() != nullptr)
		_mainContext.getIRBuilder().CreateCondBr(conditionValue, thenBB, elseBB);
	else
		_mainContext.getIRBuilder().CreateCondBr(conditionValue, thenBB, ifContinueBB);

	function->getBasicBlockList().push_back(thenBB);
	_mainContext.getIRBuilder().SetInsertPoint(thenBB);
	this->getLLVMValue(node->getThenBlock());

	if (_mainContext.getIRBuilder().GetInsertBlock()->getTerminator() == nullptr)
		_mainContext.getIRBuilder().CreateBr(ifContinueBB);

	if (node->getElseBlock() != nullptr)
	{
		function->getBasicBlockList().push_back(elseBB);
		_mainContext.getIRBuilder().SetInsertPoint(elseBB);
		this->getLLVMValue(node->getElseBlock());

		if (_mainContext.getIRBuilder().GetInsertBlock()->getTerminator() == nullptr)
			_mainContext.getIRBuilder().CreateBr(ifContinueBB);
	}

	function->getBasicBlockList().push_back(ifContinueBB);
	_mainContext.getIRBuilder().SetInsertPoint(ifContinueBB);
}

void CodeGenVisitor::visit(IOCommandAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating IOCommandAST " << node->getCommandName() << std::endl;
#endif

	std::string const &commandName = node->getCommandName();
	llvm::Function *function = _mainContext.getLLVMModule()->getFunction(commandName);

	if (commandName == "setInternalAREF" || commandName == "setExternalAREF")
	{
		llvm::FunctionType *returnType = llvm::FunctionType::get(llvm::Type::getVoidTy(_mainContext.getLLVMContext()), false);
		if (function == nullptr)
			function = llvm::Function::Create(returnType, llvm::Function::ExternalLinkage, commandName, _mainContext.getLLVMModule());

		_mainContext.getIRBuilder().CreateCall(function);
	}
	else if (commandName == "pwm")
	{
		std::vector<llvm::Type *> parameters { llvm::Type::getInt8Ty(_mainContext.getLLVMContext()), llvm::Type::getInt16Ty(_mainContext.getLLVMContext()) };
		llvm::FunctionType *returnType = llvm::FunctionType::get(llvm::Type::getVoidTy(_mainContext.getLLVMContext()), parameters, false);
		if (function == nullptr)
			function = llvm::Function::Create(returnType, llvm::Function::ExternalLinkage, commandName, _mainContext.getLLVMModule());

		std::vector<llvm::Value *> argumentValues;
		for (unsigned char argumentIndex = 0; argumentIndex < 2; ++argumentIndex)
		{
			AType const *argumentType = (node->getPinList()[argumentIndex])->getExpressionType();
			AType const *parameterType;
			if (argumentIndex == 0)
				parameterType = new AByte();
			else
				parameterType = new AWord();

			if (*argumentType < *parameterType)
				argumentValues.push_back(_mainContext.doPromotion(this->getLLVMValue(node->getPinList()[argumentIndex]), argumentType, parameterType));
			else if (*argumentType > *parameterType)
				argumentValues.push_back(_mainContext.doDemotion(this->getLLVMValue(node->getPinList()[argumentIndex]), argumentType, parameterType));
			else
				argumentValues.push_back(this->getLLVMValue(node->getPinList()[argumentIndex]));

			delete parameterType;
		}

		_mainContext.getIRBuilder().CreateCall(function, argumentValues);
	}
	else if (commandName == "wait" || commandName == "begin")
	{
		std::vector<llvm::Type *> parameters { llvm::Type::getInt32Ty(_mainContext.getLLVMContext()) };
		llvm::FunctionType *returnType = llvm::FunctionType::get(llvm::Type::getVoidTy(_mainContext.getLLVMContext()), parameters, false);
		if (function == nullptr)
			function = llvm::Function::Create(returnType, llvm::Function::ExternalLinkage, commandName, _mainContext.getLLVMModule());

		std::vector<llvm::Value *> argumentValues;
		AType const *argumentType = node->getPinExpression()->getExpressionType();
		AType const *parameterType = new ADword();

		if (*argumentType < *parameterType)
			argumentValues.push_back(_mainContext.doPromotion(this->getLLVMValue(node->getPinExpression()), argumentType, parameterType));
		else if (*argumentType > *parameterType)
			argumentValues.push_back(_mainContext.doDemotion(this->getLLVMValue(node->getPinExpression()), argumentType, parameterType));
		else
			argumentValues.push_back(this->getLLVMValue(node->getPinExpression()));

		delete parameterType;

		_mainContext.getIRBuilder().CreateCall(function, argumentValues);
	}
	else if (commandName == "init_pwm")
	{
		std::vector<llvm::Type *> parameters { llvm::Type::getInt8Ty(_mainContext.getLLVMContext()) };
		llvm::FunctionType *returnType = llvm::FunctionType::get(llvm::Type::getVoidTy(_mainContext.getLLVMContext()), parameters, false);
		if (function == nullptr)
			function = llvm::Function::Create(returnType, llvm::Function::ExternalLinkage, commandName, _mainContext.getLLVMModule());

		std::vector<llvm::Value *> argumentValues;
		AType const *argumentType = node->getPinExpression()->getExpressionType();
		AType const *parameterType = new AByte();

		if (*argumentType < *parameterType)
			argumentValues.push_back(_mainContext.doPromotion(this->getLLVMValue(node->getPinExpression()), argumentType, parameterType));
		else if (*argumentType > *parameterType)
			argumentValues.push_back(_mainContext.doDemotion(this->getLLVMValue(node->getPinExpression()), argumentType, parameterType));
		else
			argumentValues.push_back(this->getLLVMValue(node->getPinExpression()));

		delete parameterType;

		_mainContext.getIRBuilder().CreateCall(function, argumentValues);
	}
	else
	{
		std::vector<llvm::Type *> parameters { llvm::Type::getInt8Ty(_mainContext.getLLVMContext()) };
		llvm::FunctionType *returnType = llvm::FunctionType::get(llvm::Type::getVoidTy(_mainContext.getLLVMContext()), parameters, false);
		if (function == nullptr)
			function = llvm::Function::Create(returnType, llvm::Function::ExternalLinkage, commandName, _mainContext.getLLVMModule());

		for (unsigned int argumentIndex = 0; argumentIndex < node->getPinList().size(); ++argumentIndex)
		{
			std::vector<llvm::Value *> argumentValues;
			AType const *argumentType = (node->getPinList()[argumentIndex])->getExpressionType();
			AType const *parameterType = new AByte();

			if (*argumentType < *parameterType)
				argumentValues.push_back(_mainContext.doPromotion(this->getLLVMValue(node->getPinList()[argumentIndex]), argumentType, parameterType));
			else if (*argumentType > *parameterType)
				argumentValues.push_back(_mainContext.doDemotion(this->getLLVMValue(node->getPinList()[argumentIndex]), argumentType, parameterType));
			else
				argumentValues.push_back(this->getLLVMValue(node->getPinList()[argumentIndex]));

			delete parameterType;

			_mainContext.getIRBuilder().CreateCall(function, argumentValues);
		}
	}
}

void CodeGenVisitor::visit(FunctionCallStatementAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating FunctionCallStatementAST" << std::endl;
#endif

	llvm::Function *function = _mainContext.getLLVMModule()->getFunction(node->getFunctionName());
	if (function == nullptr)
		function = static_cast<llvm::Function *>(this->getLLVMValue(node->getPrototype()));

	unsigned char argumentIndex = 0;
	std::vector<llvm::Value *> argumentValues;
	for (auto argument : node->getArguments())
	{
		AType const *argumentType = argument->getExpressionType();
		AType const *parameterType = ((node->getPrototype()->getParameters())[argumentIndex])->getType();

		if (*argumentType < *parameterType)
		{
			argumentValues.push_back(_mainContext.doPromotion(this->getLLVMValue(argument), argumentType, parameterType));
		}
		else if (*argumentType > *parameterType)
		{
			argumentValues.push_back(_mainContext.doDemotion(this->getLLVMValue(argument), argumentType, parameterType));
		}
		else
		{
			argumentValues.push_back(this->getLLVMValue(argument));
		}

		++argumentIndex;
	}

	if (node->getPrototype()->getReturnType()->getTypeTag() != AType::T_VOID)
		_mainContext.getIRBuilder().CreateCall(function, argumentValues, "function_call_statement");
	else
		_mainContext.getIRBuilder().CreateCall(function, argumentValues);
}

void CodeGenVisitor::visit(LocalVariableDeclarationAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating LocalVariableDeclarationAST" << std::endl;
#endif

	llvm::Function *function = _mainContext.getIRBuilder().GetInsertBlock()->getParent();

	node->setAlloca(_mainContext.createEntryBlockAlloca(function, node, _mainContext));

	if (node->getExpression() != nullptr)
	{
		llvm::Value *expressionValue = this->getLLVMValue(node->getExpression());

		AType const *leftType = node->getType();
		AType const *rightType = node->getExpression()->getExpressionType();


		if (*leftType == *rightType)
			_mainContext.getIRBuilder().CreateStore(expressionValue, node->getAlloca());
		else if (*leftType > *rightType)
			_mainContext.getIRBuilder().CreateStore(_mainContext.doPromotion(expressionValue, rightType, leftType), node->getAlloca());
		else if (*leftType < *rightType)
			_mainContext.getIRBuilder().CreateStore(_mainContext.doDemotion(expressionValue, rightType, leftType), node->getAlloca());
	}

	this->storeLLVMValue(node->getAlloca());
}

void CodeGenVisitor::visit(AssignmentAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating AssignmentAST" << std::endl;
#endif

	llvm::Value *expressionValue = this->getLLVMValue(node->getExpression());

	AType const *leftType = node->getLocalVariableDeclaration()->getType();
	AType const *rightType = node->getExpression()->getExpressionType();

	if (*leftType == *rightType)
	{
		_mainContext.getIRBuilder().CreateStore(expressionValue, node->getLocalVariableDeclaration()->getAlloca());
	}
	else if (*leftType > *rightType)
	{
		_mainContext.getIRBuilder().CreateStore(_mainContext.doPromotion(expressionValue, rightType, leftType), node->getLocalVariableDeclaration()->getAlloca());
	}
	else if (*leftType < *rightType)
	{
		_mainContext.getIRBuilder().CreateStore(_mainContext.doDemotion(expressionValue, rightType, leftType), node->getLocalVariableDeclaration()->getAlloca());
	}
}

void CodeGenVisitor::visit(PointerAssignmentAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Generating PointerAssignmentAST" << std::endl;
#endif

	llvm::Value *expressionValue = this->getLLVMValue(node->getExpression());

	AType const *leftType = node->getLocalVariableDeclaration()->getType();

	for (unsigned char i = 0; i < node->getPointerContent()->getReferenceNumber(); ++i)
		leftType = static_cast<const APointer *>(leftType)->getPointee();

	AType const *rightType = node->getExpression()->getExpressionType();

	std::vector<llvm::Value *> indices(2);
	indices[0] = llvm::ConstantInt::get(_mainContext.getLLVMContext(), llvm::APInt(32, 0, true));
	indices[1] = this->getLLVMValue(node->getPointerContent()->getIndex());

	llvm::Value *loadedValue;
	if (node->getLocalVariableDeclaration()->getType()->getTypeTag() == AType::T_ARRAY)
	{
		loadedValue = _mainContext.getIRBuilder().CreateGEP(node->getLocalVariableDeclaration()->getType()->getRawType(_mainContext),
					  node->getLocalVariableDeclaration()->getAlloca(), indices);

		this->storeLLVMValue(_mainContext.getIRBuilder().CreateLoad(loadedValue));
	}
	else if (node->getLocalVariableDeclaration()->getType()->getTypeTag() == AType::T_POINTER)
	{
		loadedValue = _mainContext.getIRBuilder().CreateLoad(node->getLocalVariableDeclaration()->getAlloca());

		for (unsigned char i = 0; i < node->getPointerContent()->getReferenceNumber()-1; ++i)
			loadedValue = _mainContext.getIRBuilder().CreateLoad(loadedValue);
	}

	if (*leftType == *rightType)
	{
		_mainContext.getIRBuilder().CreateStore(expressionValue, loadedValue);
	}
	else if (*leftType > *rightType)
	{
		_mainContext.getIRBuilder().CreateStore(_mainContext.doPromotion(expressionValue, rightType, leftType), loadedValue);
	}
	else if (*leftType < *rightType)
	{
		_mainContext.getIRBuilder().CreateStore(_mainContext.doDemotion(expressionValue, rightType, leftType), loadedValue);
	}
}

llvm::Value *CodeGenVisitor::getLLVMValue(BaseNodeAST const *node)
{
	node->accept(*this);
	return _value;
}

llvm::Value *CodeGenVisitor::getCurrentLLVMValue()
{
	return _value;
}

void CodeGenVisitor::storeLLVMValue(llvm::Value *value)
{
	_value = value;
}
