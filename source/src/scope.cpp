#include "../include/scope.hpp"
#include "../include/statement_ast.hpp"

Scope::Scope(PrototypeAST const *prototype)
    : _prototype {prototype}
{}

AType const *Scope::getReturnType() const
{
    return _prototype->getReturnType();
}

PrototypeAST const *Scope::getPrototype() const
{
    return _prototype;
}

std::vector<LocalVariableDeclarationAST const *> const &Scope::getSymbolTable() const
{
    return _variables;
}

std::vector<LocalVariableDeclarationAST const *> &Scope::updateSymbolTable()
{
    return _variables;
}
