#include "../include/statement_ast.hpp"
#include "../include/main_context.hpp"
#include "../include/atype.hpp"
#include "../include/debug.hpp"
#include "../include/visitor.hpp"
#include "../include/expression_ast.hpp"
#include "../include/pointer_content.hpp"

//-----------------------------------------------------------------------------
// BaseStatementAST
//-----------------------------------------------------------------------------
BaseStatementAST::~BaseStatementAST()
{}

//-----------------------------------------------------------------------------
// BlockAST
//-----------------------------------------------------------------------------
BlockAST::BlockAST()
{}

BlockAST::~BlockAST()
{
	for (BaseStatementAST const *statementPtr : _block)
		delete statementPtr;
}

void BlockAST::addStatement(BaseStatementAST const *statement)
{
	_block.push_back(statement);
}

std::vector<BaseStatementAST const *> const &BlockAST::getStatements() const
{
	return _block;
}

void BlockAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// PrototypeAST
//-----------------------------------------------------------------------------
PrototypeAST::PrototypeAST(std::string const &functionName, std::vector<LocalVariableDeclarationAST const *> const &parameters, AType const *returnType, std::string const &prototypeType)
	: _functionName {functionName},
	  _parameters {parameters},
	  _returnType {returnType->clone()},
	  _prototypeType {prototypeType}
{}

PrototypeAST::~PrototypeAST()
{
	for (LocalVariableDeclarationAST const *p : _parameters)
		delete p;

	delete _returnType;
}

std::string const &PrototypeAST::getFunctionName() const
{
	return _functionName;
}

AType const *PrototypeAST::getReturnType() const
{
	return _returnType;
}

std::vector<const LocalVariableDeclarationAST *> const &PrototypeAST::getParameters() const
{
	return _parameters;
}

void PrototypeAST::setLineNumber(unsigned long lineNumber)
{
	_lineNumber = lineNumber;
}

unsigned long PrototypeAST::getLineNumber() const
{
	return _lineNumber;
}

std::string const &PrototypeAST::getPrototypeType() const
{
	return _prototypeType;
}

void PrototypeAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// FunctionDefinitionAST                                                      |
//-----------------------------------------------------------------------------
FunctionDefinitionAST::FunctionDefinitionAST(PrototypeAST const *prototype, BlockAST const *body)
	: _prototype {prototype},
	  _body {body}
{}

FunctionDefinitionAST::~FunctionDefinitionAST()
{
	delete _prototype;
	delete _body;
}

std::string const &FunctionDefinitionAST::getFunctionName() const
{
	return _prototype->getFunctionName();
}

AType const *FunctionDefinitionAST::getReturnType() const
{
	return _prototype->getReturnType();
}

std::vector<LocalVariableDeclarationAST const *> const &FunctionDefinitionAST::getParameters() const
{
	return _prototype->getParameters();
}

PrototypeAST const *FunctionDefinitionAST::getPrototype() const
{
	return _prototype;
}

void FunctionDefinitionAST::setLineNumber(unsigned long lineNumber)
{
	_lineNumber = lineNumber;
}

unsigned long FunctionDefinitionAST::getLineNumber() const
{
	return _lineNumber;
}

BlockAST const *FunctionDefinitionAST::getBody() const
{
	return _body;
}

void FunctionDefinitionAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// ReturnStatementAST
//-----------------------------------------------------------------------------
ReturnStatementAST::ReturnStatementAST(BaseExpressionAST const *returnExpression)
	: _returnExpression {returnExpression->clone()},
	  _scopeReturnType {new AUnknown()}
{}

ReturnStatementAST::~ReturnStatementAST()
{
	delete _returnExpression;
	delete _scopeReturnType;
}

void ReturnStatementAST::setLineNumber(unsigned long lineNumber)
{
	_lineNumber = lineNumber;
}

unsigned long ReturnStatementAST::getLineNumber() const
{
	return _lineNumber;
}

AType const *ReturnStatementAST::getScopeReturnType() const
{
	return _scopeReturnType;
}

void ReturnStatementAST::replaceType(AType const *type) const
{
	delete _scopeReturnType;
	_scopeReturnType = type;
}

BaseExpressionAST const *ReturnStatementAST::getReturnExpression() const
{
	return _returnExpression;
}

void ReturnStatementAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// WhileAST
//-----------------------------------------------------------------------------
WhileAST::WhileAST(BaseExpressionAST const *condition, BaseStatementAST const *block)
	: _condition {condition->clone()},
	  _block {block}
{}

WhileAST::~WhileAST()
{
	delete _condition;
	delete _block;
}

BaseExpressionAST const *WhileAST::getCondition() const
{
	return _condition;
}

BaseStatementAST const *WhileAST::getBlock() const
{
	return _block;
}

void WhileAST::setLineNumber(unsigned long lineNumber)
{
	_lineNumber = lineNumber;
}

unsigned long WhileAST::getLineNumber() const
{
	return _lineNumber;
}

void WhileAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// ForAST
//-----------------------------------------------------------------------------
ForAST::ForAST(LocalVariableDeclarationAST const *counter, BaseExpressionAST const *startValue, BaseExpressionAST const *endValue, BaseStatementAST const *block)
	: _counter {counter},
	  _counterType {new AUnknown()},
	  _startValue {startValue->clone()},
	  _stopValue {endValue->clone()},
	  _block {block}
{}

ForAST::~ForAST()
{
	delete _counter;
	delete _counterType;
	delete _startValue;
	delete _stopValue;
	delete _block;
}

void ForAST::setLineNumber(unsigned long lineNumber)
{
	_lineNumber = lineNumber;
}

unsigned long ForAST::getLineNumber() const
{
	return _lineNumber;
}

BaseExpressionAST const *ForAST::getStartValue() const
{
	return _startValue;
}

BaseExpressionAST const *ForAST::getStopValue() const
{
	return _stopValue;
}

BaseStatementAST const *ForAST::getBlock() const
{
	return _block;
}

LocalVariableDeclarationAST const *ForAST::getCounter() const
{
	return _counter;
}

void ForAST::replaceCounterType(AType const *counterType) const
{
	delete _counterType;
	_counterType = counterType;
}

void ForAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

AType const *ForAST::getCounterType() const
{
	return _counterType;
}

//-----------------------------------------------------------------------------
// IfThenElseAST
//-----------------------------------------------------------------------------
IfThenElseAST::IfThenElseAST(BaseExpressionAST const *condition, BaseStatementAST const *thenBlock, BaseStatementAST const *elseBlock)
	: _condition {condition->clone()},
	  _thenBlock {thenBlock},
	  _elseBlock {elseBlock}
{}

IfThenElseAST::~IfThenElseAST()
{
	delete _condition;
	delete _thenBlock;
	delete _elseBlock;
}

void IfThenElseAST::setLineNumber(unsigned long lineNumber)
{
	_lineNumber = lineNumber;
}

unsigned long IfThenElseAST::getLineNumber() const
{
	return _lineNumber;
}

BaseExpressionAST const *IfThenElseAST::getCondition() const
{
	return _condition;
}

BaseStatementAST const *IfThenElseAST::getThenBlock() const
{
	return _thenBlock;
}

BaseStatementAST const *IfThenElseAST::getElseBlock() const
{
	return _elseBlock;
}

void IfThenElseAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// IOCommandAST
//-----------------------------------------------------------------------------
IOCommandAST::IOCommandAST(std::string const &commandName, std::vector<const BaseExpressionAST *> const &pinList)
	: _commandName {commandName},
	  _pinList {pinList}
{}

IOCommandAST::IOCommandAST(std::string const &commandName, BaseExpressionAST const *pinExpression)
	: _commandName {commandName},
	  _pinExpression {pinExpression->clone()}
{}

IOCommandAST::IOCommandAST(std::string const &commandName)
	: _commandName(commandName)
{}

IOCommandAST::~IOCommandAST()
{
	if (_commandName == "init_pwm" || _commandName == "wait" || _commandName == "begin")
	{
		delete _pinExpression;
	}
	else
	{
		for (BaseExpressionAST const *pin : _pinList)
			delete pin;
	}
}

void IOCommandAST::setLineNumber(unsigned long lineNumber)
{
	_lineNumber = lineNumber;
}

unsigned long IOCommandAST::getLineNumber() const
{
	return _lineNumber;
}

std::string const &IOCommandAST::getCommandName() const
{
	return _commandName;
}

std::vector<BaseExpressionAST const *> const &IOCommandAST::getPinList() const
{
	return _pinList;
}

BaseExpressionAST const *IOCommandAST::getPinExpression() const
{
	return _pinExpression;
}

void IOCommandAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// FunctionCallStatementAST
//-----------------------------------------------------------------------------
FunctionCallStatementAST::FunctionCallStatementAST(std::string const &functionName, std::vector<BaseExpressionAST const *> const arguments)
	: _functionName {functionName},
	  _arguments {arguments}
{}

FunctionCallStatementAST::~FunctionCallStatementAST()
{
	for (BaseExpressionAST const *argument : _arguments)
		delete argument;
}

void FunctionCallStatementAST::setLineNumber(unsigned long lineNumber)
{
	_lineNumber = lineNumber;
}

unsigned long FunctionCallStatementAST::getLineNumber() const
{
	return _lineNumber;
}

std::string const &FunctionCallStatementAST::getFunctionName() const
{
	return _functionName;
}

std::vector<BaseExpressionAST const *> const &FunctionCallStatementAST::getArguments() const
{
	return _arguments;
}

void FunctionCallStatementAST::setPrototype(PrototypeAST const *prototype) const
{
	_prototype = prototype;
}

PrototypeAST const *FunctionCallStatementAST::getPrototype() const
{
	return _prototype;
}

void FunctionCallStatementAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// LocalVariableDeclarationAST
//-----------------------------------------------------------------------------
LocalVariableDeclarationAST::LocalVariableDeclarationAST(std::string const &name, AType const *type, BaseExpressionAST const *expression)
	: _name {name},
	  _type {type->clone()},
	  _expression {expression ? expression->clone() : nullptr}
{}

LocalVariableDeclarationAST::~LocalVariableDeclarationAST()
{
	if (_expression != nullptr)
		delete _expression;

	delete _type;
}

llvm::AllocaInst *LocalVariableDeclarationAST::getAlloca() const
{
	return _localVariableAlloca;
}

void LocalVariableDeclarationAST::setLineNumber(unsigned long lineNumber)
{
	_lineNumber = lineNumber;
}

unsigned long LocalVariableDeclarationAST::getLineNumber() const
{
	return _lineNumber;
}

std::string const &LocalVariableDeclarationAST::getName() const
{
	return _name;
}

AType const *LocalVariableDeclarationAST::getType() const
{
	return _type;
}

BaseExpressionAST const *LocalVariableDeclarationAST::getExpression() const
{
	return _expression;
}

void LocalVariableDeclarationAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

void LocalVariableDeclarationAST::setAlloca(llvm::AllocaInst *allocator) const
{
	_localVariableAlloca = allocator;
}

void LocalVariableDeclarationAST::replaceType(AType const *type) const
{
	delete _type;
	_type = type->clone();
}

//-----------------------------------------------------------------------------
// AssignmentAST
//-----------------------------------------------------------------------------
AssignmentAST::AssignmentAST(std::string const &variableName, BaseExpressionAST const *expression)
	: _variableName {variableName},
	  _expression {expression->clone()}
{}

AssignmentAST::~AssignmentAST()
{
	delete _expression;
}

void AssignmentAST::setLineNumber(unsigned long lineNumber)
{
	_lineNumber = lineNumber;
}

unsigned long AssignmentAST::getLineNumber() const
{
	return _lineNumber;
}

std::string const &AssignmentAST::getVaribleName() const
{
	return _variableName;
}

BaseExpressionAST const *AssignmentAST::getExpression() const
{
	return _expression;
}

LocalVariableDeclarationAST const *AssignmentAST::getLocalVariableDeclaration() const
{
	return _localVariableDeclaration;
}

void AssignmentAST::setLocalVariableDeclaration(LocalVariableDeclarationAST const *localVariableDeclaration) const
{
	_localVariableDeclaration = localVariableDeclaration;
}

void AssignmentAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}

//-----------------------------------------------------------------------------
// PointerAssignmentAST
//-----------------------------------------------------------------------------
PointerAssignmentAST::PointerAssignmentAST(PointerContent const *pointerContent, BaseExpressionAST const *expression)
	: _pointerContent {pointerContent->clone()},
	  _expression {expression->clone()}
{}

PointerAssignmentAST::~PointerAssignmentAST()
{
	delete _pointerContent;
	delete _expression;
}

void PointerAssignmentAST::setLineNumber(unsigned long lineNumber)
{
	_lineNumber = lineNumber;
}

unsigned long PointerAssignmentAST::getLineNumber() const
{
	return _lineNumber;
}

PointerContent const *PointerAssignmentAST::getPointerContent() const
{
	return _pointerContent;
}

BaseExpressionAST const *PointerAssignmentAST::getExpression() const
{
	return _expression;
}

LocalVariableDeclarationAST const *PointerAssignmentAST::getLocalVariableDeclaration() const
{
	return _localVariableDeclaration;
}

void PointerAssignmentAST::setLocalVariableDeclaration(LocalVariableDeclarationAST const *localVariableDeclaration) const
{
	_localVariableDeclaration = localVariableDeclaration;
}

void PointerAssignmentAST::accept(Visitor &visitor) const
{
	visitor.visit(this);
}
