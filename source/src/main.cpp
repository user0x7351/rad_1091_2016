#include <iostream>
#include "../include/prototype_register_visitor.hpp"
#include "../include/semantic_check_visitor.hpp"
#include "../include/code_gen_visitor.hpp"
#include "../include/main_context.hpp"
#include "../include/statement_ast.hpp"
#include "../include/debug.hpp"
#include "parser.tab.hpp"

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
    MainContext &mainContext = MainContext::GetInstance();

    if (argc != 2)
    {
        std::cerr << "[aKcent usage] # ./akcent <source_file.ak>"
                  << std::endl;

        exit(EXIT_SUCCESS);
    }
    else
    {
        mainContext.setFileName(basename(argv[argc - 1]));
    }

    extern FILE *yyin;
    yyin = fopen(argv[argc - 1], "r");
    if (yyin == nullptr)
    {
        std::cerr << "[" << mainContext.getFileName() << "] # "
                  << FRED("input file problem: ") << "can't open the file"
                  << std::endl;

        exit(EXIT_SUCCESS);
    }

    yyparse();                      // parse <source_file.ak>
    extern BlockAST *mainBlock;     // from parser.ypp
    if (mainBlock == nullptr)
    {
        std::cerr << "[" << mainContext.getFileName() << "] # "
                  << FGRN("file is empty")
                  << std::endl;

        fclose(yyin);
        exit(EXIT_SUCCESS);
    }

    PrototypeRegisterVisitor prototypeRegisterVisitor;
    if (prototypeRegisterVisitor.getSemanticState(mainBlock) == true)
    {
        SemanticCheckVisitor semanticCheckVisitor;
        if (semanticCheckVisitor.getSemanticState(mainBlock) == true)
        {
            CodeGenVisitor codeGenVisitor;
            codeGenVisitor.getLLVMValue(mainBlock);

            mainContext.printToFile();
        }

    }

    delete mainBlock;
    fclose(yyin);

    return 0;
}
