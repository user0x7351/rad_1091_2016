#include "../include/semantic_check_visitor.hpp"
#include "../include/statement_ast.hpp"
#include "../include/expression_ast.hpp"
#include "../include/main_context.hpp"
#include "../include/debug.hpp"
#include "../include/atype.hpp"
#include "../include/pointer_content.hpp"

#define SEMANTIC_ERROR this->storeSemanticState(false); return;
#define SEMANTIC_CORRECT this->storeSemanticState(true);

SemanticCheckVisitor::SemanticCheckVisitor()
	: _mainContext {MainContext::GetInstance()}
{}

SemanticCheckVisitor::~SemanticCheckVisitor()
{}

void SemanticCheckVisitor::visit(BlockAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking BlockAST" << std::endl;
#endif

	bool status = true;

	for (BaseStatementAST const *statementPtr : node->getStatements())
		status = status && this->getSemanticState(statementPtr);

	this->storeSemanticState(status);
}

void SemanticCheckVisitor::visit(PrototypeAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking PrototypeAST" << std::endl;
#endif

	// extern functions are already checked in the prototype register phase
	if (node->getPrototypeType() != "extern_function")
	{
		for (LocalVariableDeclarationAST const *parameter : node->getParameters())
		{
			if (_mainContext.updateSymbolTable().registerVariable(parameter) == false)
			{
				std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
						  << FRED("prototype parameter error: ")
						  << "duplicate parameter declaration: " << parameter->getName()
						  << " in the " << node->getFunctionName() << "(...)"
						  << std::endl;

				SEMANTIC_ERROR;
			}
		}
	}

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(FunctionDefinitionAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking FunctionDefinitionAST" << std::endl;
#endif

	_mainContext.updateSymbolTable().registerNewScope(node->getPrototype());
	bool status = this->getSemanticState(node->getPrototype()) && this->getSemanticState(node->getBody());
	_mainContext.updateSymbolTable().destroyCurrentScope();

	this->storeSemanticState(status);
}

void SemanticCheckVisitor::visit(BoolAST const *node __attribute__((unused)))
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking BoolAST" << std::endl;
#endif

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(WordAST const *node __attribute__((unused)))
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking WordAST" << std::endl;
#endif

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(DWordAST const *node __attribute__((unused)))
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking DWordAST" << std::endl;
#endif

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(ByteAST const *node __attribute__((unused)))
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking ByteAST" << std::endl;
#endif

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(RealAST const *node __attribute__((unused)))
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking RealAST" << std::endl;
#endif

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(BinaryOperatorAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking BinaryOperatorAST" << std::endl;
#endif

	if (this->getSemanticState(node->getLeftOperand()) == false)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("operator ") << node->getOperatorCode() << FRED(" error: ")
				  << "invalid left operand"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	if (this->getSemanticState(node->getRightOperand()) == false)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("operator ") << node->getOperatorCode() << FRED(" error: ")
				  << "invalid right operand"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(ArithmeticBinaryOperatorAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking ArithmeticBinaryOperatorAST" << std::endl;
#endif

	visit(static_cast<BinaryOperatorAST const *>(node));
	if (this->getCurrentSemanticState() == false)
	{
		SEMANTIC_ERROR;
	}

	AType const *leftType = node->getLeftOperand()->getExpressionType();
	AType const *rightType = node->getRightOperand()->getExpressionType();

	if (AType::areCompatible(leftType, rightType) && (leftType->getTypeTag() != AType::T_BOOL && rightType->getTypeTag() != AType::T_BOOL))
	{
		node->replaceType((*leftType > *rightType) ? leftType->clone() : rightType->clone());

		if (*leftType < *rightType)
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FGRN("operator ") << node->getOperatorCode() << FGRN(" info: ")
					  << "left operand promotion"
					  << std::endl << "\t"
					  << *leftType << node->getOperatorCode() << *rightType
					  << std::endl;
		}
		else if (*rightType < *leftType)
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FGRN("operator ") << node->getOperatorCode() << FGRN(" info: ")
					  << "right operand promotion"
					  << std::endl << "\t"
					  << *leftType << node->getOperatorCode() << *rightType
					  << std::endl;
		}
	}
	else
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("operator ") << node->getOperatorCode() << FRED(" error: ")
				  << "type mismatch"
				  << std::endl << "\t"
				  << *leftType << node->getOperatorCode() << *rightType
				  << std::endl;

		SEMANTIC_ERROR;
	}

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(ArithmeticUnaryOperatorAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking ArithmeticUnaryOperatorAST" << std::endl;
#endif

	visit(static_cast<UnaryOperatorAST const *>(node));
	if (this->getCurrentSemanticState() == false)
	{
		SEMANTIC_ERROR;
	}

	AType const *type = node->getOperand()->getExpressionType();

	if (type->getTypeClass() == AType::C_NUMBER)
	{
		node->replaceType(type->clone());
	}
	else
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("operator ") << node->getOperatorCode() << FRED(" error: ")
				  << "type mismatch"
				  << std::endl << "\t"
				  << "'-' " << *type
				  << std::endl;

		SEMANTIC_ERROR;
	}

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(RelationBinaryOperatorAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking RelationBinaryOperatorAST" << std::endl;
#endif

	visit(static_cast<BinaryOperatorAST const *>(node));
	if (this->getCurrentSemanticState() == false)
	{
		SEMANTIC_ERROR;
	}

	AType const *leftType = node->getLeftOperand()->getExpressionType();
	AType const *rightType = node->getRightOperand()->getExpressionType();

	if (AType::areCompatible(leftType, rightType) && (leftType->getTypeTag() != AType::T_BOOL && rightType->getTypeTag() != AType::T_BOOL))
	{
		node->replaceType(new ABool());

		if (*leftType < *rightType)
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FGRN("operator ") << node->getOperatorCode() << FGRN(" info: ")
					  << "left operand promotion"
					  << std::endl << "\t"
					  << *leftType << node->getOperatorCode() << *rightType
					  << std::endl;
		}
		else if (*rightType < *leftType)
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FGRN("operator ") << node->getOperatorCode() << FGRN(" info: ")
					  << "right operand promotion"
					  << std::endl << "\t"
					  << *leftType << node->getOperatorCode() << *rightType
					  << std::endl;
		}
	}
	else
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("operator ") << node->getOperatorCode() << FRED(" error: ")
				  << "type mismatch"
				  << std::endl << "\t"
				  << *leftType << node->getOperatorCode() << *rightType
				  << std::endl;

		SEMANTIC_ERROR;
	}

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(LogicBinaryOperatorAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking LogicBinaryOperatorAST" << std::endl;
#endif

	visit(static_cast<BinaryOperatorAST const *>(node));
	if (this->getCurrentSemanticState() == false)
	{
		SEMANTIC_ERROR;
	}

	AType const *leftType = node->getLeftOperand()->getExpressionType();
	AType const *rightType = node->getRightOperand()->getExpressionType();

	if (leftType->getTypeTag() == AType::T_BOOL && rightType->getTypeTag() == AType::T_BOOL)
	{
		node->replaceType(new ABool());
	}
	else
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("operator ") << node->getOperatorCode() << FRED(" error: ")
				  << "type mismatch"
				  << std::endl << "\t"
				  << *leftType << node->getOperatorCode() << *rightType
				  << std::endl;

		SEMANTIC_ERROR;
	}

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(UnaryOperatorAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking UnaryOperatorAST" << std::endl;
#endif

	if (this->getSemanticState(node->getOperand()) == false)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("operator ") << node->getOperatorCode() << FRED(" error: ")
				  << "invalid operand"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(LogicUnaryOperatorAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking LogicUnaryOperatorAST" << std::endl;
#endif

	visit(static_cast<UnaryOperatorAST const *>(node));
	if (this->getCurrentSemanticState() == false)
	{
		SEMANTIC_ERROR;
	}

	AType const *type = node->getOperand()->getExpressionType();

	if (type->getTypeTag() == AType::T_BOOL)
	{
		node->replaceType(new ABool());
	}
	else
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("operator ") << node->getOperatorCode() << FRED(" error: ")
				  << "type mismatch"
				  << std::endl << "\t"
				  << "'not' " << *type
				  << std::endl;

		SEMANTIC_ERROR;
	}

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(AdditionAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking AdditionAST" << std::endl;
#endif

	visit(static_cast<ArithmeticBinaryOperatorAST const *>(node));
	if (this->getCurrentSemanticState() == false)
	{
		SEMANTIC_ERROR;
	}
}

void SemanticCheckVisitor::visit(SubtractionAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking SubtractionAST" << std::endl;
#endif

	visit(static_cast<ArithmeticBinaryOperatorAST const *>(node));
	if (this->getCurrentSemanticState() == false)
	{
		SEMANTIC_ERROR;
	}
}

void SemanticCheckVisitor::visit(MultiplicationAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking MultiplicationAST" << std::endl;
#endif

	visit(static_cast<ArithmeticBinaryOperatorAST const *>(node));
	if (this->getCurrentSemanticState() == false)
	{
		SEMANTIC_ERROR;
	}
}

void SemanticCheckVisitor::visit(DivisionAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking DivisionAST" << std::endl;
#endif

	visit(static_cast<ArithmeticBinaryOperatorAST const *>(node));
	if (this->getCurrentSemanticState() == false)
	{
		SEMANTIC_ERROR;
	}
}

void SemanticCheckVisitor::visit(ModAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking ModAST" << std::endl;
#endif

	visit(static_cast<ArithmeticBinaryOperatorAST const *>(node));
	if (this->getCurrentSemanticState() == false)
	{
		SEMANTIC_ERROR;
	}
}

void SemanticCheckVisitor::visit(LessThanAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking LessThanAST" << std::endl;
#endif

	visit(static_cast<RelationBinaryOperatorAST const *>(node));
	if (this->getCurrentSemanticState() == false)
	{
		SEMANTIC_ERROR;
	}
}

void SemanticCheckVisitor::visit(GreaterThanAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking GreaterThanAST" << std::endl;
#endif

	visit(static_cast<RelationBinaryOperatorAST const *>(node));
	if (this->getCurrentSemanticState() == false)
	{
		SEMANTIC_ERROR;
	}
}

void SemanticCheckVisitor::visit(EqualToAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking EqualToAST" << std::endl;
#endif

	visit(static_cast<RelationBinaryOperatorAST const *>(node));
	if (this->getCurrentSemanticState() == false)
	{
		SEMANTIC_ERROR;
	}
}

void SemanticCheckVisitor::visit(NotEqualToAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking NotEqualToAST" << std::endl;
#endif

	visit(static_cast<RelationBinaryOperatorAST const *>(node));
	if (this->getCurrentSemanticState() == false)
	{
		SEMANTIC_ERROR;
	}
}

void SemanticCheckVisitor::visit(AndAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking AndAST" << std::endl;
#endif

	visit(static_cast<LogicBinaryOperatorAST const *>(node));
	if (this->getCurrentSemanticState() == false)
	{
		SEMANTIC_ERROR;
	}
}

void SemanticCheckVisitor::visit(OrAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking OrAST" << std::endl;
#endif

	visit(static_cast<LogicBinaryOperatorAST const *>(node));
	if (this->getCurrentSemanticState() == false)
	{
		SEMANTIC_ERROR;
	}
}

void SemanticCheckVisitor::visit(NotAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking NotAST" << std::endl;
#endif

	visit(static_cast<LogicUnaryOperatorAST const *>(node));
	if (this->getCurrentSemanticState() == false)
	{
		SEMANTIC_ERROR;
	}
}

void SemanticCheckVisitor::visit(NegAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking NegAST" << std::endl;
#endif

	visit(static_cast<ArithmeticUnaryOperatorAST const *>(node));
	if (this->getCurrentSemanticState() == false)
	{
		SEMANTIC_ERROR;
	}
}

void SemanticCheckVisitor::visit(EquivalentToAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking EquivalentToAST" << std::endl;
#endif

	visit(static_cast<LogicBinaryOperatorAST const *>(node));
	if (this->getCurrentSemanticState() == false)
	{
		SEMANTIC_ERROR;
	}
}

void SemanticCheckVisitor::visit(FunctionCallExpressionAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking FunctionCallExpressionAST" << std::endl;
#endif

	node->setPrototype(_mainContext.getSymbolTable().getPrototypeFromSymbolTable(node->getFunctionName()));

	if (node->getPrototype() == nullptr)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("function call error: ") << node->getFunctionName() << " not defined"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	unsigned long argSize = node->getArguments().size();
	unsigned long parSize = (node->getPrototype()->getParameters()).size();

	if (argSize != parSize)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("function call error: ")
				  << "wrong number of arguments in the " << node->getFunctionName() << "(...)"
				  << std::endl << "\t"
				  << parSize << " argument(s) expected, " << argSize << " got"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	for (unsigned int i = 0; i < node->getArguments().size(); ++i)
	{
		if (this->getSemanticState(node->getArguments()[i]) == false)
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FRED("function call argument error: ")
					  << "invalid " << (i+1) << ". argument in the " <<  node->getFunctionName() << "(...)"
					  << std::endl;

			SEMANTIC_ERROR;
		}

		AType const *leftType = node->getArguments()[i]->getExpressionType();
		AType const *rightType = ((node->getPrototype()->getParameters())[i])->getType();

		if (AType::areCompatible(leftType, rightType))
		{
			if (*leftType < *rightType)
			{
				std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
						  << FGRN("function call argument info: ")
						  << (i+1) << ". argument promotion in the " << node->getFunctionName() << "(...)"
						  << std::endl << "\t"
						  << *leftType << " ~> " << *rightType
						  << std::endl;
			}
			else if (*rightType < *leftType)
			{
				std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
						  << FYEL("function call argument warning: ")
						  << (i+1) << ". argument demotion in the " << node->getFunctionName() << "(...)"
						  << std::endl << "\t"
						  << *leftType << " ~> " << *rightType
						  << std::endl;
			}
		}
		else
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FRED("function call argument error: ")
					  << "type mismatch of the " << (i+1) << ". argument in the " <<  node->getFunctionName() << "(...)"
					  << std::endl << "\t"
					  << *rightType << " <~> " << *leftType
					  << std::endl;

			SEMANTIC_ERROR;
		}
	}

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(LocalVariableAccessAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking LocalVariableAccessAST" << std::endl;
#endif

	node->setLocalVariableDeclaration(_mainContext.getSymbolTable().getLocalVariableFromSymbolTable(node->getName()));

	if (node->getLocalVariableDeclaration() == nullptr)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("local variable access error: ")
				  << "'" << node->getName() << "' was not declared in this scope"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(LocalPointerAccessAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking LocalPointerAccessAST" << std::endl;
#endif

	node->setLocalVariableDeclaration(_mainContext.getSymbolTable().getLocalVariableFromSymbolTable(node->getPointerContent()->getIdentifier()));

	if (node->getLocalVariableDeclaration() == nullptr)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("local pointer access error: ")
				  << "'" << node->getPointerContent()->getIdentifier()
				  << "' was not declared in this scope"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	if (this->getSemanticState(node->getPointerContent()->getIndex()) == false)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("local pointer access error: index error")
				  << std::endl;

		SEMANTIC_ERROR;
	}

	AType const *dereferencedType = node->getLocalVariableDeclaration()->getType();

	for (int i = 0; i < node->getPointerContent()->getReferenceNumber(); ++i)
	{
		if (dereferencedType->getTypeTag() != AType::T_POINTER && dereferencedType->getTypeTag() != AType::T_ARRAY)
		{
			if (i == 0)
			{
				std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
						  << FRED("local pointer access error: ")
						  << "'" << node->getPointerContent()->getIdentifier()
						  << "' has non-pointer type"
						  << std::endl;
			}
			else
			{
				std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
						  << FRED("local pointer access error: ")
						  << "Number of '^' exceeded for '"
						  << node->getPointerContent()->getIdentifier() << "'"
						  << std::endl;
			}

			SEMANTIC_ERROR;
		}

		dereferencedType = static_cast<APointer const *>(dereferencedType)->getPointee();
	}

	node->replaceType(dereferencedType->clone());

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(LocalVariableAddressAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking LocalVariableAddressAST" << std::endl;
#endif

	node->setLocalVariableDeclaration(_mainContext.getSymbolTable().getLocalVariableFromSymbolTable(node->getName()));

	if (node->getLocalVariableDeclaration() == nullptr)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("local variable address error: ")
				  << "'" << node->getName() << "' was not declared in this scope"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	if (node->getIndex() != nullptr)
	{
		if (this->getSemanticState(node->getIndex()) == false)
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FRED("local variable address error: ")
					  << "invalid index expression"
					  << std::endl;

			SEMANTIC_ERROR;
		}
	}

	if ((node->getLocalVariableDeclaration()->getType()->getTypeTag() == AType::T_ARRAY) && (node->getIndex() == nullptr))
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("local variable address error: ")
				  << "'" << node->getName() << "': can't get address of the array identifier"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	if (node->getLocalVariableDeclaration()->getType()->getTypeTag() != AType::T_ARRAY)
	{
		node->replaceType(new APointer(node->getLocalVariableDeclaration()->getType()));
	}
	else
	{
		node->replaceType((node->getLocalVariableDeclaration()->getType())->clone());
	}

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(ExpressionCommandAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking ExpressionCommandAST->" << node->getCommandName() << std::endl;
#endif

	std::string const &commandName = node->getCommandName();

	if (commandName == "time" || commandName == "utime")
	{
		node->replaceType(new ADword());
	}
	else if (commandName == "receive")
	{
		node->replaceType(new AByte());
	}
	else
	{
		if (this->getSemanticState(node->getPinNumber()) == false)
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FRED("command error: ") << "invalid operand in " << commandName
					  << std::endl;

			SEMANTIC_ERROR;
		}

		AType const *compatibleType = new AWord {};
		if (AType::areCompatible(node->getPinNumber()->getExpressionType(), compatibleType) == false)
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FRED("command error: ") << "incompatible type of the operand in " << commandName
					  << std::endl;

			delete compatibleType;
			SEMANTIC_ERROR;
		}
		delete compatibleType;

		if (commandName == "read")
		{
			node->replaceType(new ABool());
		}
		else if (commandName == "adc")
		{
			node->replaceType(new AWord());
		}
	}

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(ReturnStatementAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking ReturnStatementAST" << std::endl;
#endif

	if (this->getSemanticState(node->getReturnExpression()) == false)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("'ret' statement error: ")
				  << "invalid return expression"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	AType const *expressionType = node->getReturnExpression()->getExpressionType();
	node->replaceType((_mainContext.getSymbolTable().getCurrentScopeReturnType())->clone());
	AType const *scopeReturnType = node->getScopeReturnType();

	if (AType::areCompatible(expressionType, scopeReturnType))
	{
		if (*scopeReturnType < *expressionType)
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FYEL("'ret' statement warning: ")
					  << "return expression demotion"
					  << std::endl << "\t"
					  << *expressionType << " ~> " << *scopeReturnType
					  << std::endl;
		}
		else if (*expressionType < *scopeReturnType)
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FGRN("'ret' statement info: ")
					  << "return expression promotion"
					  << std::endl << "\t"
					  << *expressionType << " ~> " << *scopeReturnType
					  << std::endl;
		}
	}
	else
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("'ret' statement error: ")
				  << "type mismatch"
				  << std::endl << "\t"
				  << *scopeReturnType << " '<~' " << *expressionType
				  << std::endl;

		SEMANTIC_ERROR;
	}

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(WhileAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking WhileAST" << std::endl;
#endif

	if (this->getSemanticState(node->getCondition()) == false)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("'while' statement error: ")
				  << "invalid condition"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	if ((node->getCondition()->getExpressionType())->getTypeTag() != AType::T_BOOL)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("'while' statement error: ")
				  << "condition has non-bool type"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	_mainContext.updateSymbolTable().registerNewScope();
	if (this->getSemanticState(node->getBlock()) == false)
	{
		SEMANTIC_ERROR;
	}
	_mainContext.updateSymbolTable().destroyCurrentScope();

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(ForAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking ForAST" << std::endl;
#endif

	_mainContext.updateSymbolTable().registerNewScope();

	if (this->getSemanticState(node->getStartValue()) == false)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("'for' statement error: ")
				  << "invalid start condition"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	if (this->getSemanticState(node->getStopValue()) == false)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("'for' statement error: ")
				  << "invalid stop condition"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	AType const *leftType = node->getStartValue()->getExpressionType();
	AType const *rightType = node->getStopValue()->getExpressionType();

	if (AType::areCompatible(leftType, rightType) && (leftType->getTypeTag() != AType::T_BOOL && rightType->getTypeTag() != AType::T_BOOL))
	{
		node->replaceCounterType((*leftType > *rightType) ? leftType->clone() : rightType->clone());

		if (*leftType < *rightType)
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FGRN("'while' statement info: ")
					  << "start condition promotion"
					  << std::endl;
		}
		else if (*rightType < *leftType)
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FGRN("'while' statement info: ")
					  << "stop condition promotion"
					  << std::endl;
		}
	}
	else
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("'while' statement error: ")
				  << "start or stop condition type mismatch"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	if (this->getSemanticState(node->getCounter()) == false)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("'while' statement error: ")
				  << "start or stop condition type mismatch"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	if (this->getSemanticState(node->getBlock()) == false)
	{
		SEMANTIC_ERROR;
	}

	_mainContext.updateSymbolTable().destroyCurrentScope();

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(IfThenElseAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking IfThenElseAST" << std::endl;
#endif

	if (this->getSemanticState(node->getCondition()) == false)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("'if' statement error: ")
				  << "invalid condition"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	if ((node->getCondition()->getExpressionType())->getTypeTag() != AType::T_BOOL)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("'if' statement error: ")
				  << "condition has non-bool type"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	_mainContext.updateSymbolTable().registerNewScope();
	if (this->getSemanticState(node->getThenBlock()) == false)
	{
		SEMANTIC_ERROR;
	}
	_mainContext.updateSymbolTable().destroyCurrentScope();

	if (node->getElseBlock() != nullptr)
	{
		_mainContext.updateSymbolTable().registerNewScope();
		if (this->getSemanticState(node->getElseBlock()) == false)
		{
			SEMANTIC_ERROR;
		}
		_mainContext.updateSymbolTable().destroyCurrentScope();
	}

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(IOCommandAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking IOCommandAST->" << node->getCommandName() << std::endl;
#endif

	std::string const &commandName = node->getCommandName();

	if (commandName == "init_pwm" || commandName == "wait" || commandName == "begin")
	{
		if (this->getSemanticState(node->getPinExpression()) == false)
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FRED("command error: ") << "invalid operand in " << commandName
					  << std::endl;

			SEMANTIC_ERROR;
		}

		AType const *compatibleType = new AWord {};
		if (AType::areCompatible(node->getPinExpression()->getExpressionType(), compatibleType) == false)
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FRED("command error: ") << "incompatible type of the operand in " << commandName
					  << std::endl;

			delete compatibleType;
			SEMANTIC_ERROR;
		}
		delete compatibleType;
	}
	else
	{
		unsigned char argumentIndex = 0;
		for (BaseExpressionAST const *pin : node->getPinList())
		{
			if (this->getSemanticState(pin) == false)
			{
				std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
						  << FRED("command error: ") << "invalid " << argumentIndex+1 << "th operand in " << commandName
						  << std::endl;

				SEMANTIC_ERROR;
			}

			AType const *compatibleType = new AWord {};
			if (AType::areCompatible(pin->getExpressionType(), compatibleType) == false)
			{
				std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
						  << FRED("command error: ") << "incompatible type of the operands in " << commandName
						  << std::endl;

				delete compatibleType;
				SEMANTIC_ERROR;
			}
			delete compatibleType;
		}
	}

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(FunctionCallStatementAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking FunctionCallStatementAST" << std::endl;
#endif

	node->setPrototype(_mainContext.getSymbolTable().getPrototypeFromSymbolTable(node->getFunctionName()));

	if (node->getPrototype() == nullptr)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("function call error: ") << node->getFunctionName() << " not defined"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	unsigned long argSize = node->getArguments().size();
	unsigned long parSize = (node->getPrototype()->getParameters()).size();

	if (argSize != parSize)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("function call error: ")
				  << "wrong number of arguments in the " << node->getFunctionName() << "(...)"
				  << std::endl << "\t"
				  << parSize << " argument(s) expected, " << argSize << " got"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	for (unsigned int i = 0; i < node->getArguments().size(); ++i)
	{
		if (this->getSemanticState(node->getArguments()[i]) == false)
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FRED("function call argument error: ")
					  << "invalid " << (i+1) << ". argument in the " <<  node->getFunctionName() << "(...)"
					  << std::endl;

			SEMANTIC_ERROR;
		}

		AType const *leftType = node->getArguments()[i]->getExpressionType();
		AType const *rightType = ((node->getPrototype()->getParameters())[i])->getType();

		if (AType::areCompatible(leftType, rightType))
		{
			if (*leftType < *rightType)
			{
				std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
						  << FGRN("function call argument info: ")
						  << (i+1) << ". argument promotion in the " << node->getFunctionName() << "(...)"
						  << std::endl << "\t"
						  << *leftType << " ~> " << *rightType
						  << std::endl;
			}
			else if (*rightType < *leftType)
			{
				std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
						  << FYEL("function call argument warning: ")
						  << (i+1) << ". argument demotion in the " << node->getFunctionName() << "(...)"
						  << std::endl << "\t"
						  << *leftType << " ~> " << *rightType
						  << std::endl;
			}
		}
		else
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FRED("function call argument error: ")
					  << "type mismatch of the " << (i+1) << ". argument in the " <<  node->getFunctionName() << "(...)"
					  << std::endl << "\t"
					  << *rightType << " <~> " << *leftType
					  << std::endl;

			SEMANTIC_ERROR;
		}
	}

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(LocalVariableDeclarationAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking LocalVariableDeclarationAST" << std::endl;
#endif

	if (_mainContext.updateSymbolTable().registerVariable(node) == false)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("'local variable declaration' error: ") << "redeclaration of '" << node->getName() << "'"
				  << std::endl << "\t"
				  << node->getName() << " " << *node->getType()
				  << std::endl;

		SEMANTIC_ERROR;
	}

	if (node->getExpression() != nullptr)
	{
		if (this->getSemanticState(node->getExpression()) == false)
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FRED("'local variable definition' error: ") << "invalid assignment to '" << node->getName() << "'"
					  << std::endl;

			SEMANTIC_ERROR;
		}

		AType const *leftType = node->getType();
		AType const *rightType = node->getExpression()->getExpressionType();

		if (AType::areCompatible(leftType, rightType))
		{
			if (*leftType > *rightType)
			{
				std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
						  << FGRN("'local variable definition' info: ") << "rhs promotion"
						  << std::endl << "\t"
						  << node->getName() << " " << *node->getType() << " = " << *rightType
						  << std::endl;
			}
			else if (*leftType < *rightType)
			{
				std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
						  << FYEL("'local variable definition' warning: ") << "rhs demotion"
						  << std::endl << "\t"
						  << node->getName() << " " << *node->getType() << " = " << *rightType
						  << std::endl;
			}
		}
		else
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FRED("'local variable definition' error: ") << "type mismatch"
					  << std::endl << "\t"
					  << node->getName() << " " << *node->getType() << " = " << *rightType
					  << std::endl;

			SEMANTIC_ERROR;
		}
	}

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(AssignmentAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking AssignmentAST" << std::endl;
#endif

	node->setLocalVariableDeclaration(_mainContext.getSymbolTable().getLocalVariableFromSymbolTable(node->getVaribleName()));

	if (node->getLocalVariableDeclaration() == nullptr)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("'assignment' error: ") << "undeclared variable '" << node->getVaribleName() << "'"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	if (this->getSemanticState(node->getExpression()) == false)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("'assignment' error: ") << "invalid expression"
				  << std::endl << "\t"
				  << node->getVaribleName() << " = ???"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	AType const *leftType = node->getLocalVariableDeclaration()->getType();
	AType const *rightType = node->getExpression()->getExpressionType();

	if (AType::areCompatible(leftType, rightType))
	{
		if (*leftType < *rightType)
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FYEL("'assignment' warning: ") << "expression demotion in '" << node->getVaribleName() << "'"
					  << std::endl << "\t"
					  << *leftType << " = " << *rightType
					  << std::endl;
		}
		else if (*rightType < *leftType)
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FGRN("'assignment' info: ") << "expression promotion in '" << node->getVaribleName() << "'"
					  << std::endl << "\t"
					  << *leftType << " = " << *rightType
					  << std::endl;
		}
	}
	else
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("'assignment' error: ") << "type mismatch" << node->getVaribleName() << "'"
				  << std::endl << "\t"
				  << *leftType << " = " << *rightType
				  << std::endl;

		SEMANTIC_ERROR;
	}

	SEMANTIC_CORRECT;
}

void SemanticCheckVisitor::visit(PointerAssignmentAST const *node)
{
#ifdef AKCENT_DEBUG
	std::cout << "Checking PointerAssignmentAST" << std::endl;
#endif

	node->setLocalVariableDeclaration(_mainContext.getSymbolTable().getLocalVariableFromSymbolTable(node->getPointerContent()->getIdentifier()));

	if (node->getLocalVariableDeclaration() == nullptr)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("'assignment' error: ") << "undeclared variable '" << node->getPointerContent()->getIdentifier() << "'"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	if (this->getSemanticState(node->getExpression()) == false)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("'assignment' error: ") << "invalid expression"
				  << std::endl << "\t"
				  << node->getPointerContent()->getIdentifier() << " = ???"
				  << std::endl;

		SEMANTIC_ERROR;
	}

	if (this->getSemanticState(node->getPointerContent()->getIndex()) == false)
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("local pointer assignment error: index error")
				  << std::endl;

		SEMANTIC_ERROR;
	}

	AType const *leftType = node->getLocalVariableDeclaration()->getType();
	for (unsigned int i = 0; i < node->getPointerContent()->getReferenceNumber(); ++i)
	{
		if (leftType->getTypeTag() != AType::T_POINTER && leftType->getTypeTag() != AType::T_ARRAY)
		{
			if (i == 0)
			{
				std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
						  << FRED("local pointer access error: ")
						  << "'" << node->getPointerContent()->getIdentifier()
						  << "' has non-pointer type"
						  << std::endl;
			}
			else
			{
				std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
						  << FRED("local pointer access error: ")
						  << "Number of '^' exceeded for '"
						  << node->getPointerContent()->getIdentifier() << "'"
						  << std::endl;
			}

			SEMANTIC_ERROR;
		}

		leftType = static_cast<const APointer *>(leftType)->getPointee();
	}

	AType const *rightType = node->getExpression()->getExpressionType();

	if (AType::areCompatible(leftType, rightType))
	{
		if (*leftType > *rightType)
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FGRN("'assignment' info: ")
					  << "expression promotion in '" << node->getPointerContent()->getIdentifier() << "'"
					  << std::endl << "\t"
					  << *leftType << " = " << *rightType
					  << std::endl;
		}
		else if (*leftType < *rightType)
		{
			std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
					  << FYEL("'assignment' warning: ")
					  << "expression demotion in '" << node->getPointerContent()->getIdentifier() << "'"
					  << std::endl << "\t"
					  << *leftType << " = " << *rightType
					  << std::endl;
		}
	}
	else
	{
		std::cerr << "[" << _mainContext.getFileName() << " | line " << node->getLineNumber() << "] # "
				  << FRED("'assignment' error: ") << "type mismatch"
				  << std::endl << "\t"
				  << *leftType << " = " << *rightType
				  << std::endl;

		SEMANTIC_ERROR;
	}

	SEMANTIC_CORRECT;
}

bool SemanticCheckVisitor::getSemanticState(BaseNodeAST const *node)
{
	node->accept(*this);
	return _state;
}

bool SemanticCheckVisitor::getCurrentSemanticState()
{
	return _state;
}

void SemanticCheckVisitor::storeSemanticState(bool state)
{
	_state = state;
}
