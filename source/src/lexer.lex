%option noyywrap
%option noinput
%option nounput
%option yylineno

CHARACTER       \'[A-Za-z0-9]\'
IDENTIFIER      [a-zA-Z_][a-zA-Z0-9_]*
INTEGER_NUMBER  (0|([1-9][0-9]*))
REAL_NUMBER     [0-9]+[.][0-9]+
COMMENT         \/\/.*
WHITESPACE      [\t\n ]

%{
    #include <iostream>
    #include <cstdlib>
    #include <string>

    #include "../include/statement_ast.hpp"
    #include "../include/expression_ast.hpp"
    #include "parser.tab.hpp"
%}

%%
"function"                      return FUNCTION_TOKEN;
"extern"                        return EXTERN_TOKEN;
"ret"                           return RETURN_TOKEN;

"forever"                       return FOREVER_TOKEN;
"while"                         return WHILE_TOKEN;
"for"                           return FOR_TOKEN;
"in"                            return IN_TOKEN;
"do"                            return DO_TOKEN;

"if"                            return IF_TOKEN;
"else"                          return ELSE_TOKEN;
"..."                           return TRIPLE_DOT_TOKEN;

"output"                        return OUTPUT_TOKEN;
"input"                         return INPUT_TOKEN;
"high"                          return HIGH_TOKEN;
"low"                           return LOW_TOKEN;
"toggle"                        return TOGGLE_TOKEN;
"read"                          return READ_TOKEN;

"init_pwm"                      return INIT_PWM_TOKEN;
"pwm"                           return PWM_TOKEN;

"setInternalAREF"               return INTERNAL_AREF_TOKEN;
"setExternalAREF"               return EXTERNAL_AREF_TOKEN;
"adc"                           return ADC_TOKEN;

"wait"                          return WAIT_TOKEN;
"time"                          return TIME_TOKEN;
"utime"                         return UTIME_TOKEN;

"begin"                         return BEGIN_TOKEN;
"send"                          return SEND_TOKEN;
"receive"                       return RECEIVE_TOKEN;

"i16"                           return WORD_TOKEN;
"i32"                           return DWORD_TOKEN;
"i8"                            return BYTE_TOKEN;
"void"                          return VOID_TOKEN;
"bool"                          return BOOL_TOKEN;
"real"                          return REAL_TOKEN;

"true"                          return TRUE_TOKEN;
"false"                         return FALSE_TOKEN;

"and"                           return AND_TOKEN;
"or"                            return OR_TOKEN;
"not"                           return NOT_TOKEN;
"<=>"                           return EQUIVALENT_TOKEN;

"+"                             return PLUS_TOKEN;
"-"                             return MINUS_TOKEN;
"*"                             return ASTERISK_TOKEN;
"/"                             return SLASH_TOKEN;
"%"                             return MOD_TOKEN;
"&"                             return AMPERSAND_TOKEN;
"<"                             return LT_TOKEN;
">"                             return GT_TOKEN;
"=="                            return EQ_TOKEN;
"!="                            return NE_TOKEN;
"\^"                            return FLY_TOKEN;

"["                             return LSB_TOKEN;
"]"                             return RSB_TOKEN;
"("                             return LP_TOKEN;
")"                             return RP_TOKEN;
"{"                             return LB_TOKEN;
"}"                             return RB_TOKEN;

"."                             return DOT_TOKEN;
","                             return COMMA_TOKEN;
";"                             return SEMICOLON_TOKEN;
":"                             return COLON_TOKEN;
"="                             return ASSIGNMENT_TOKEN;

{CHARACTER}						yylval._stringValueToken = new std::string(yytext); return CHAR_LITERAL;
{IDENTIFIER}					yylval._stringValueToken = new std::string(yytext); return IDENTIFIER_TOKEN;
{INTEGER_NUMBER}				yylval._stringValueToken = new std::string(yytext); return NUMBER_TOKEN;
{REAL_NUMBER}					yylval._stringValueToken = new std::string(yytext); return REAL_NUMBER_TOKEN;
{COMMENT}						{}
{WHITESPACE}					{}
.                               {
                                    std::cerr << "[lexical error] # Unknown lexeme: '" << *yytext << "'" << std::endl;
									exit(EXIT_FAILURE);
								}
%%
