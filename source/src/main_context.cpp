#include <cassert>
#include "../include/main_context.hpp"
#include "../include/statement_ast.hpp"
#include "../include/atype.hpp"

#define MODULE_NAME "aKcent_module"

MainContext::MainContext()
    : _llvmModule {MODULE_NAME, _llvmContext},
      _irBuilder {_llvmContext}
{}

MainContext &MainContext::GetInstance()
{
    static MainContext MainContext;
    return MainContext;
}

llvm::Module *MainContext::getLLVMModule()
{
    return &_llvmModule;
}

llvm::IRBuilder<> &MainContext::getIRBuilder()
{
    return _irBuilder;
}

llvm::LLVMContext &MainContext::getLLVMContext()
{
    return _llvmContext;
}

SymbolTable &MainContext::updateSymbolTable()
{
    return _symbolTable;
}

const SymbolTable &MainContext::getSymbolTable() const
{
    return _symbolTable;
}

llvm::Value *MainContext::doPromotion(llvm::Value *value, AType const *fromType, AType const *toType)
{
    if (*fromType == *toType)
    {
        return value;
    }
    else
    {
        switch (toType->getTypeTag())
        {
            case AType::T_REAL:
                return new llvm::SIToFPInst(value, llvm::Type::getFloatTy(_llvmContext), "ext_to_real", _irBuilder.GetInsertBlock());
            case AType::T_DWORD:
                return new llvm::SExtInst(value, llvm::Type::getInt32Ty(_llvmContext), "ext_to_dword", _irBuilder.GetInsertBlock());
            case AType::T_WORD:
                return new llvm::SExtInst(value, llvm::Type::getInt16Ty(_llvmContext), "ext_to_word", _irBuilder.GetInsertBlock());
            default:
                assert(false);
        }
    }
}

llvm::Value *MainContext::doDemotion(llvm::Value *value, AType const *fromType, AType const *toType)
{
    if (*fromType == *toType)
    {
        return value;
    }
    else
    {
        if (fromType->getTypeTag() != AType::T_REAL)
        {
            switch (toType->getTypeTag())
            {
                case AType::T_REAL:
                    return new llvm::TruncInst(value, llvm::Type::getFloatTy(_llvmContext), "trunc_to_real", _irBuilder.GetInsertBlock());
                case AType::T_DWORD:
                    return new llvm::TruncInst(value, llvm::Type::getInt32Ty(_llvmContext), "trunc_to_dword", _irBuilder.GetInsertBlock());
                case AType::T_WORD:
                    return new llvm::TruncInst(value, llvm::Type::getInt16Ty(_llvmContext), "trunc_to_word", _irBuilder.GetInsertBlock());
                case AType::T_BYTE:
                    return new llvm::TruncInst(value, llvm::Type::getInt8Ty(_llvmContext), "trunc_to_byte", _irBuilder.GetInsertBlock());
                default:
                    assert(false);
            }
        }
        else
        {
            switch (toType->getTypeTag())
            {
                case AType::T_WORD:
                    return new llvm::FPToSIInst(value, llvm::Type::getInt16Ty(_llvmContext), "trunc_to_word", _irBuilder.GetInsertBlock());
                case AType::T_BYTE:
                    return new llvm::FPToSIInst(value, llvm::Type::getInt8Ty(_llvmContext), "trunc_to_byte", _irBuilder.GetInsertBlock());
                default:
                    assert(false);
            }
        }
    }
}

llvm::AllocaInst *MainContext::createEntryBlockAlloca(llvm::Function *function, LocalVariableDeclarationAST const *variable, MainContext &mainContext) const
{
    llvm::IRBuilder<> irBuilderTmp(&function->getEntryBlock(), function->getEntryBlock().begin());
    return irBuilderTmp.CreateAlloca(variable->getType()->getRawType(mainContext), nullptr, variable->getName());
}

void MainContext::setFileName(std::string const &fileName)
{
    _filename = fileName;
}

std::string const &MainContext::getFileName() const
{
    return _filename;
}

void MainContext::printToFile() const
{
#include <stdio.h>
    std::string filename = _filename.substr(0, _filename.find_last_of(".")) + ".ll";
    FILE *outputFile = freopen(filename.c_str(), "w", stderr);
    _llvmModule.print(llvm::errs(), nullptr);
    fclose(outputFile);
}
