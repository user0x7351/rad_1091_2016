## aKcent - programski jezik (master rad)
---

#### Struktura repozitorijuma
* **latex_project** - pisani deo rada
* **prijava_teme** - molba za odobravanje teme master rada
* **source** - izvorni kod kompilatora
* **examples** - test primeri
* **temp_compilation_files** - direktorijum u kom se vrši kompilacija .ak programa (sadrži test primere)
* **akcent_lib** - statička biblioteka koja podržava rad ATmega328P mikrokontrolera

---

#### Kompilacija kompilatora (Ubuntu 18.04 x64)
* Neophodno je instalirati sledeće programe:
    * **g++** - `sudo apt-get install g++`
    * **git** - `sudo apt-get install git` 
    * **cmake** - `sudo apt-get install cmake`
    * **flex** - `sudo apt-get install flex`
    * **bison** - `sudo apt-get install bison`
* Preuzeti i raspakovati **LLVM 6.0** (http://releases.llvm.org/download.html#6.0.0)
* Klonirati repozitorijum
    * `git clone https://bitbucket.org/user0x7351/rad_1091_2016.git`
* U datoteci **source/CMakeLists.txt** za **LLVM_DIR** postaviti putanju koja odgovara direktorijumu **lib/cmake/llvm**, koji se nalazi u okviru preuzetog LLVM paketa
* U okviru direktorijuma **source** napraviti direktorijum **build** i pozicionirati se unutar njega
* Izvršiti sledeće komande:
    * `cmake ..` 
    * `make -B -j8`
* Preporučuje se korišćenje Qt Creator-a, koji će prethodna dva koraka automatski izvršiti

---

#### Kompilacija statičke biblioteke (Ubuntu 18.04 x64)
* Neophodno je instalirati sledeće programe:
    * **gcc-avr** - `sudo apt-get install gcc-avr` 
    * **binutils-avr** - `sudo apt-get install binutils-avr`
    * **avr-libc** - `sudo apt-get install avr-libc`
    * **avrdude** - `sudo apt-get install avrdude`
* Pozicionirati se unutar **akcent_lib** direktorijuma
* Izvršiti komandu:
    * `make -B -j8`

---

#### Kompilacija programa, linkovanje i upload
* Preuzeti i kompajlirati **AVR-LLVM** u skladu sa uputstvima (https://github.com/avr-llvm/llvm)
* Pozicionirati se u direktorijum **temp_compilation_files** 
* U okviru **Makefile** datoteke, za **AVR_LLVM_DIRECTORY**, postaviti putanju koja odgovara **build/bin** direktorijumu koji se nalazi u okviru preuzetog AVR-LLVM paketa, podesiti odgovarajući port (obično je to port *ttyACM0* ili *ttyUSB0*) i putanju do *aKcent* kompilatora
* Proces kompilacije za ulazni program *01_blink.ak* moguće je pokrenuti narednom komandom (analogno i za bilo koju drugu .ak datoteku):
    * `make PROGRAM=01_blink`

---

#### Autor i kontakt
* Đorđe Milićević, user0x32@gmail.com

