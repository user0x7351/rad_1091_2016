; ModuleID = '11_matrix.ll'
source_filename = "aKcent_module"

define void @main() {
function_block:
  %matrix = alloca [12 x i8]
  call void @init()
  %trunc_to_byte = trunc i16 18 to i8
  call void @output(i8 %trunc_to_byte)
  %trunc_to_byte1 = trunc i16 18 to i8
  call void @low(i8 %trunc_to_byte1)
  br label %for_condition

for_condition:                                    ; preds = %for_continue, %function_block
  %i.0 = phi i16 [ 0, %function_block ], [ %8, %for_continue ]
  %0 = icmp slt i16 %i.0, 3
  br i1 %0, label %for_loop, label %for_continue6

for_loop:                                         ; preds = %for_condition
  br label %for_condition2

for_condition2:                                   ; preds = %for_loop3, %for_loop
  %j.0 = phi i16 [ 0, %for_loop ], [ %7, %for_loop3 ]
  %1 = icmp slt i16 %j.0, 4
  br i1 %1, label %for_loop3, label %for_continue

for_loop3:                                        ; preds = %for_condition2
  %2 = add i16 %i.0, 1
  %3 = mul i16 %i.0, 4
  %4 = add i16 %3, %j.0
  %5 = getelementptr [12 x i8], [12 x i8]* %matrix, i32 0, i16 %4
  %6 = load i8, i8* %5
  %trunc_to_byte4 = trunc i16 %2 to i8
  store i8 %trunc_to_byte4, i8* %5
  %ext_to_word = sext i8 1 to i16
  %7 = add i16 %j.0, %ext_to_word
  br label %for_condition2

for_continue:                                     ; preds = %for_condition2
  %ext_to_word5 = sext i8 1 to i16
  %8 = add i16 %i.0, %ext_to_word5
  br label %for_condition

for_continue6:                                    ; preds = %for_condition
  %trunc_to_byte7 = trunc i16 0 to i8
  br label %for_condition9

for_condition9:                                   ; preds = %for_loop10, %for_continue6
  %secondRowSum.0 = phi i8 [ %trunc_to_byte7, %for_continue6 ], [ %13, %for_loop10 ]
  %i8.0 = phi i16 [ 0, %for_continue6 ], [ %14, %for_loop10 ]
  %9 = icmp slt i16 %i8.0, 4
  br i1 %9, label %for_loop10, label %for_continue12

for_loop10:                                       ; preds = %for_condition9
  %10 = add i16 4, %i8.0
  %11 = getelementptr [12 x i8], [12 x i8]* %matrix, i32 0, i16 %10
  %12 = load i8, i8* %11
  %13 = add i8 %secondRowSum.0, %12
  %ext_to_word11 = sext i8 1 to i16
  %14 = add i16 %i8.0, %ext_to_word11
  br label %for_condition9

for_continue12:                                   ; preds = %for_condition9
  br label %if_condition

if_condition:                                     ; preds = %for_continue12
  %ext_to_word13 = sext i8 %secondRowSum.0 to i16
  %15 = icmp eq i16 %ext_to_word13, 8
  br i1 %15, label %then_block, label %if_continue

then_block:                                       ; preds = %if_condition
  %trunc_to_byte14 = trunc i16 18 to i8
  call void @high(i8 %trunc_to_byte14)
  br label %if_continue

if_continue:                                      ; preds = %then_block, %if_condition
  ret void
}

declare void @init()

declare void @output(i8)

declare void @low(i8)

declare void @high(i8)
