; ModuleID = '05_pwm.ll'
source_filename = "aKcent_module"

define void @main() {
function_block:
  call void @init()
  %trunc_to_byte = trunc i16 10 to i8
  call void @init_pwm(i8 %trunc_to_byte)
  %trunc_to_byte1 = trunc i16 5 to i8
  br label %while_condition

while_condition:                                  ; preds = %if_continue, %function_block
  %brightness.0 = phi i16 [ 5, %function_block ], [ %4, %if_continue ]
  %fadeAmount.0 = phi i8 [ %trunc_to_byte1, %function_block ], [ %fadeAmount.1, %if_continue ]
  br i1 true, label %while_loop, label %while_continue

while_loop:                                       ; preds = %while_condition
  br label %if_condition

if_condition:                                     ; preds = %while_loop
  %0 = icmp eq i16 %brightness.0, 0
  %1 = icmp eq i16 %brightness.0, 255
  %2 = or i1 %0, %1
  br i1 %2, label %then_block, label %if_continue

then_block:                                       ; preds = %if_condition
  %3 = sub i8 0, %fadeAmount.0
  br label %if_continue

if_continue:                                      ; preds = %then_block, %if_condition
  %fadeAmount.1 = phi i8 [ %3, %then_block ], [ %fadeAmount.0, %if_condition ]
  %trunc_to_byte2 = trunc i16 10 to i8
  call void @pwm(i8 %trunc_to_byte2, i16 %brightness.0)
  %ext_to_word = sext i8 %fadeAmount.1 to i16
  %4 = add i16 %brightness.0, %ext_to_word
  %ext_to_dword = sext i16 30 to i32
  call void @wait(i32 %ext_to_dword)
  br label %while_condition

while_continue:                                   ; preds = %while_condition
  ret void
}

declare void @init()

declare void @init_pwm(i8)

declare void @pwm(i8, i16)

declare void @wait(i32)
