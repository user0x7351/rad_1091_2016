; ModuleID = '10_fibonacci.ll'
source_filename = "aKcent_module"

define void @main() {
function_block:
  call void @init()
  %trunc_to_byte = trunc i16 11 to i8
  call void @output(i8 %trunc_to_byte)
  br label %if_condition

if_condition:                                     ; preds = %function_block
  %trunc_to_byte1 = trunc i16 8 to i8
  %function_call_expression = call i8 @fibonacci(i8 %trunc_to_byte1)
  %ext_to_word = sext i8 %function_call_expression to i16
  %0 = icmp eq i16 %ext_to_word, 21
  br i1 %0, label %then_block, label %if_continue

then_block:                                       ; preds = %if_condition
  %trunc_to_byte2 = trunc i16 11 to i8
  call void @high(i8 %trunc_to_byte2)
  br label %if_continue

if_continue:                                      ; preds = %then_block, %if_condition
  ret void
}

define i8 @fibonacci(i8 %number_PARAM_) {
function_block:
  %niz = alloca [20 x i8]
  %0 = getelementptr [20 x i8], [20 x i8]* %niz, i32 0, i16 0
  %1 = load i8, i8* %0
  %trunc_to_byte = trunc i16 0 to i8
  store i8 %trunc_to_byte, i8* %0
  %2 = getelementptr [20 x i8], [20 x i8]* %niz, i32 0, i16 1
  %3 = load i8, i8* %2
  %trunc_to_byte1 = trunc i16 1 to i8
  store i8 %trunc_to_byte1, i8* %2
  br label %for_condition

for_condition:                                    ; preds = %for_loop, %function_block
  %i.0 = phi i16 [ 2, %function_block ], [ %15, %for_loop ]
  %ext_to_word = sext i8 %number_PARAM_ to i16
  %4 = add i16 %ext_to_word, 1
  %5 = icmp slt i16 %i.0, %4
  br i1 %5, label %for_loop, label %for_continue

for_loop:                                         ; preds = %for_condition
  %6 = sub i16 %i.0, 1
  %7 = getelementptr [20 x i8], [20 x i8]* %niz, i32 0, i16 %6
  %8 = load i8, i8* %7
  %9 = sub i16 %i.0, 2
  %10 = getelementptr [20 x i8], [20 x i8]* %niz, i32 0, i16 %9
  %11 = load i8, i8* %10
  %12 = add i8 %8, %11
  %13 = getelementptr [20 x i8], [20 x i8]* %niz, i32 0, i16 %i.0
  %14 = load i8, i8* %13
  store i8 %12, i8* %13
  %ext_to_word2 = sext i8 1 to i16
  %15 = add i16 %i.0, %ext_to_word2
  br label %for_condition

for_continue:                                     ; preds = %for_condition
  %16 = getelementptr [20 x i8], [20 x i8]* %niz, i32 0, i8 %number_PARAM_
  %17 = load i8, i8* %16
  ret i8 %17
}

declare void @init()

declare void @output(i8)

declare void @high(i8)
