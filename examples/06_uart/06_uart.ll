; ModuleID = '06_uart.ll'
source_filename = "aKcent_module"

declare i8 @available()

define void @main() {
function_block:
  %array_2 = alloca [2 x i8*]
  %array = alloca [6 x i8]
  call void @init()
  %ext_to_dword = sext i16 9600 to i32
  call void @begin(i32 %ext_to_dword)
  %0 = getelementptr [6 x i8], [6 x i8]* %array, i32 0, i16 0
  %1 = load i8, i8* %0
  store i8 97, i8* %0
  %2 = getelementptr [6 x i8], [6 x i8]* %array, i32 0, i16 1
  %3 = load i8, i8* %2
  store i8 75, i8* %2
  %4 = getelementptr [6 x i8], [6 x i8]* %array, i32 0, i16 2
  %5 = load i8, i8* %4
  store i8 99, i8* %4
  %6 = getelementptr [6 x i8], [6 x i8]* %array, i32 0, i16 3
  %7 = load i8, i8* %6
  store i8 101, i8* %6
  %8 = getelementptr [6 x i8], [6 x i8]* %array, i32 0, i16 4
  %9 = load i8, i8* %8
  store i8 110, i8* %8
  %10 = getelementptr [6 x i8], [6 x i8]* %array, i32 0, i16 5
  %11 = load i8, i8* %10
  store i8 116, i8* %10
  %12 = getelementptr [6 x i8], [6 x i8]* %array, i32 0, i32 0
  call void @echo(i8* %12)
  %13 = getelementptr [6 x i8], [6 x i8]* %array, i32 0, i16 5
  call void @echoLastCharacter(i8* %13)
  %14 = getelementptr [6 x i8], [6 x i8]* %array, i32 0, i16 0
  %15 = getelementptr [2 x i8*], [2 x i8*]* %array_2, i32 0, i16 0
  %16 = load i8*, i8** %15
  store i8* %14, i8** %15
  %17 = getelementptr [6 x i8], [6 x i8]* %array, i32 0, i16 1
  %18 = getelementptr [2 x i8*], [2 x i8*]* %array_2, i32 0, i16 1
  %19 = load i8*, i8** %18
  store i8* %17, i8** %18
  %20 = getelementptr [2 x i8*], [2 x i8*]* %array_2, i32 0, i32 0
  call void @echo_2(i8** %20)
  br label %while_condition

while_condition:                                  ; preds = %if_continue, %function_block
  br i1 true, label %while_loop, label %while_continue

while_loop:                                       ; preds = %while_condition
  br label %if_condition

if_condition:                                     ; preds = %while_loop
  %function_call_expression = call i8 @available()
  %ext_to_word = sext i8 %function_call_expression to i16
  %21 = icmp sgt i16 %ext_to_word, 0
  br i1 %21, label %then_block, label %if_continue

then_block:                                       ; preds = %if_condition
  %22 = call i8 @receive()
  call void @send(i8 %22)
  br label %if_continue

if_continue:                                      ; preds = %then_block, %if_condition
  br label %while_condition

while_continue:                                   ; preds = %while_condition
  ret void
}

define void @echo(i8* %array_PARAM_) {
function_block:
  br label %for_condition

for_condition:                                    ; preds = %for_loop, %function_block
  %index.0 = phi i16 [ 0, %function_block ], [ %3, %for_loop ]
  %0 = icmp slt i16 %index.0, 6
  br i1 %0, label %for_loop, label %for_continue

for_loop:                                         ; preds = %for_condition
  %1 = getelementptr i8, i8* %array_PARAM_, i16 %index.0
  %2 = load i8, i8* %1
  call void @send(i8 %2)
  %ext_to_dword = sext i16 1000 to i32
  call void @wait(i32 %ext_to_dword)
  %ext_to_word = sext i8 1 to i16
  %3 = add i16 %index.0, %ext_to_word
  br label %for_condition

for_continue:                                     ; preds = %for_condition
  ret void
}

define void @echo_2(i8** %array_PARAM_) {
function_block:
  br label %for_condition

for_condition:                                    ; preds = %for_loop, %function_block
  %index.0 = phi i16 [ 0, %function_block ], [ %4, %for_loop ]
  %0 = icmp slt i16 %index.0, 2
  br i1 %0, label %for_loop, label %for_continue

for_loop:                                         ; preds = %for_condition
  %1 = load i8*, i8** %array_PARAM_
  %2 = getelementptr i8, i8* %1, i16 %index.0
  %3 = load i8, i8* %2
  call void @send(i8 %3)
  %ext_to_dword = sext i16 1000 to i32
  call void @wait(i32 %ext_to_dword)
  %ext_to_word = sext i8 1 to i16
  %4 = add i16 %index.0, %ext_to_word
  br label %for_condition

for_continue:                                     ; preds = %for_condition
  ret void
}

define void @echoLastCharacter(i8* %lastCharacter_PARAM_) {
function_block:
  %0 = getelementptr i8, i8* %lastCharacter_PARAM_, i16 0
  %1 = load i8, i8* %0
  call void @send(i8 %1)
  %ext_to_dword = sext i16 1000 to i32
  call void @wait(i32 %ext_to_dword)
  ret void
}

declare void @init()

declare void @begin(i32)

declare i8 @receive()

declare void @send(i8)

declare void @wait(i32)
