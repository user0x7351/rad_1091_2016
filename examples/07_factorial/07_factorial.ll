; ModuleID = '07_factorial.ll'
source_filename = "aKcent_module"

define void @main() {
function_block:
  call void @init()
  %trunc_to_byte = trunc i16 11 to i8
  call void @output(i8 %trunc_to_byte)
  %trunc_to_byte1 = trunc i16 18 to i8
  call void @output(i8 %trunc_to_byte1)
  br label %if_condition

if_condition:                                     ; preds = %function_block
  %function_call_expression = call i16 @factorial(i16 5)
  %0 = icmp eq i16 %function_call_expression, 120
  br i1 %0, label %then_block, label %if_continue

then_block:                                       ; preds = %if_condition
  %trunc_to_byte2 = trunc i16 11 to i8
  call void @high(i8 %trunc_to_byte2)
  br label %if_continue

if_continue:                                      ; preds = %then_block, %if_condition
  br label %if_condition3

if_condition3:                                    ; preds = %if_continue
  %function_call_expression4 = call i16 @factorial(i16 4)
  %1 = icmp eq i16 %function_call_expression4, 23
  br i1 %1, label %then_block5, label %if_continue7

then_block5:                                      ; preds = %if_condition3
  %trunc_to_byte6 = trunc i16 18 to i8
  call void @high(i8 %trunc_to_byte6)
  br label %if_continue7

if_continue7:                                     ; preds = %then_block5, %if_condition3
  ret void
}

define i16 @factorial(i16 %number_PARAM_) {
function_block:
  br label %if_condition

if_condition:                                     ; preds = %function_block
  %0 = icmp eq i16 %number_PARAM_, 0
  br i1 %0, label %then_block, label %if_continue

then_block:                                       ; preds = %if_condition
  ret i16 1

if_continue:                                      ; preds = %if_condition
  %1 = sub i16 %number_PARAM_, 1
  %function_call_expression = call i16 @factorial(i16 %1)
  %2 = mul i16 %function_call_expression, %number_PARAM_
  ret i16 %2
}

declare void @init()

declare void @output(i8)

declare void @high(i8)
