; ModuleID = '02_blink_without_wait.ll'
source_filename = "aKcent_module"

define void @main() {
function_block:
  %previousMillis18 = alloca i32
  %previousMillis11 = alloca i32
  call void @init()
  %ext_to_dword = sext i16 0 to i32
  store i32 %ext_to_dword, i32* %previousMillis11
  %ext_to_dword1 = sext i16 0 to i32
  store i32 %ext_to_dword1, i32* %previousMillis18
  %trunc_to_byte = trunc i16 11 to i8
  call void @output(i8 %trunc_to_byte)
  %trunc_to_byte2 = trunc i16 18 to i8
  call void @output(i8 %trunc_to_byte2)
  br label %while_condition

while_condition:                                  ; preds = %while_loop, %function_block
  br i1 true, label %while_loop, label %while_continue

while_loop:                                       ; preds = %while_condition
  %trunc_to_byte3 = trunc i16 11 to i8
  call void @blinkLED(i32* %previousMillis11, i8 %trunc_to_byte3, i16 500)
  %trunc_to_byte4 = trunc i16 18 to i8
  call void @blinkLED(i32* %previousMillis18, i8 %trunc_to_byte4, i16 100)
  br label %while_condition

while_continue:                                   ; preds = %while_condition
  ret void
}

define void @blinkLED(i32* %previousMillis_PARAM_, i8 %pin_PARAM_, i16 %interval_PARAM_) {
function_block:
  %0 = call i32 @time()
  br label %if_condition

if_condition:                                     ; preds = %function_block
  %1 = getelementptr i32, i32* %previousMillis_PARAM_, i16 0
  %2 = load i32, i32* %1
  %3 = sub i32 %0, %2
  %ext_to_dword = sext i16 %interval_PARAM_ to i32
  %4 = icmp sgt i32 %3, %ext_to_dword
  br i1 %4, label %then_block, label %if_continue

then_block:                                       ; preds = %if_condition
  store i32 %0, i32* %previousMillis_PARAM_
  call void @toggle(i8 %pin_PARAM_)
  br label %if_continue

if_continue:                                      ; preds = %then_block, %if_condition
  ret void
}

declare void @init()

declare void @output(i8)

declare i32 @time()

declare void @toggle(i8)
