; ModuleID = 'aKcent_module'
source_filename = "aKcent_module"

define void @main() {
function_block:
  call void @init()
  %trunc_to_byte = trunc i16 11 to i8
  call void @output(i8 %trunc_to_byte)
  %function_call_expression = call i8 @getPin18()
  call void @output(i8 %function_call_expression)
  br label %while_condition

while_condition:                                  ; preds = %while_loop, %function_block
  br i1 true, label %while_loop, label %while_continue

while_loop:                                       ; preds = %while_condition
  %trunc_to_byte1 = trunc i16 11 to i8
  call void @toggle(i8 %trunc_to_byte1)
  %trunc_to_byte2 = trunc i16 18 to i8
  call void @toggle(i8 %trunc_to_byte2)
  %ext_to_dword = sext i16 1000 to i32
  call void @wait(i32 %ext_to_dword)
  br label %while_condition

while_continue:                                   ; preds = %while_condition
  ret void
}

define i8 @getPin18() {
function_block:
  %variable = alloca float
  store float 0x402C666660000000, float* %variable
  %0 = load float, float* %variable
  %ext_to_real = sitofp i16 4 to float
  %1 = fadd float %0, %ext_to_real
  %trunc_to_byte = fptosi float %1 to i8
  ret i8 %trunc_to_byte
}

declare void @init()

declare void @output(i8)

declare void @toggle(i8)

declare void @wait(i32)
