; ModuleID = '08_blink_smart.ll'
source_filename = "aKcent_module"

define void @main() {
function_block:
  %previousMillis = alloca i32
  call void @init()
  %ext_to_dword = sext i16 0 to i32
  store i32 %ext_to_dword, i32* %previousMillis
  %trunc_to_byte = trunc i16 11 to i8
  call void @output(i8 %trunc_to_byte)
  br label %while_condition

while_condition:                                  ; preds = %if_continue, %function_block
  br i1 true, label %while_loop, label %while_continue

while_loop:                                       ; preds = %while_condition
  %0 = call i32 @time()
  br label %if_condition

if_condition:                                     ; preds = %while_loop
  %1 = getelementptr i32, i32* %previousMillis, i16 0
  %2 = load i32, i32* %1
  %3 = sub i32 %0, %2
  %ext_to_dword1 = sext i16 500 to i32
  %4 = icmp sgt i32 %3, %ext_to_dword1
  br i1 %4, label %then_block, label %if_continue

then_block:                                       ; preds = %if_condition
  store i32 %0, i32* %previousMillis
  %trunc_to_byte2 = trunc i16 11 to i8
  call void @toggle(i8 %trunc_to_byte2)
  br label %if_continue

if_continue:                                      ; preds = %then_block, %if_condition
  br label %while_condition

while_continue:                                   ; preds = %while_condition
  ret void
}

declare void @init()

declare void @output(i8)

declare i32 @time()

declare void @toggle(i8)
