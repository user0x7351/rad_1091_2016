; ModuleID = '09_fibonacci.ll'
source_filename = "aKcent_module"

define void @main() {
function_block:
  call void @init()
  %trunc_to_byte = trunc i16 11 to i8
  call void @output(i8 %trunc_to_byte)
  %trunc_to_byte1 = trunc i16 12 to i8
  call void @output(i8 %trunc_to_byte1)
  %trunc_to_byte2 = trunc i16 17 to i8
  call void @output(i8 %trunc_to_byte2)
  br label %if_condition

if_condition:                                     ; preds = %function_block
  %function_call_expression = call i16 @fibonacci(i16 8)
  %0 = icmp eq i16 %function_call_expression, 21
  br i1 %0, label %then_block, label %if_continue

then_block:                                       ; preds = %if_condition
  %trunc_to_byte3 = trunc i16 11 to i8
  call void @high(i8 %trunc_to_byte3)
  br label %if_continue

if_continue:                                      ; preds = %then_block, %if_condition
  ret void
}

define i16 @fibonacci(i16 %number_PARAM_) {
function_block:
  br label %if_condition

if_condition:                                     ; preds = %function_block
  %0 = icmp eq i16 %number_PARAM_, 0
  br i1 %0, label %then_block, label %if_continue

then_block:                                       ; preds = %if_condition
  ret i16 0

if_continue:                                      ; preds = %if_condition
  br label %if_condition1

if_condition1:                                    ; preds = %if_continue
  %1 = icmp eq i16 %number_PARAM_, 1
  br i1 %1, label %then_block2, label %if_continue3

then_block2:                                      ; preds = %if_condition1
  ret i16 1

if_continue3:                                     ; preds = %if_condition1
  %2 = sub i16 %number_PARAM_, 1
  %function_call_expression = call i16 @fibonacci(i16 %2)
  %3 = sub i16 %number_PARAM_, 2
  %function_call_expression4 = call i16 @fibonacci(i16 %3)
  %4 = add i16 %function_call_expression, %function_call_expression4
  ret i16 %4
}

declare void @init()

declare void @output(i8)

declare void @high(i8)
