; ModuleID = '03_digital_read.ll'
source_filename = "aKcent_module"

define void @main() {
function_block:
  call void @init()
  %trunc_to_byte = trunc i16 6 to i8
  call void @input(i8 %trunc_to_byte)
  %trunc_to_byte1 = trunc i16 11 to i8
  call void @output(i8 %trunc_to_byte1)
  %trunc_to_byte2 = trunc i16 17 to i8
  call void @output(i8 %trunc_to_byte2)
  br label %while_condition

while_condition:                                  ; preds = %if_continue, %function_block
  br i1 true, label %while_loop, label %while_continue

while_loop:                                       ; preds = %while_condition
  br label %if_condition

if_condition:                                     ; preds = %while_loop
  %trunc_to_byte3 = trunc i16 6 to i8
  %0 = call i1 @read(i8 %trunc_to_byte3)
  %1 = icmp eq i1 %0, true
  br i1 %1, label %then_block, label %else_block

then_block:                                       ; preds = %if_condition
  %trunc_to_byte4 = trunc i16 11 to i8
  call void @high(i8 %trunc_to_byte4)
  %trunc_to_byte5 = trunc i16 17 to i8
  call void @high(i8 %trunc_to_byte5)
  br label %if_continue

else_block:                                       ; preds = %if_condition
  %trunc_to_byte6 = trunc i16 11 to i8
  call void @low(i8 %trunc_to_byte6)
  %trunc_to_byte7 = trunc i16 17 to i8
  call void @low(i8 %trunc_to_byte7)
  br label %if_continue

if_continue:                                      ; preds = %else_block, %then_block
  br label %while_condition

while_continue:                                   ; preds = %while_condition
  ret void
}

declare void @init()

declare void @input(i8)

declare void @output(i8)

declare i1 @read(i8)

declare void @high(i8)

declare void @low(i8)
