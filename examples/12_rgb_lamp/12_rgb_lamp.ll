; ModuleID = '12_rgb_lamp.ll'
source_filename = "aKcent_module"

declare i8 @available()

define void @setColor(i16 %redValue_PARAM_, i16 %greenValue_PARAM_, i16 %blueValue_PARAM_) {
function_block:
  %trunc_to_byte = trunc i16 9 to i8
  call void @pwm(i8 %trunc_to_byte, i16 %redValue_PARAM_)
  %trunc_to_byte1 = trunc i16 10 to i8
  call void @pwm(i8 %trunc_to_byte1, i16 %greenValue_PARAM_)
  %trunc_to_byte2 = trunc i16 11 to i8
  call void @pwm(i8 %trunc_to_byte2, i16 %blueValue_PARAM_)
  ret void
}

define void @setMode(i8 %mode_PARAM_, i1* %blinkIndicator_PARAM_) {
function_block:
  br label %if_condition

if_condition:                                     ; preds = %function_block
  %0 = icmp eq i8 %mode_PARAM_, 114
  br i1 %0, label %then_block, label %else_block

then_block:                                       ; preds = %if_condition
  call void @setColor(i16 255, i16 0, i16 0)
  br label %if_continue15

else_block:                                       ; preds = %if_condition
  br label %if_condition1

if_condition1:                                    ; preds = %else_block
  %1 = icmp eq i8 %mode_PARAM_, 103
  br i1 %1, label %then_block2, label %else_block3

then_block2:                                      ; preds = %if_condition1
  call void @setColor(i16 0, i16 255, i16 0)
  br label %if_continue14

else_block3:                                      ; preds = %if_condition1
  br label %if_condition4

if_condition4:                                    ; preds = %else_block3
  %2 = icmp eq i8 %mode_PARAM_, 98
  br i1 %2, label %then_block5, label %else_block6

then_block5:                                      ; preds = %if_condition4
  call void @setColor(i16 0, i16 0, i16 255)
  br label %if_continue13

else_block6:                                      ; preds = %if_condition4
  br label %if_condition7

if_condition7:                                    ; preds = %else_block6
  %3 = icmp eq i8 %mode_PARAM_, 108
  br i1 %3, label %then_block8, label %if_continue12

then_block8:                                      ; preds = %if_condition7
  br label %if_condition9

if_condition9:                                    ; preds = %then_block8
  %4 = getelementptr i1, i1* %blinkIndicator_PARAM_, i16 0
  %5 = load i1, i1* %4
  %6 = icmp eq i1 %5, true
  br i1 %6, label %then_block10, label %else_block11

then_block10:                                     ; preds = %if_condition9
  store i1 false, i1* %blinkIndicator_PARAM_
  br label %if_continue

else_block11:                                     ; preds = %if_condition9
  store i1 true, i1* %blinkIndicator_PARAM_
  br label %if_continue

if_continue:                                      ; preds = %else_block11, %then_block10
  br label %if_continue12

if_continue12:                                    ; preds = %if_continue, %if_condition7
  br label %if_continue13

if_continue13:                                    ; preds = %if_continue12, %then_block5
  br label %if_continue14

if_continue14:                                    ; preds = %if_continue13, %then_block2
  br label %if_continue15

if_continue15:                                    ; preds = %if_continue14, %then_block
  ret void
}

define void @blink(i32* %previousMillis_PARAM_, i1* %blinkState_PARAM_) {
function_block:
  %0 = call i32 @time()
  br label %if_condition

if_condition:                                     ; preds = %function_block
  %1 = getelementptr i32, i32* %previousMillis_PARAM_, i16 0
  %2 = load i32, i32* %1
  %3 = sub i32 %0, %2
  %trunc_to_byte = trunc i16 14 to i8
  %4 = call i16 @adc(i8 %trunc_to_byte)
  %ext_to_dword = sext i16 %4 to i32
  %5 = icmp sgt i32 %3, %ext_to_dword
  br i1 %5, label %then_block, label %if_continue3

then_block:                                       ; preds = %if_condition
  br label %if_condition1

if_condition1:                                    ; preds = %then_block
  %6 = getelementptr i1, i1* %blinkState_PARAM_, i16 0
  %7 = load i1, i1* %6
  %8 = icmp eq i1 %7, true
  br i1 %8, label %then_block2, label %else_block

then_block2:                                      ; preds = %if_condition1
  call void @setColor(i16 150, i16 0, i16 255)
  br label %if_continue

else_block:                                       ; preds = %if_condition1
  call void @setColor(i16 0, i16 0, i16 0)
  br label %if_continue

if_continue:                                      ; preds = %else_block, %then_block2
  store i32 %0, i32* %previousMillis_PARAM_
  %9 = getelementptr i1, i1* %blinkState_PARAM_, i16 0
  %10 = load i1, i1* %9
  %11 = xor i1 %10, true
  store i1 %11, i1* %blinkState_PARAM_
  br label %if_continue3

if_continue3:                                     ; preds = %if_continue, %if_condition
  ret void
}

define void @main() {
function_block:
  %previousMillis = alloca i32
  %blinkIndicator = alloca i1
  %blinkState = alloca i1
  call void @init()
  %ext_to_dword = sext i16 9600 to i32
  call void @begin(i32 %ext_to_dword)
  %trunc_to_byte = trunc i16 9 to i8
  call void @init_pwm(i8 %trunc_to_byte)
  %trunc_to_byte1 = trunc i16 10 to i8
  call void @init_pwm(i8 %trunc_to_byte1)
  %trunc_to_byte2 = trunc i16 11 to i8
  call void @init_pwm(i8 %trunc_to_byte2)
  call void @setInternalAREF()
  store i1 true, i1* %blinkState
  store i1 false, i1* %blinkIndicator
  %ext_to_dword3 = sext i16 0 to i32
  store i32 %ext_to_dword3, i32* %previousMillis
  call void @setColor(i16 255, i16 255, i16 255)
  br label %while_condition

while_condition:                                  ; preds = %if_continue6, %function_block
  br i1 true, label %while_loop, label %while_continue

while_loop:                                       ; preds = %while_condition
  br label %if_condition

if_condition:                                     ; preds = %while_loop
  %function_call_expression = call i8 @available()
  %ext_to_word = sext i8 %function_call_expression to i16
  %0 = icmp sgt i16 %ext_to_word, 0
  br i1 %0, label %then_block, label %if_continue

then_block:                                       ; preds = %if_condition
  %1 = call i8 @receive()
  call void @setMode(i8 %1, i1* %blinkIndicator)
  br label %if_continue

if_continue:                                      ; preds = %then_block, %if_condition
  br label %if_condition4

if_condition4:                                    ; preds = %if_continue
  %2 = load i1, i1* %blinkIndicator
  %3 = icmp eq i1 %2, true
  br i1 %3, label %then_block5, label %if_continue6

then_block5:                                      ; preds = %if_condition4
  call void @blink(i32* %previousMillis, i1* %blinkState)
  br label %if_continue6

if_continue6:                                     ; preds = %then_block5, %if_condition4
  br label %while_condition

while_continue:                                   ; preds = %while_condition
  ret void
}

declare void @pwm(i8, i16)

declare i32 @time()

declare i16 @adc(i8)

declare void @init()

declare void @begin(i32)

declare void @init_pwm(i8)

declare void @setInternalAREF()

declare i8 @receive()
