; ModuleID = '04_adc_read.ll'
source_filename = "aKcent_module"

define void @main() {
function_block:
  call void @init()
  call void @setInternalAREF()
  %trunc_to_byte = trunc i16 11 to i8
  call void @output(i8 %trunc_to_byte)
  %trunc_to_byte1 = trunc i16 12 to i8
  call void @output(i8 %trunc_to_byte1)
  %trunc_to_byte2 = trunc i16 17 to i8
  call void @output(i8 %trunc_to_byte2)
  %trunc_to_byte3 = trunc i16 18 to i8
  call void @output(i8 %trunc_to_byte3)
  br label %while_condition

while_condition:                                  ; preds = %if_continue, %function_block
  br i1 true, label %while_loop, label %while_continue

while_loop:                                       ; preds = %while_condition
  %trunc_to_byte4 = trunc i16 19 to i8
  %0 = call i16 @adc(i8 %trunc_to_byte4)
  br label %if_condition

if_condition:                                     ; preds = %while_loop
  %1 = icmp slt i16 %0, 512
  br i1 %1, label %then_block, label %else_block

then_block:                                       ; preds = %if_condition
  %trunc_to_byte5 = trunc i16 11 to i8
  call void @high(i8 %trunc_to_byte5)
  %trunc_to_byte6 = trunc i16 12 to i8
  call void @high(i8 %trunc_to_byte6)
  %trunc_to_byte7 = trunc i16 17 to i8
  call void @high(i8 %trunc_to_byte7)
  %trunc_to_byte8 = trunc i16 18 to i8
  call void @high(i8 %trunc_to_byte8)
  br label %if_continue

else_block:                                       ; preds = %if_condition
  %trunc_to_byte9 = trunc i16 11 to i8
  call void @low(i8 %trunc_to_byte9)
  %trunc_to_byte10 = trunc i16 12 to i8
  call void @low(i8 %trunc_to_byte10)
  %trunc_to_byte11 = trunc i16 17 to i8
  call void @low(i8 %trunc_to_byte11)
  %trunc_to_byte12 = trunc i16 18 to i8
  call void @low(i8 %trunc_to_byte12)
  br label %if_continue

if_continue:                                      ; preds = %else_block, %then_block
  br label %while_condition

while_continue:                                   ; preds = %while_condition
  ret void
}

declare void @init()

declare void @setInternalAREF()

declare void @output(i8)

declare i16 @adc(i8)

declare void @high(i8)

declare void @low(i8)
