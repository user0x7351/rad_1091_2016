#include "uart.h"
#include "defines.h"
#include <avr/interrupt.h>
#include <util/atomic.h>

// RX data
static volatile uint8_t _rx_buffer[RX_BUFFER_SIZE];
static volatile uint8_t _rx_buffer_read_position = 0;
static volatile uint8_t _rx_buffer_write_position = 0;

//-----------------------------------------------------------------------------
// begin
//-----------------------------------------------------------------------------
void begin(uint32_t baudrate)
{
	// set baudrate
	uint16_t ubrr;
	switch (baudrate)
	{
		case 300:
			ubrr = 3332;
			break;
		case 600:
			ubrr = 1666;
			break;
		case 1200:
			ubrr = 832;
			break;
		case 2400:
			ubrr = 416;
			break;
		case 4800:
			ubrr = 207;
			break;
		case 9600:
			ubrr = 103;
			break;
		case 14400:
			ubrr = 68;
			break;
		case 19200:
			ubrr = 51;
			break;
		case 28800:
			ubrr = 34;
			break;
		case 38400:
			ubrr = 25;
			break;
		case 57600:
			ubrr = 16;
			break;
		case 115200:
			ubrr = 8;
			break;
	}
	UBRR0H = ubrr >> 8;
	UBRR0L = ubrr;

	// set 8-bit data frame
	UCSR0C |= (_BV(UCSZ00) | _BV(UCSZ01));

	// RX complete interrupt enable
	// RX enable
	// TX enable
	UCSR0B |= _BV(RXCIE0);
	UCSR0B |= _BV(RXEN0);
	UCSR0B |= _BV(TXEN0);

	// set global interrupt flag
	sei();
}

//-----------------------------------------------------------------------------
// available
//-----------------------------------------------------------------------------
uint8_t available()
{
	uint8_t number_of_bytes;

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		number_of_bytes = ((RX_BUFFER_SIZE + _rx_buffer_write_position - _rx_buffer_read_position) % RX_BUFFER_SIZE);
	}

	return number_of_bytes;
}

//-----------------------------------------------------------------------------
// receive
//-----------------------------------------------------------------------------
uint8_t receive()
{
	uint8_t received_byte = '\0';
	uint8_t write_position;

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		write_position = _rx_buffer_write_position;
	}

	if (_rx_buffer_read_position != write_position)
	{
		received_byte = _rx_buffer[_rx_buffer_read_position];
		++_rx_buffer_read_position;

		if (_rx_buffer_read_position == RX_BUFFER_SIZE)
			_rx_buffer_read_position = 0;
	}

	return received_byte;
}

//-----------------------------------------------------------------------------
// send
//-----------------------------------------------------------------------------
void send(uint8_t byte)
{
	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = byte;
	loop_until_bit_is_set(UCSR0A, TXC0);
}

//-----------------------------------------------------------------------------
// ISR (USART_RX_vect)
//-----------------------------------------------------------------------------
ISR (USART_RX_vect)
{
	_rx_buffer[_rx_buffer_write_position] = UDR0;

	++_rx_buffer_write_position;
	if (_rx_buffer_write_position == RX_BUFFER_SIZE)
		_rx_buffer_write_position = 0;
}
