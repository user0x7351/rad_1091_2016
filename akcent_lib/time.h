#ifndef TIME_HPP
#define TIME_HPP

#include <avr/io.h>

void init();
int32_t time();
int32_t utime();
void wait(int32_t delayTime);

#endif // TIME_HPP
