#include "time.h"
#include "defines.h"
#include <util/delay.h>
#include <avr/interrupt.h>
#include <util/atomic.h>

static volatile int32_t _timer0_millis = 0;
static volatile int32_t _timer0_overflow_count = 0;
static uint8_t _timer0_fract = 0;

//-----------------------------------------------------------------------------
// init_timer_0
//-----------------------------------------------------------------------------
void init()
{
	// set global interrupt flag
	sei();
	// clk/64 prescaler
	TCCR0B |= _BV(CS00);
	TCCR0B |= _BV(CS11);
	// overflow interrupt enable
	TIMSK0 |= _BV(TOIE0);
}

//-----------------------------------------------------------------------------
// time
//-----------------------------------------------------------------------------
int32_t time()
{
	int32_t millis_temp;

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		millis_temp = _timer0_millis;
	}

	return millis_temp;
}

//-----------------------------------------------------------------------------
// utime
//-----------------------------------------------------------------------------
int32_t utime()
{
	int32_t timer0_overflow_count_temp;
	uint8_t timer0_count_temp;

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		timer0_overflow_count_temp = _timer0_overflow_count;
		timer0_count_temp = TCNT0;

		// the bit TIFR0.TOV0 is set when an overflow occurs
		if (bit_is_set(TIFR0, TOV0) && (timer0_count_temp < 255))
			++timer0_overflow_count_temp;
	}

	return ((timer0_overflow_count_temp << 8) + timer0_count_temp - 1) << 2;
}

//-----------------------------------------------------------------------------
// wait
//-----------------------------------------------------------------------------
void wait(int32_t delayTime)
{
	int32_t micros_temp = utime();
	int32_t elapsedUTime;
	
	do
	{
		elapsedUTime = (utime() - micros_temp);

		if (elapsedUTime >= 1000)
		{
			--delayTime;
			micros_temp += elapsedUTime;
		}
	} while (delayTime);
}

//-----------------------------------------------------------------------------
// ISR (TIMER0_OVF_vect)
//-----------------------------------------------------------------------------
ISR(TIMER0_OVF_vect)
{
	int32_t millis_temp = _timer0_millis;
	uint8_t fract_temp = _timer0_fract;

	fract_temp += 3;

	if (fract_temp >= 125)
	{
		fract_temp -= 125;
		++millis_temp;
	}

	_timer0_fract = fract_temp;
	_timer0_millis = ++millis_temp;

	++_timer0_overflow_count;
}
