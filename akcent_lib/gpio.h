#ifndef GPIO_HPP
#define GPIO_HPP

#include <avr/io.h>

void input(uint8_t pinNumber);
void output(uint8_t pinNumber);

void low(uint8_t pinNumber);
void high(uint8_t pinNumber);
void toggle(uint8_t pinNumber);

uint8_t read(uint8_t pinNumber);

void setInternalAREF();
void setExternalAREF();
uint16_t adc(uint8_t pinNumber);

void init_pwm(uint8_t pinNumber);
void pwm(uint8_t pinNumber, int16_t value);

#endif // GPIO_HPP
