#include "gpio.h"
#include "defines.h"

//-----------------------------------------------------------------------------
// output
//-----------------------------------------------------------------------------
void output(uint8_t pinNumber)
{
	// pins: PD0 - PD7
	if (pinNumber < 8)
		DDRD |= _BV(pinNumber);
	// pins: PB0 - PB5
	else if (pinNumber < 14)
		DDRB |= _BV(pinNumber-8);
	// pins: PC0 - PC5
	else
		DDRC |= _BV(pinNumber-14);
}

//-----------------------------------------------------------------------------
// input
//-----------------------------------------------------------------------------
void input(uint8_t pinNumber)
{
	// pins: PD0 - PD7
	if (pinNumber < 8)
		DDRD &= _BM(pinNumber);
	// pins: PB0 - PB5
	else if (pinNumber < 14)
		DDRB &= _BM(pinNumber-8);
	// pins: PC0 - PC5
	else
		DDRC &= _BM(pinNumber-14);
}

//-----------------------------------------------------------------------------
// low
//-----------------------------------------------------------------------------
void low(uint8_t pinNumber)
{
	// pins: PD0 - PD7
	if (pinNumber < 8)
		PORTD &= _BM(pinNumber);
	// pins: PB0 - PB5
	else if (pinNumber < 14)
		PORTB &= _BM(pinNumber-8);
	// pins: PC0 - PC5
	else
		PORTC &= _BM(pinNumber-14);
}

//-----------------------------------------------------------------------------
// high
//-----------------------------------------------------------------------------
void high(uint8_t pinNumber)
{
	// pins: PD0 - PD7
	if (pinNumber < 8)
		PORTD |= _BV(pinNumber);
	// pins: PB0 - PB5
	else if (pinNumber < 14)
		PORTB |= _BV(pinNumber-8);
	// pins: PC0 - PC5
	else
		PORTC |= _BV(pinNumber-14);
}

//-----------------------------------------------------------------------------
// toggle
//-----------------------------------------------------------------------------
void toggle(uint8_t pinNumber)
{
	// pins: PD0 - PD7
	if (pinNumber < 8)
		PIND |= _BV(pinNumber);
	// pins: PB0 - PB5
	else if (pinNumber < 14)
		PINB |= _BV(pinNumber - 8);
	// pins: PC0 - PC5
	else
		PINC |= _BV(pinNumber - 14);
}

//-----------------------------------------------------------------------------
// read
//-----------------------------------------------------------------------------
uint8_t read(uint8_t pinNumber)
{
	// pins: PD0 - PD7
	if (pinNumber < 8)
		return (PIND &= _BV(pinNumber)) >> pinNumber;
	// pins: PB0 - PB5
	else if (pinNumber < 14)
		return (PINB &= _BV(pinNumber-8)) >> (pinNumber - 8);
	// pins: PC0 - PC5
	else
		return (PINC &= _BV(pinNumber-14)) >> (pinNumber - 14);
}

//-----------------------------------------------------------------------------
// setInternalAREF
//-----------------------------------------------------------------------------
void setInternalAREF()
{
	ADMUX |= _BV(REFS0);

	// ADC working speed: set prescale factor to 128 (125 KHz)
	ADCSRA |= (_BV(ADPS0) | _BV(ADPS1) | _BV(ADPS2));

	// enable ADC
	ADCSRA |= _BV(ADEN);
}

//-----------------------------------------------------------------------------
// setExternalAREF
//-----------------------------------------------------------------------------
void setExternalAREF()
{
	// ADC working speed: set prescale factor to 128 (125 KHz)
	ADCSRA |= (_BV(ADPS0) | _BV(ADPS1) | _BV(ADPS2));

	// enable ADC
	ADCSRA |= _BV(ADEN);
}

//-----------------------------------------------------------------------------
// adc
//-----------------------------------------------------------------------------
uint16_t adc(uint8_t pinNumber)
{
	uint8_t channel = pinNumber - 14;

	// set ADC channel
	ADMUX |= channel;

	// start conversion and wait for result
	ADCSRA |= _BV(ADSC);
	while (bit_is_set(ADCSRA, ADSC));

	// return result
	return ADC;
}

//-----------------------------------------------------------------------------
// init_pwm
//-----------------------------------------------------------------------------
void init_pwm(uint8_t pinNumber)
{
	switch (pinNumber)
	{
		//---------------
		// Timer #0
		//---------------
		case 5:
			// set as output
			DDRD |= _BV(PD5);
			
			// set prescaler: clk/64
			if (bit_is_clear(TCCR0B, CS01))
				TCCR0B |= _BV(CS01);
			if (bit_is_clear(TCCR0B, CS00))
				TCCR0B |= _BV(CS00);
			// set fast PWM
			if (bit_is_clear(TCCR0A, WGM00))
				TCCR0A |= _BV(WGM00);
			if (bit_is_clear(TCCR0A, WGM01))
				TCCR0A |= _BV(WGM01);

			// clear OC0B on compare match, set OC0B at BOTTOM
			TCCR0A |= _BV(COM0B1);

			break;
		case 6:
			// set as output
			DDRD |= _BV(PD6);
			
			// set prescaler: clk/64
			if (bit_is_clear(TCCR0B, CS01))
				TCCR0B |= _BV(CS01);
			if (bit_is_clear(TCCR0B, CS00))
				TCCR0B |= _BV(CS00);
			// set fast PWM
			if (bit_is_clear(TCCR0A, WGM00))
				TCCR0A |= _BV(WGM00);
			if (bit_is_clear(TCCR0A, WGM01))
				TCCR0A |= _BV(WGM01);
			
			// clear OC0A on compare match, set OC0A at BOTTOM
			TCCR0A |= _BV(COM0A1);

			break;
		//---------------
		// Timer #1
		//---------------
		case 9:
			// set as output
			DDRB |= _BV(PB1);
			
			// set prescaler: clk/64
			if (bit_is_clear(TCCR1B, CS11))
				TCCR1B |= _BV(CS11);
			if (bit_is_clear(TCCR1B, CS10))
				TCCR1B |= _BV(CS10);
			// set fast PWM
			if (bit_is_clear(TCCR1A, WGM10))
				TCCR1A |= _BV(WGM10);
			if (bit_is_clear(TCCR1B, WGM12))
				TCCR1B |= _BV(WGM12);
			
			// clear OC0B on compare match, set OC0B at BOTTOM 
			TCCR1A |= _BV(COM1A1);

			break;
		case 10:
			// set as output
			DDRB |= _BV(PB2);
			
			// set prescaler: clk/64
			if (bit_is_clear(TCCR1B, CS11))
				TCCR1B |= _BV(CS11);
			if (bit_is_clear(TCCR1B, CS10))
				TCCR1B |= _BV(CS10);
			// set fast PWM
			if (bit_is_clear(TCCR1A, WGM10))
				TCCR1A |= _BV(WGM10);
			if (bit_is_clear(TCCR1B, WGM12))
				TCCR1B |= _BV(WGM12);
			
			// clear OC0B on compare match, set OC0B at BOTTOM 
			TCCR1A |= _BV(COM1B1);

			break;
		//---------------
		// Timer #2
		//---------------			
		case 11:
			// set as output
			DDRB |= _BV(PB3);			

			// set prescaler: clk/64
			if (bit_is_clear(TCCR2B, CS22))
				TCCR2B |= _BV(CS22);
			// set fast PWM
			if (bit_is_clear(TCCR2A, WGM21))
				TCCR2A |= _BV(WGM21);
			if (bit_is_clear(TCCR2A, WGM20))
				TCCR2A |= _BV(WGM20);
			
			// clear OC0B on compare match, set OC0B at BOTTOM 
			TCCR2A |= _BV(COM2A1);

			break;
		case 3:
			// set as output
			DDRD |= _BV(PD3);
			
			// set prescaler: clk/64
			if (bit_is_clear(TCCR2B, CS22))
				TCCR2B |= _BV(CS22);
			// set fast PWM
			if (bit_is_clear(TCCR2A, WGM21))
				TCCR2A |= _BV(WGM21);
			if (bit_is_clear(TCCR2A, WGM20))
				TCCR2A |= _BV(WGM20);
			
			// clear OC0B on compare match, set OC0B at BOTTOM 
			TCCR2A |= _BV(COM2B1);

			break;
	}
}

//-----------------------------------------------------------------------------
// pwm
//-----------------------------------------------------------------------------
void pwm(uint8_t pinNumber, int16_t value)
{
	// set compare register value
	switch (pinNumber)
	{
		//---------------
		// Timer #0
		//---------------		
		case 6:
			        OCR0A = value;
			        break;
		case 5:
			        OCR0B = value;
			        break;
		//---------------
		// Timer #1
		//---------------			        
		case 9:
			        OCR1A = value;
			        break;
		case 10:
			        OCR1B = value;
			        break;
		//---------------
		// Timer #2
		//---------------					        
		case 11:
			        OCR2A = value;
			        break;
		case 3:
			        OCR2B = value;
			        break;
	}
}
