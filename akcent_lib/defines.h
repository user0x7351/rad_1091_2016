#ifndef DEFINES_HPP
#define DEFINES_HPP

#define _BM(bit) (~_BV(bit))

#define TX_BUFFER_SIZE 64
#define RX_BUFFER_SIZE 64

#endif // DEFINES_HPP
