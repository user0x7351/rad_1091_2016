#ifndef UART_HPP
#define UART_HPP

#include <avr/io.h>

void begin(uint32_t baudrate);
uint8_t available();
uint8_t receive();
void send(uint8_t data);

#endif // UART_HPP
